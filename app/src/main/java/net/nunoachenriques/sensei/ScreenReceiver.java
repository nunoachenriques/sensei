/*
 * Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Get broadcast of interactive state change and act accordingly.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class ScreenReceiver
        extends BroadcastReceiver {

    private static final String TAG = "ScreenReceiver";

    public ScreenReceiver() {
        super();
    }

    @Override
    public void onReceive(Context c, Intent i) {
        String action;
        if (i != null && (action = i.getAction()) != null) {
            try {
                if (action.equals(Intent.ACTION_SCREEN_OFF)) {
                    Log.d(TAG, Intent.ACTION_SCREEN_OFF);
                } else if (action.equals(Intent.ACTION_SCREEN_ON)) {
                    Log.d(TAG, Intent.ACTION_SCREEN_ON);
                    // EMPATHY notification UPDATE
                    c.startService(new Intent(c, EmpathyNotifyService.class));
                }
            } catch (Exception e) {  // IllegalState or other...
                Log.e("ScreenReceiver", "Start service FAILED!", e);
            }
        }
    }
}
