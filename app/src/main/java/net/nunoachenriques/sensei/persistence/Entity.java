/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.persistence;

import androidx.annotation.NonNull;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.sql.Date;

/**
 * Entity (just humans for now) data holder (e.g., birthday) for local
 * persistence and expanse sync.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
@SuppressWarnings("WeakerAccess")
@androidx.room.Entity
public class Entity
        implements Serializable {

    @SuppressWarnings("unused")
    @PrimaryKey
    public long primaryKey = 1;  // ONLY ONE record!

    public String id;
    public Date birthday;
    public String gender;
    public boolean synced;

    public Entity() {}

    /**
     * Entity only entry.
     *
     * @param i Identifier (abstract).
     * @param b Date of birth.
     * @param g Gender.
     * @param s Synced (mark data already synced with Expanse).
     */
    public Entity(String i, Date b, String g, boolean s) {
        id = i;
        birthday = b;
        gender = g;
        synced = s;
    }

    @NonNull
    @Override
    public String toString() {
        return "Entity{" +
                "id='" + id + "'" +
                ", birthday='" + birthday + "'" +
                ", gender='" + gender + "'" +
                ", synced='" + synced + "'" +
                "}";
    }
}
