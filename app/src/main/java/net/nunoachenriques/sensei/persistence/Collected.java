/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.persistence;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Sensors collected data holder for local persistence and expanse sync.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
@Entity(indices = {@Index(value = {"synced", "moment"}),
        @Index(value = {"sensorType", "id"}),
        @Index(value = {"sensorType", "sensorAttribute", "sensorValue"}),
        @Index(value = {"moment", "momentTz",
                "sensorType", "sensorAttribute", "sensorValue", "sensorValueExtra"}, unique = true)})
public class Collected
        implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public long id;

    public Timestamp moment;
    /** Time zone in {@code {+|-}HH:MM} format (e.g., "+08:00") */
    public String momentTz;
    public String sensorType;
    public String sensorAttribute;
    public Double sensorValue;
    public String sensorValueExtra;
    public boolean synced;

    public Collected() {}

    /**
     * All collected data entry.
     *
     * @param m Moment (date and time).
     * @param mtz Moment time zone.
     * @param st Sensor type identifier (e.g., app.sensor.geopositioning).
     * @param sa Sensor attribute (e.g., latitude).
     * @param sv Sensor value (e.g., 38.7555857).
     * @param sve Sensor value extra.
     * @param s Synced (mark data already synced with Expanse).
     */
    public Collected(Timestamp m, String mtz, String st, String sa, double sv, String sve, boolean s) {
        moment = m;
        momentTz = mtz;
        sensorType = st;
        sensorAttribute = sa;
        sensorValue = sv;
        sensorValueExtra = sve;
        synced = s;
    }

    @NonNull
    @Override
    public String toString() {
        return "Collected{" +
                "moment='" + moment + "'" +
                ", momentTz='" + momentTz + "'" +
                ", sensorType='" + sensorType + "'" +
                ", sensorAttribute='" + sensorAttribute + "'" +
                ", sensorValue=" + sensorValue +
                ", sensorValueExtra='" + sensorValueExtra + "'" +
                ", synced='" + synced + "'" +
                "}";
    }
}
