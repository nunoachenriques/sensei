/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.persistence;

import androidx.room.TypeConverter;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * The type converter class utility for persistence.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
@SuppressWarnings("WeakerAccess")
public class Converter {

    private Converter() {}

    /**
     * Converts boolean logic value to integer value.
     *
     * @param b {@code true} or {@code false}.
     * @return 1 if true, 0 otherwise.
     */
    @TypeConverter
    public static int fromBooleanToInt(boolean b) {
        return b ? 1 : 0;
    }

    /**
     * Converts integer value to boolean value.
     *
     * @param i 1 or 0.
     * @return {@code true} if 1, {@code false} otherwise.
     */
    @TypeConverter
    public static boolean fromIntToBoolean(int i) {
        return i == 1;
    }

    /**
     * Converts Java milliseconds since UNIX Epoch UTC timestamp {@code long}
     * value to SQL Date object.
     *
     * @param millis Milliseconds since UNIX Epoch UTC.
     * @return SQL Date or {@code null} if millis is {@code null}.
     */
    @TypeConverter
    public static Date fromMillisToDate(Long millis) {
        return millis == null ? null : new Date(millis);
    }

    /**
     * Converts SQL Date object to Java milliseconds since UNIX Epoch UTC
     * timestamp {@code long} value.
     *
     * @param d SQL Date.
     * @return Java milliseconds since UNIX Epoch UTC or {@code null} if date is {@code null}.
     */
    @TypeConverter
    public static Long fromDateToMillis(Date d) {
        return d == null ? null : d.getTime();
    }

    /**
     * Converts Java milliseconds since UNIX Epoch UTC timestamp {@code long}
     * value to SQL Timestamp object.
     *
     * @param millis Milliseconds since UNIX Epoch UTC.
     * @return SQL Timestamp.
     */
    @TypeConverter
    public static Timestamp fromMillisToTimestamp(long millis) {
        return new Timestamp(millis);
    }

    /**
     * Converts SQL Timestamp object to Java milliseconds since UNIX Epoch UTC
     * timestamp {@code long} value.
     *
     * @param ts SQL Timestamp.
     * @return Java milliseconds since UNIX Epoch UTC.
     */
    @TypeConverter
    public static long fromTimestampToMillis(Timestamp ts) {
        return ts.getTime();
    }
}
