/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.persistence;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Application parameter data holder for local persistence and expanse sync.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
@Entity(indices = {@Index(value = {"synced", "moment"}), @Index("key")})
public class Parameter
        implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public long id;

    public Timestamp moment;
    /** Time zone in {@code {+|-}HH:MM} format (e.g., "+08:00") */
    public String momentTz;
    public String key;
    public String value;
    public boolean synced;

    public Parameter() {}

    /**
     * Application parameter entry.
     *
     * @param m Moment (date and time).
     * @param mtz Moment time zone.
     * @param k Key (name).
     * @param v Value.
     * @param s Synced (mark data already synced with Expanse).
     */
    public Parameter(Timestamp m, String mtz, String k, String v, boolean s) {
        moment = m;
        momentTz = mtz;
        key = k;
        value = v;
        synced = s;
    }

    @NonNull
    @Override
    public String toString() {
        return "Parameter{" +
                "moment='" + moment + "'" +
                ", momentTz='" + momentTz + "'" +
                ", key='" + key + "'" +
                ", value='" + value + "'" +
                ", synced='" + synced + "'" +
                "}";
    }
}
