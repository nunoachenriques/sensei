/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.persistence;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Chat/Diary collected data holder for local persistence and expanse sync.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
@SuppressWarnings("WeakerAccess")
@Entity(indices = {@Index(value = {"moment"})})
public class Chat
        implements Serializable {

    public static final int AGENT = 0;
    public static final int HUMAN = 1;

    @PrimaryKey(autoGenerate = true)
    public long id;

    public Timestamp moment;
    /** Time zone in {@code {+|-}HH:MM} format (e.g., "+08:00") */
    public String momentTz;
    public int entity;
    public String text;
    public String language;
    public double polarity;

    public Chat() {}

    /**
     * Chat/Diary entry.
     *
     * @param m Moment (date and time).
     * @param mtz Moment time zone.
     * @param e Entity (e.g., {@link Chat#HUMAN}).
     * @param t Text written.
     * @param l Language as in ISO 639 alpha-2 code (e.g., en).
     * @param p Emotional valence polarity in [-1, 1].
     */
    public Chat(Timestamp m, String mtz, int e, String t, String l, double p) {
        moment = m;
        momentTz = mtz;
        entity = e;
        text = t;
        language = l;
        polarity = p;
    }

    @NonNull
    @Override
    public String toString() {
        return "Chat{" +
                "moment='" + moment + "'" +
                ", momentTz='" + momentTz + "'" +
                ", entity=" + entity +
                ", text='" + text + "'" +
                ", language='" + language + "'" +
                ", polarity=" + polarity +
                "}";
    }
}
