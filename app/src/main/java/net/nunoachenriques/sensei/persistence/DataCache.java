/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.persistence;

import android.content.Context;
import android.database.Cursor;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SimpleSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteQuery;

import net.nunoachenriques.sensei.ActivityFeed;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The database persistence ({@link RoomDatabase}) for storing collected data.
 * Works like a staging memory before sync with a permanent (long-term) memory.
 * Includes (re)engineering of full text search (FTS) feature in some tables.
 * Moreover, deals with migration between database versions to avoid data loss.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
@Database(entities = {Chat.class, Collected.class, Entity.class, Parameter.class},
        version = 3)
@TypeConverters({Converter.class})
public abstract class DataCache
        extends RoomDatabase {

    private static volatile DataCache INSTANCE;

    // language=TEXT
    private static final String CHAT_CREATE_FTS =
            "CREATE VIRTUAL TABLE Chat_fts USING fts4(content=Chat, `text`)";
    // language=TEXT
    private static final String CHAT_CREATE_FTS_SYNC_TRIGGER =
            "CREATE TRIGGER Chat_fts_sync_insert AFTER INSERT ON Chat " +
                    "BEGIN INSERT INTO Chat_fts(docid, `text`) VALUES(new.rowid, new.text); END";
    // language=TEXT
    private static final String CHAT_CREATE_FTS_REBUILD =
            "INSERT INTO Chat_fts(Chat_fts) VALUES ('rebuild')";
    // https://en.wikipedia.org/wiki/List_of_emoticons
    private static final Pattern emoticonOnlyPattern =
            Pattern.compile("([>}])?([:;=x8#%])(')?([-oc^])?([)(|/\\\\*&#$bDPpSsxX])");
    // Avoid all but simple tokens (alphanumeric + space).
    private static final Pattern sqliteFTSAvoidPattern = Pattern.compile("[^a-zA-Z0-9 ]");

    /**
     * Full text search for Chat/Diary. Chat and Collected index refactored.
     * @see <a href="https://www.sqlite.org/queryplanner.html">SQLite query planning</a>
     */
    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase db) {
            // Chat DROP OLD INDICES
            Cursor cursor = db.query("SELECT name FROM sqlite_master WHERE type = 'index' AND tbl_name = 'Chat'");
            while (cursor.moveToNext()) {
                db.execSQL("DROP INDEX IF EXISTS '"
                        + cursor.getString(cursor.getColumnIndex("name")) + "'");
            }
            // Chat CREATE NEW INDICES
            db.execSQL("CREATE  INDEX `index_Chat_moment` ON `Chat` (`moment`)");
            // Chat CREATE FULL TEXT SEARCH
            db.execSQL(CHAT_CREATE_FTS);
            db.execSQL(CHAT_CREATE_FTS_SYNC_TRIGGER);
            db.execSQL(CHAT_CREATE_FTS_REBUILD);
        }
    };

    /**
     * Collected duplicate data clean and unique index create.
     */
    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase db) {
            /*
             * ALTER TABLE to add constraints not supported by SQLite
             * https://www.sqlite.org/lang_altertable.html
             */
            db.execSQL("PRAGMA foreign_keys=false");
            db.beginTransaction();
            try {
                // FIRST: delete ALL activity data
                db.execSQL("DELETE FROM Collected WHERE sensorType = '" + ActivityFeed.SENSOR_ID + "'");
                // LAST: delete duplicate data
                db.execSQL("DELETE FROM Collected WHERE id NOT IN " +
                        "(SELECT min(id) FROM Collected " +
                        "GROUP BY moment, momentTz, sensorType, sensorAttribute, sensorValue, sensorValueExtra)");
                // Migrate data using another table
                db.execSQL("CREATE TABLE `newCollected` " +
                        "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `moment` INTEGER, `momentTz` TEXT, " +
                        "`sensorType` TEXT, `sensorAttribute` TEXT, `sensorValue` REAL, `sensorValueExtra` TEXT, " +
                        "`synced` INTEGER NOT NULL)");
                db.execSQL("INSERT INTO newCollected SELECT * FROM Collected");
                db.execSQL("DROP TABLE Collected");
                db.execSQL("ALTER TABLE newCollected RENAME TO Collected");
                // Recreate indices
                db.execSQL("CREATE INDEX `index_Collected_synced_moment` ON `Collected` (`synced`, `moment`)");
                db.execSQL("CREATE INDEX `index_Collected_sensorType_id` ON `Collected` (`sensorType`, `id`)");
                db.execSQL("CREATE INDEX `index_Collected_sensorType_sensorAttribute_sensorValue` " +
                        "ON `Collected` (`sensorType`, `sensorAttribute`, `sensorValue`)");
                db.execSQL("CREATE UNIQUE INDEX " +
                        "`index_Collected_moment_momentTz_sensorType_sensorAttribute_sensorValue_sensorValueExtra` " +
                        "ON `Collected` (`moment`, `momentTz`, " +
                        "`sensorType`, `sensorAttribute`, `sensorValue`, `sensorValueExtra`)");
                db.execSQL("PRAGMA foreign_key_check");
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }
            db.execSQL("PRAGMA foreign_keys=true");
        }
    };

    public abstract DataCacheDao getDao();

    public static DataCache getInstance(Context c) {
        if (INSTANCE == null) {
            synchronized (DataCache.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(
                            c.getApplicationContext(), DataCache.class, "DataCache.db")
                            .addCallback(new DataCacheCallback())
                            .addMigrations(MIGRATION_1_2, MIGRATION_2_3)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Gets a constructed raw query to do a full text search in {@link Chat}.
     * Deals with emoticon single (e.g., :-D) and mixed (e.g., happy :-D) case.
     *
     * @param token Text to search for.
     * @return A proper FTS SQLite query where {@code token} is appended
     *         with "{@code *}" for partial token match.
     * @see androidx.room.RawQuery
     * @see <a href="https://sqlite.org/fts3.html#full_text_index_queries">
     *      SQLite FTS3 and FTS4 Extensions</a>
     */
    public static SupportSQLiteQuery getChatMatchQuery(String token) {
        // SQLite FTS quirks and match protection: special meaning for ":", "-", ")", ...
        SimpleSQLiteQuery query;
        Matcher emoticonMatcher = emoticonOnlyPattern.matcher(token);
        if (emoticonMatcher.matches()) {  // Emoticon-only case
            query = new SimpleSQLiteQuery(
                    "SELECT * FROM Chat WHERE text LIKE ?",
                    new Object[]{"%" + token + "%"});
        } else if (emoticonMatcher.find()) {  // Emoticon mixed case
            query = new SimpleSQLiteQuery(
                    "SELECT * FROM Chat WHERE text LIKE ?",
                    new Object[]{"%" + token.replace(" ", "%") + "%"});
        } else {  // Only alphanumeric token, FTS may take from here
            query = new SimpleSQLiteQuery(
                    "SELECT * FROM Chat WHERE id IN " +
                            "(SELECT docid FROM Chat_fts WHERE text MATCH ?) ORDER BY id ASC",
                    new Object[]{sqliteFTSAvoidPattern.matcher(token).replaceAll("") + "*"});
        }
        return query;
    }

    /**
     * On create DataCache insert full text service virtual table, trigger
     * and rebuild.
     */
    public static class DataCacheCallback
            extends RoomDatabase.Callback {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            db.execSQL(CHAT_CREATE_FTS);
            db.execSQL(CHAT_CREATE_FTS_SYNC_TRIGGER);
            db.execSQL(CHAT_CREATE_FTS_REBUILD);
        }
    }
}
