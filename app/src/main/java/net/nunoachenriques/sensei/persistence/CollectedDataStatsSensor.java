/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.persistence;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Data holder helper class for SQLite (Room) persistence.
 * Use case: query total, total moment distinct, max moment, sensor type.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public class CollectedDataStatsSensor
        implements Serializable {

    public long total;
    public long total_events;
    public Timestamp last;
    public String name;

    CollectedDataStatsSensor() {}

    @NonNull
    @Override
    public String toString() {
        return "CollectedDataStatsSensor{" +
                "total=" + total +
                ", total_events=" + total_events +
                ", last=" + last +
                ", name=" + name +
                "}";
    }
}
