/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.persistence;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;

import java.sql.Timestamp;
import java.util.Arrays;

/**
 * The data cache DAO (set and more) service for the collected data.
 * Gets data from the intent, processes and stores it.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see DataCacheDao
 */
public class DataCacheDaoService
        extends IntentService {

    private static final String TAG = "DataCacheDaoService";

    public static final String EXTRA_CHAT_SET = TAG + "Chat_set";
    public static final String EXTRA_COLLECTED_SET = TAG + "Collected_set";
//    public static final String EXTRA_COLLECTED_RESET = TAG + "Collected_reset";
    public static final String EXTRA_ENTITY_SET = TAG + "Entity_set";
    public static final String EXTRA_ENTITY_RESET = TAG + "Entity_reset";
    public static final String EXTRA_PARAMETER_SET = TAG + "Parameter_set";
//    public static final String EXTRA_PARAMETER_RESET = TAG + "Parameter_reset";

    public DataCacheDaoService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent i) {
        if (i != null) {
            try {
                DataCacheDao dataCacheDao = DataCache.getInstance(this).getDao();
                if (i.hasExtra(EXTRA_CHAT_SET)) {
                    Chat[] c = (Chat[])i.getSerializableExtra(EXTRA_CHAT_SET);
                    dataCacheDao.set(c);
                    Log.d(TAG, "SET " + Arrays.toString(c));
                } else if (i.hasExtra(EXTRA_COLLECTED_SET)) {
                    Collected[] c = (Collected[])i.getSerializableExtra(EXTRA_COLLECTED_SET);
                    dataCacheDao.set(c);
                    Log.d(TAG, "SET " + Arrays.toString(c));
//                } else if (i.hasExtra(EXTRA_COLLECTED_RESET)) {
//                    Collected[] c = (Collected[])i.getSerializableExtra(EXTRA_COLLECTED_RESET);
//                    dataCacheDao.reset(c);
//                    Log.d(TAG, "RESET " + Arrays.toString(c));
                } else if (i.hasExtra(EXTRA_ENTITY_SET)) {
                    Entity[] e = (Entity[]) i.getSerializableExtra(EXTRA_ENTITY_SET);
                    dataCacheDao.set(e);
                    Log.d(TAG, "SET " + Arrays.toString(e));
                } else if (i.hasExtra(EXTRA_ENTITY_RESET)) {
                    Entity[] e = (Entity[])i.getSerializableExtra(EXTRA_ENTITY_RESET);
                    dataCacheDao.reset(e);
                    Log.d(TAG, "RESET " + Arrays.toString(e));
                } else if (i.hasExtra(EXTRA_PARAMETER_SET)) {
                    Parameter[] p = (Parameter[])i.getSerializableExtra(EXTRA_PARAMETER_SET);
                    dataCacheDao.set(p);
                    Log.d(TAG, "SET " + Arrays.toString(p));
//                } else if (i.hasExtra(EXTRA_PARAMETER_RESET)) {
//                    Parameter[] p = (Parameter[])i.getSerializableExtra(EXTRA_PARAMETER_RESET);
//                    dataCacheDao.reset(p);
//                    Log.d(TAG, "SET " + Arrays.toString(p));
                } else {
                    throw new Exception("Intent WITHOUT known EXTRA!");
                }
            } catch (Exception e) {
                Log.e(TAG, "DataCacheDaoService DATA DISCARDED! ", e);
            }
        }
    }

    /**
     * Get a new {@link Intent} for the {@link Parameter} only case: SET.
     */
    public static Intent getParameterIntent(Context ctx,
                                            Timestamp m, String mtz,
                                            String k, String v, boolean s) {
        return new Intent(ctx, DataCacheDaoService.class)
                .putExtra(EXTRA_PARAMETER_SET,
                        new Parameter[]{new Parameter(m, mtz, k, v, s)});
    }
}
