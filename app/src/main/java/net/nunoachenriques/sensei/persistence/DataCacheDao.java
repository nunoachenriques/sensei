/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.persistence;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.RawQuery;
import androidx.room.Update;
import androidx.sqlite.db.SupportSQLiteQuery;

import java.sql.Timestamp;
import java.util.List;

/**
 * The {@link DataCache} data access object. Contains all the SQL queries and
 * methods to interact with the local SQLite using Room.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
@Dao
public interface DataCacheDao {

    /*
     *                      C H A T   /   D I A R Y
     */

    @Insert
    void set(Chat... c);

    @Query("select text from Chat where moment = :moment limit 1")
    String getDiaryMessageText(Timestamp moment);

    @Query("select * from Chat order by moment asc")
    LiveData<List<Chat>> getDiaryMessageAll();

    /** @see DataCache#getChatMatchQuery(String) */
    @RawQuery(observedEntities = Chat.class)
    LiveData<List<Chat>> getDiaryMessageMatch(SupportSQLiteQuery query);

    /*
     *                        C O L L E C T E D
     */

    @Insert
    void set(Collected... c);

    @Update
    void reset(Collected... c);

    @Delete
    void delete(Collected... c);

    /** ALL collected synced and outdated */
    @Query("select * from Collected" +
            " where synced = 1 and moment < :before" +
            " order by id asc")
    List<Collected> getCollectedSyncedOutdated(Timestamp before);

    /** ALL (e.g., app.agent.empathy) synced and outdated records */
    @Query("select * from Collected" +
            " where synced = 1 and moment < :before" +
            " and sensorType = :sensorType" +
            " order by id asc")
    List<Collected> getCollectedSyncedOutdated(Timestamp before, String sensorType);

    /** ALL %.sensor.% synced and outdated records EXCEPT app.sensor.activity */
    @Query("select * from Collected" +
            " where synced = 1 and moment < :before" +
            " and sensorType like '%.sensor.%'" +
            " and sensorType not like 'app.sensor.activity'" +
            " order by id asc")
    List<Collected> getCollectedSyncedOutdatedSensor(Timestamp before);

    @Query("select * from Collected where synced = 0 order by id asc")
    List<Collected> getCollectedNotSynced();

    @Query("select sensorValueExtra from Collected" +
            " where moment = :moment" +
            " and sensorType = :sensorType" +
            " and sensorAttribute = :sensorAttribute" +
            " limit 1")
    String getCollectedExtra(Timestamp moment, String sensorType, String sensorAttribute);

    @Query("select moment, sensorValue as value from Collected" +
            " where sensorType = :sensorType" +
            " and sensorAttribute = :sensorAttribute" +
            " order by moment asc")
    List<CollectedMomentValue> getCollectedMomentValue(String sensorType, String sensorAttribute);

    @Query("select * from Collected" +
            " where sensorType = :sensorType" +
            " order by id desc" +
            " limit :limit")
    List<Collected> getCollectedLast(String sensorType, int limit);

    @Query("select * from Collected" +
            " where sensorAttribute = :sensorAttribute" +
            " order by id asc")
    List<Collected> getCollectedByAttribute(String sensorAttribute);

    // LIVE

    @Query("select one, two, three from" +
            " (select count(*) as one from Collected" +
            " where sensorType = :sensorType and sensorAttribute = :sensorAttribute and sensorValue = :sensorValue1)," +
            " (select count(*) as two from Collected" +
            " where sensorType = :sensorType and sensorAttribute = :sensorAttribute and sensorValue = :sensorValue2)," +
            " (select count(*) as three from Collected" +
            " where sensorType = :sensorType and sensorAttribute = :sensorAttribute and sensorValue = :sensorValue3)")
    LiveData<LongOneTwoThree> getCollectedCountOneTwoThreeLive(
            String sensorType, String sensorAttribute, double sensorValue1, double sensorValue2, double sensorValue3);

    @Query("select * from Collected" +
            " where sensorType = :sensorType" +
            " order by id asc")
    LiveData<List<Collected>> getCollectedLive(String sensorType);

    @Query("select * from Collected" +
            " where sensorType = :sensorType" +
            " order by id desc" +
            " limit :limit")
    LiveData<List<Collected>> getCollectedLastLive(String sensorType, int limit);

    @Query("select * from Collected" +
            " where sensorAttribute = :sensorAttribute" +
            " order by id asc")
    LiveData<List<Collected>> getCollectedByAttributeLive(String sensorAttribute);

    // STATISTICS

    @Query("select" +
            " total, total_synced, total_waiting," +
            " total_events, total_events_synced, total_events_waiting," +
            " first, last, duration" +
            " from" +
            " (select count(moment) as total from Collected)," +
            " (select count(moment) as total_synced from Collected where synced = 1)," +
            " (select count(moment) as total_waiting from Collected where synced = 0)," +
            " (select count(distinct(moment)) as total_events from Collected)," +
            " (select count(distinct(moment)) as total_events_synced from Collected where synced = 1)," +
            " (select count(distinct(moment)) as total_events_waiting from Collected where synced = 0)," +
            " (select min(moment) as first, max(moment) as last, max(moment) - min(moment) as duration from Collected)")
    CollectedDataStats getCollectedDataStats();

    @Query("select count(moment) as total, count(distinct(moment)) as total_events," +
            " max(moment) as last, sensorType as name" +
            " from Collected group by sensorType" +
            " union" +
            " select count(moment) as total, count(moment) as total_events," +
            " max(moment) as last, 'app.human.chat (diary)' as name" +
            " from Chat" +
            " order by sensorType asc")
    List<CollectedDataStatsSensor> getCollectedDataStatsSensor();

    /*
     *                           E N T I T Y
     */

    @Insert
    void set(Entity... e);

    @Update
    void reset(Entity... e);

    @Query("select * from Entity limit 1")
    Entity getEntity();

    /*
     *                        P A R A M E T E R
     */

    @Insert
    void set(Parameter... p);

    @Update
    void reset(Parameter... p);

    @Delete
    void delete(Parameter... p);

    @Query("select * from Parameter" +
            " where synced = 0" +
            " order by id asc")
    List<Parameter> getParameterNotSynced();

    @Query("select * from Parameter" +
            " where synced = 1 and moment < :before" +
            " group by `key` having count(`key`) > 1" +
            " order by id asc")
    List<Parameter> getParameterSyncedOutdated(Timestamp before);
}
