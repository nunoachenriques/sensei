/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.utility;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;

import androidx.preference.DialogPreference;

/**
 * Settings date preference type using ISO 8601.
 * It relies on a persistent value of {@link String} type.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see DatePreferenceDialogFragment
 */
public final class DatePreference
        extends DialogPreference {

    private static final String TAG = "DatePreference";

    private String dateValue;

    public DatePreference(Context c, AttributeSet as) {
        super(c, as);
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        Log.d(TAG, "onGetDefaultValue [" + a.getString(index) + "]");
        return a.getString(index);
    }

    @Override
    protected void onSetInitialValue(Object defaultValue) {
        setDate(getPersistedString((String)defaultValue));
        Log.d(TAG, "onSetInitialValue dateValue [" + dateValue + "] defaultValue [" + defaultValue + "]");
    }

    /**
     * Gets the date as a string from the current settings.
     *
     * @return string representation of the date.
     */
    public String getDate() {
        return dateValue;
    }

    /**
     * Stores the date as a string in the current settings.
     *
     * @param date String representation of the date to save.
     */
    public void setDate(String date) {
        // Adapted: https://stackoverflow.com/questions/4216082/how-to-use-datepickerdialog-as-a-preference#25581018
        final boolean wasBlocking = shouldDisableDependents();
        dateValue = date;
        persistString(date);
        final boolean isBlocking = shouldDisableDependents();
        if (isBlocking != wasBlocking) {
            notifyDependencyChange(isBlocking);
        }
        notifyChanged();
    }
}
