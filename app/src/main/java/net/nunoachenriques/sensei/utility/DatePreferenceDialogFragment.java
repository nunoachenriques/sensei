/*
 * Copyright 2020 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.utility;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;

import androidx.preference.PreferenceDialogFragmentCompat;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

/**
 * Settings date preference type using ISO 8601.
 * It relies on a persistent value of {@link String} type.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see DateTime
 */
public final class DatePreferenceDialogFragment
        extends PreferenceDialogFragmentCompat {

    private static final String TAG = "DatePreferenceDialogF";

    private String dateValue;
    private DatePicker datePicker;

    public static DatePreferenceDialogFragment newInstance(String key) {
        final DatePreferenceDialogFragment fragment = new DatePreferenceDialogFragment();
        final Bundle b = new Bundle(1);
        b.putString(ARG_KEY, key);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dateValue = ((DatePreference)getPreference()).getDate();
    }

    @Override
    protected View onCreateDialogView(Context context) {
        super.onCreateDialogView(context);
        datePicker = new DatePicker(context);
        datePicker.setFirstDayOfWeek(Calendar.MONDAY);
        datePicker.setMaxDate(System.currentTimeMillis());
        return datePicker;
    }

    @Override
    protected void onBindDialogView(View v) {
        super.onBindDialogView(v);
        if (dateValue == null) { // DEFAULT DATE FOR PICKER DIALOG: 2000-01-01
            datePicker.updateDate(2000, 0, 1);
            Log.d(TAG, "onBindDialogView dateValue [" + dateValue + "] default [2000-01-01]");
        } else {
            String[] da = dateValue.split(DateTime.DATE_SEPARATOR);
            datePicker.updateDate(Integer.parseInt(da[0]), Integer.parseInt(da[1]) - 1, Integer.parseInt(da[2]));
            Log.d(TAG, "onBindDialogView dateValue [" + dateValue + "] da[] [" + Arrays.toString(da) + "]");
        }
    }

    @Override
    public void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            dateValue = String.format(Locale.ROOT, "%4d-%02d-%02d",
                    datePicker.getYear(), (datePicker.getMonth() + 1), datePicker.getDayOfMonth());
            final DatePreference datePreference = (DatePreference) getPreference();
            if (datePreference.callChangeListener(dateValue)) {
                datePreference.setDate(dateValue);
                Log.d(TAG, "onDialogClosed dateValue [" + dateValue + "] persisted!");
            }
        }
    }
}
