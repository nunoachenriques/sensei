/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.utility;

/**
 * Calculations utilities (e.g., codomain conversions).
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class Calc {

    private Calc() {}

    /**
     * Converts a value from a codomain {@code A} to a codomain {@code B}.
     * Example: 25 in [0,100] converts to -0.5 in [-1,1].
     * NOTICE: does not check for singularity codomain, i.e., inferior limit
     * equals superior limit.
     *
     * @param v Current value.
     * @param vMin Current value's codomain inferior limit.
     * @param vMax Current value's codomain superior limit.
     * @param nMin New codomain inferior limit.
     * @param nMax New codomain superior limit.
     * @return Value {@code v} converted to the new range.
     */
    public static double convertCodomain(double v, double vMin, double vMax, double nMin, double nMax) {
        return (v - vMin) / (vMax - vMin) * (nMax - nMin) + nMin;
    }

    /**
     * Converts a floating-point {@code double} given value to an integer
     * using {@code (int)Math.floor(d + 0.5d)}. NOTICE: the integer part of the
     * given floating-point value must be less than {@link Integer#MAX_VALUE}.
     *
     * @param d A floating-point {@code double} value.
     * @return The floating-point value {@code d} rounded to the nearest integer.
     * @see Math#round(double)
     */
    public static int round(double d) {
        return (int)Math.floor(d + 0.5d);
    }

    /**
     * Finds the index of the maximum value in the given array of values.
     *
     * @param values The array of values to search for the maximum.
     * @return The index of the maximum value.
     */
    public static int argMax(double[] values) {
        int maxIndex = 0;
        for (int i = 0; i < values.length; i++) {
            if (values[maxIndex] < values[i]) {
                maxIndex = i;
            }
        }
        return maxIndex;
    }
}
