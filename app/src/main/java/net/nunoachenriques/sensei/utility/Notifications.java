/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.utility;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.media.RingtoneManager;
import android.os.Build;

import androidx.core.app.NotificationCompat;

/**
 * Utility for the application default notifications used in the
 * Android's notification drawer. Notifications may be regular or heads up
 * (importance high) and persistable (ongoing without cancel option).
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class Notifications {

    private static final String CHANNEL_ID = "net.nunoachenriques.sensei.utility";

    private Notifications() {}

    /**
     * Gets a notification builder with default settings: default importance,
     * discreet (no lights, no sounds), notification drawer small icon is
     * missing (NOTICE: the caller MUST set small icon).
     *
     * Notice that the notification priority is set to default so it will not be
     * shown expanded at the top.
     *
     * It may be persistent ({@code ongoing}) or not and auto cancellable
     * ({@code cancel}) or not.
     *
     * It's Android 8.0+ notification channel compatible with badge disabled
     * and no lock screen visibility set (Android 8.0+). NOTICE: Channel name
     * equals {@link android.Manifest} label name.
     *
     * @param c Application context.
     * @param ongoing Ongoing enabled.
     * @param cancel Auto cancellable enabled.
     * @return A {@link NotificationCompat.Builder} object ready to set some
     *         content and action!
     * @see NotificationCompat.Builder
     * @see #get(Context, boolean, boolean, int, int, boolean, boolean)
     */
    public static NotificationCompat.Builder getDefault(Context c, boolean ongoing, boolean cancel) {
        int importance =
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
                        ? NotificationManager.IMPORTANCE_DEFAULT
                        : Integer.MIN_VALUE;
        return get(c,
                false,false,
                importance, NotificationCompat.PRIORITY_DEFAULT,
                ongoing, cancel);
    }

    /**
     * Gets a notification builder with heads up settings: high importance,
     * sound, notification drawer small icon is
     * missing (NOTICE: the caller MUST set small icon).
     *
     * NOTICE that the notification priority is set to maximum to be shown
     * expanded at the top.
     *
     * It may be persistent ({@code ongoing}) or not and auto cancellable
     * ({@code cancel}) or not.
     *
     * It's Android 8.0+ notification channel compatible with badge disabled
     * and no lock screen visibility set (Android 8.0+). NOTICE: Channel name
     * equals {@link android.Manifest} label name.
     *
     * @param c Application context.
     * @param ongoing Ongoing enabled.
     * @param cancel Auto cancellable enabled.
     * @return A {@link NotificationCompat.Builder} object ready to set some
     *         content and action!
     * @see NotificationCompat.Builder
     * @see #get(Context, boolean, boolean, int, int, boolean, boolean)
     */
    public static NotificationCompat.Builder getHeadsUp(Context c, boolean ongoing, boolean cancel) {
        int importance =
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
                        ? NotificationManager.IMPORTANCE_HIGH
                        : Integer.MIN_VALUE;
        return get(c,
                true,true,
                importance, NotificationCompat.PRIORITY_MAX,
                ongoing, cancel)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
    }

    /**
     * Gets the common notification settings, i.e., alert only once,
     * priority level, no color, notification drawer small icon is missing
     * (NOTICE: the caller MUST set small icon).
     *
     * It's Android 8.0+ notification channel compatible with badge disabled
     * and lock screen visibility set (Android 8.0+). NOTICE: Channel name
     * equals {@link android.Manifest} label name.
     *
     * @param c Application context.
     * @param lights Lights enabled.
     * @param vibration Vibration enabled.
     * @param importance Channel importance level (Android 8.0+).
     * @param priority Notification priority level.
     * @param ongoing Ongoing enabled.
     * @param cancel Auto cancellable enabled.
     * @return A {@link NotificationCompat.Builder} object ready to set some content and action!
     * @see NotificationCompat.Builder#setSmallIcon(int)
     */
    private static NotificationCompat.Builder get(Context c,
                                                  boolean lights, boolean vibration,
                                                  int importance, int priority,
                                                  boolean ongoing, boolean cancel) {
        /* Android Oreo (>= 8.0): (new) channel is mandatory! */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager nm = (NotificationManager)c.getSystemService(Context.NOTIFICATION_SERVICE);
            if (nm != null) {
                NotificationChannel nc =
                        new NotificationChannel(CHANNEL_ID, c.getString(c.getApplicationInfo().labelRes), importance);
                nc.setShowBadge(false);
                nc.enableLights(lights);
                nc.enableVibration(vibration);
                nc.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                nm.createNotificationChannel(nc);
            }
        }
        return new NotificationCompat.Builder(c, CHANNEL_ID)
                .setOnlyAlertOnce(true)
                .setOngoing(ongoing)
                .setAutoCancel(cancel)
                .setPriority(priority)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
    }
}
