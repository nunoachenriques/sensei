/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.utility;

import android.content.Context;
import android.os.PowerManager;

/**
 * Availability utilities (e.g., if app is awake and interacting).
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class Availability {

    private Availability() {}

    /**
     * Checks if app is not in interactive mode, i.e., is dozing or asleep,
     * at least with screen turned off (black).
     *
     * @param c The app context.
     * @return True if it isn't in interactive mode, false otherwise.
     */
    public static boolean isAtRest(Context c) {
        final PowerManager pm = (PowerManager)c.getSystemService(Context.POWER_SERVICE);
        return pm != null && !pm.isInteractive();
    }
}
