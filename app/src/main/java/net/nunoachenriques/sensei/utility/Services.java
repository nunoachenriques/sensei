/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.utility;

import android.app.ActivityManager;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import java.util.List;

/**
 * Utilities for service management such as starting and stopping persistent
 * {@code Service}, scheduling and cancelling {@code JobService}.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class Services {

    private static final String TAG = "Services";

    private Services() {}

    /*
     *                               J O B
     */

    /**
     * Schedules a periodic {@link android.app.job.JobService} with common
     * requirements and persisted. Moreover, device charging never required.
     *
     * @param c Android application context.
     * @param js JobService scheduler.
     * @param cl Class which implements the JobService.
     * @param jid Unique Job id.
     * @param period Periodic interval in milliseconds.
     * @param rIdle Requires device idle.
     * @param rWifi Requires WiFi.
     * @see #getJobInfoBuilderCommon(Context, Class, int, boolean, boolean, boolean)
     */
    public static void schedulePeriodicJobService(Context c, JobScheduler js, Class<?> cl,
                                                  int jid, long period, boolean rIdle, boolean rWifi) {
        JobInfo ji = getJobInfoBuilderCommon(c, cl, jid, rIdle, false, rWifi)
                .setPeriodic(period)
                .setPersisted(true)
                .build();
        scheduleJobService(js, jid, ji);
    }

    /**
     * Schedules a one time {@link android.app.job.JobService} with common
     * requirements, latency, delay, and not persisted. Moreover, device idle
     * and charging never required.
     *
     * @param c Android application context.
     * @param js JobService scheduler.
     * @param cl Class which implements the JobService.
     * @param jid Unique Job id.
     * @param minDelay Minimum latency before considered for execution
     *                 (milliseconds).
     * @param maxDelay Maximum latency (delay in milliseconds) before
     *                 execution.
     * @param rWifi Requires WiFi.
     * @see #getJobInfoBuilderCommon(Context, Class, int, boolean, boolean, boolean)
     */
    @SuppressWarnings("SameParameterValue")
    public static void scheduleOneTimeJobService(Context c, JobScheduler js, Class<?> cl,
                                                 int jid, long minDelay, long maxDelay, boolean rWifi) {
        JobInfo ji = getJobInfoBuilderCommon(c, cl, jid, false, false, rWifi)
                .setMinimumLatency(minDelay)
                .setOverrideDeadline(maxDelay)
                .build();
        scheduleJobService(js, jid, ji);
    }

    /**
     * Checks if the Job identified by {@code jid} is scheduled.
     *
     * @param js Scheduler used to start the job.
     * @param jid The unique job identification used for scheduling.
     * @return True if already scheduled, false otherwise.
     */
    public static boolean isJobScheduled(JobScheduler js, int jid) {
        for (JobInfo ji : js.getAllPendingJobs()) {
            if (ji.getId() == jid) {
                return true;
            }
        }
        return false;
    }

    /**
     * Stops a {@link android.app.job.JobService}.
     *
     * @param js Scheduler used to start the job.
     * @param jid The unique job identification used for scheduling.
     */
    public static void stopJobService(JobScheduler js, int jid) {
        try {
            js.cancel(jid);
            Log.i(TAG, "JobService [" + jid + "] STOPPED!");
        } catch (NullPointerException e) {
            Log.e(TAG, "stopJobService FAILED! Hint: js is null?", e);
        }
    }

    /**
     * Gets the common requirements, i.e., the application and user settings
     * (e.g., network type) for all the scheduled
     * {@link android.app.job.JobService}.
     *
     * @param c Android application context.
     * @param cl Class which implements the JobService.
     * @param jid Unique Job id.
     * @param rIdle Requires device idle.
     * @param rCharging Requires charging.
     * @param rWifi Requires WiFi.
     * @return A {@link JobInfo.Builder} with the common requirements for
     *         every scheduled {@link android.app.job.JobService}.
     */
    @SuppressWarnings("SameParameterValue")
    private static JobInfo.Builder getJobInfoBuilderCommon(Context c, Class<?> cl, int jid,
                                                           boolean rIdle, boolean rCharging, boolean rWifi) {
        return new JobInfo.Builder(jid, new ComponentName(c, cl))
                .setRequiredNetworkType(rWifi ? JobInfo.NETWORK_TYPE_UNMETERED : JobInfo.NETWORK_TYPE_ANY)
                .setRequiresDeviceIdle(rIdle)
                .setRequiresCharging(rCharging);
    }

    /**
     * Schedules a {@link android.app.job.JobService}.
     *
     * @param js Scheduler used to start the job.
     * @param jid The unique job identification used for scheduling.
     * @param ji The job parameters information.
     */
    private static void scheduleJobService(JobScheduler js, int jid, JobInfo ji) {
        ComponentName cn = ji.getService();
        if (js.schedule(ji) != JobScheduler.RESULT_SUCCESS) {
            Log.e(TAG, "JobService [" + jid + "] FAILED to schedule! "
                    + cn.getClassName() + " [" + System.identityHashCode(cn) + "]");
        } else {
            Log.i(TAG, "JobService [" + jid + "] SCHEDULED! "
                    + cn.getClassName() + " [" + System.identityHashCode(cn) + "]");
        }
    }

    /**
     * Checks if a specified class is a running service.
     *
     * @param am Activity manager.
     * @param serviceClass The class which implements the service.
     * @return True if the service is running, false otherwise.
     */
    public static boolean isServiceRunning(ActivityManager am, Class<?> serviceClass) {
        if (am != null) {
            List<ActivityManager.RunningServiceInfo> services = am.getRunningServices(Integer.MAX_VALUE);
            for (ActivityManager.RunningServiceInfo service : services) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    Log.i(TAG, "Service RUNNING [" + serviceClass.getName() + "]");
                    return true;
                }
            }
        } else {
            Log.e(TAG, "Service FAILED! ActivityManager IS NULL!");
            return false;
        }
        Log.i(TAG, "Service NOT RUNNING [" + serviceClass.getName() + "]");
        return false;
    }

    /*
     *                        P E R S I S T E N T
     */

    /**
     * Starts a persistent service.
     *
     * @param c Context of the service, i.e., app context.
     * @param i Intent data of the service to start.
     */
    public static void startPersistentService(Context c, Intent i) {
        try {
            ComponentName cn = (i != null ? i.getComponent() : null);
            if (cn == null) {
                throw new NullPointerException("i == null | i.getComponent() == null");
            }
            // Android Oreo (>= 8.0): (new) foreground is mandatory!
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                cn = c.startForegroundService(i);
            } else {
                cn = c.startService(i);
            }
            if (cn != null) {
                Log.i(TAG, cn.getClassName() + " [" + System.identityHashCode(cn) + "] STARTED!");
            } else {
                throw new NullPointerException("cn == null");
            }
        } catch (SecurityException e) {
            Log.e(TAG, "startPersistentService FAILED! Hint: permissions or service file not found?", e);
        } catch (IllegalStateException e) {
            Log.e(TAG, "startPersistentService FAILED! Hint: application not allowing in this state?", e);
        } catch (NullPointerException e) {
            Log.e(TAG, "startPersistentService FAILED! Hint: intent or component name null?", e);
        }
    }

    /**
     * Stops a persistent service.
     *
     * @param c Context of the service, i.e., app context.
     * @param i Intent data of the service to stop.
     */
    public static void stopPersistentService(Context c, Intent i) {
        try {
            ComponentName cn = (i != null ? i.getComponent() : null);
            if (cn == null) {
                throw new NullPointerException("i == null | i.getComponent() == null");
            }
            if (c.stopService(i)) {
                Log.i(TAG, cn.getClassName() + " [" + System.identityHashCode(cn) + "] STOPPED!");
            } else {
                Log.e(TAG, cn.getClassName() + " [" + System.identityHashCode(cn) + "] FAILED to STOP!");
            }
        } catch (SecurityException e) {
            Log.e(TAG, "stopPersistentService FAILED! Hint: permissions or service file not found?", e);
        } catch (IllegalStateException e) {
            Log.e(TAG, "stopPersistentService FAILED! Hint: application not allowing in this state?", e);
        } catch (NullPointerException e) {
            Log.e(TAG, "stopPersistentService FAILED! Hint: intent or component name null?", e);
        }
    }
}
