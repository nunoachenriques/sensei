/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.utility;

import java.security.MessageDigest;

/**
 * Cryptography utilities for things such as digest and similar on strings.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class Crypto {

    private Crypto() {}

    /**
     * Converts a digest message from a given {@code text} to an hexadecimal
     * character sequence representing the same digest result (in byte array).
     *
     * @param algorithm The algorithm to use in {@link MessageDigest}.
     * @param text The text to digest and convert.
     * @return An hexadecimal representation of the text digest result.
     * @throws Exception On failure just throws the cause.
     */
    public static String digestToHexString(String algorithm, String text)
            throws Exception {
        MessageDigest md = MessageDigest.getInstance(algorithm);
        StringBuilder sb = new StringBuilder(md.getDigestLength());
        for (byte b : md.digest(text.getBytes())) {
            if ((0xff & b) < 0x10) {
                sb.append('0');
            }
            sb.append(Integer.toHexString(0xff & b));
        }
        return sb.toString();
    }
}
