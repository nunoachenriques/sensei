/*
 * Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.utility;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * Useful to deal with files and JSON data in assets folder.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class Assets {

    private Assets() {}

    /**
     * Get JSON format text data stored in a file.
     *
     * @param c Android application context.
     * @param name The file name.
     * @return File contents as text.
     * @throws IOException on error.
     */
    public static String readJSONDataFromAsset(Context c, String name)
            throws IOException {
        try (InputStream is = c.getAssets().open(name)) {
            int size = is.available();
            byte[] buffer = new byte[size];
            if (is.read(buffer) != size) {
                throw new IOException("read size DIFFERS from available!");
            }
            return new String(buffer, StandardCharsets.UTF_8);
        }
    }
}
