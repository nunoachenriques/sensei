/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

/**
 * The connectivity utility class. To help on statically checking for
 * boolean-type (true/false) results, and also getting some ad hoc networking
 * information.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class Connectivity {

    private Connectivity() {}

    /**
     * Gets the WiFi access point identifiers, i.e., the BSSID in the form of
     * a six-byte MAC address: XX:XX:XX:XX:XX:XX and the SSID usually in
     * a natural language name (double quotes will be stripped out).
     *
     * @param c Android application {@link Context}
     * @return An array with BSSID or [{@code null}, SSID].
     *         [{@code null}, {@code null}] when there's no connection.
     * @see WifiInfo#getBSSID()
     * @see WifiInfo#getSSID()
     */
    public static String[] getWiFiId(Context c) {
        String[] s = new String[]{null, null};
        WifiManager wm = (WifiManager)c.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wm != null) {
            WifiInfo wi = wm.getConnectionInfo();
            s[0] = wi.getBSSID();
            s[1] = wi.getSSID().replace("\"", "");
        }
        return s;
    }

    /**
     * Checks if connected or connecting to an existing active network.
     * Additionally, checks for WiFi requirement status.
     *
     * @param c Android application {@link Context}
     * @param wifiRequired If WiFi is required or not.
     * @return True if it's connected and fulfills requirements, false otherwise.
     */
    public static boolean isConnectionOk(Context c, boolean wifiRequired) {
        ConnectivityManager cm = (ConnectivityManager)c.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo ni = cm.getActiveNetworkInfo();
            if (ni != null && ni.isConnectedOrConnecting()) {
                return !wifiRequired || (ni.getType() == ConnectivityManager.TYPE_WIFI);
            }
        }
        return false;
    }
}
