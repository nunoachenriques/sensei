/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.utility;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

/**
 * The app alert-type dialog fragment. Shows an alert dialog filled with
 * content, i.e., sets the icon, title, main text, and with one to three
 * buttons configured as positive-only, positive and negative, positive and
 * neutral and negative, and specific actions to call when hit.
 * All is provided by the caller. Usage inside an {@link android.app.Activity}
 * and one button only:
 * <pre>{@code
 * ...
 * AlertDialogFragment.newInstance(
 *         R.drawable.ic_question_mark,
 *         R.string.help_title,
 *         Html.fromHtml(res.getString(R.string.help_message)),
 *         R.string.dialog_ok,
 *         new DialogInterface.OnClickListener() {
 *            @literal @Override
 *             public void onClick(DialogInterface dialog, int whichButton) {
 *                 dialog.dismiss();
 *             }
 *         }
 * ).show(this.getSupportFragmentManager(), TAG);
 * ...}</pre>
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class AlertDialogFragment
        extends DialogFragment {

    private static final String TAG = "AlertDialogFragment";

    private static DialogInterface.OnClickListener positiveButtonListener;
    private static DialogInterface.OnClickListener neutralButtonListener;
    private static DialogInterface.OnClickListener negativeButtonListener;

    public AlertDialogFragment() {}

    /**
     * Get a new instance with the arguments provided,
     * i.e, one positive button.
     *
     * @param icon The dialog icon drawable resource id
     *             (e.g., R.drawable.ic_info).
     * @param title The dialog title string resource id
     *              (e.g., R.string.about_title).
     * @param message Dialog message which may be a long text.
     * @param positiveButton A string resource id for the positive button.
     * @param posBListener Positive button listener.
     * @return An instance of the alert dialog fragment with one button.
     * @see AlertDialog
     */
    public static AlertDialogFragment newInstance(
            int icon, int title, CharSequence message,
            int positiveButton, DialogInterface.OnClickListener posBListener) {
        return newInstance(
                icon, title, message,
                positiveButton, posBListener,
                0, null,
                0, null);
    }

    /**
     * Get a new instance with the arguments provided,
     * i.e., positive and negative buttons.
     *
     * @param icon The dialog icon drawable resource id
     *             (e.g., R.drawable.ic_info).
     * @param title The dialog title string resource id
     *              (e.g., R.string.about_title).
     * @param message Dialog message which may be a long text.
     * @param positiveButton A string resource id for the positive button.
     * @param posBListener Positive button listener.
     * @param negativeButton A string resource id for the negative button.
     * @param negBListener Negative button listener.
     * @return An instance of the alert dialog fragment with two buttons.
     * @see AlertDialog
     */
    public static AlertDialogFragment newInstance(
            int icon, int title, CharSequence message,
            int positiveButton, DialogInterface.OnClickListener posBListener,
            int negativeButton, DialogInterface.OnClickListener negBListener) {
        return newInstance(
                icon, title, message,
                positiveButton, posBListener,
                0, null,
                negativeButton, negBListener);
    }

    /**
     * Gets a new instance with the arguments provided,
     * i.e., positive, neutral, and negative buttons.
     *
     * @param icon The dialog icon drawable resource id
     *             (e.g., R.drawable.ic_info).
     * @param title The dialog title string resource id
     *              (e.g., R.string.about_title).
     * @param message Dialog message which may be a long text.
     * @param positiveButton A string resource id for the positive button.
     * @param posBListener Positive button listener.
     * @param neutralButton A string resource id for the neutral button.
     * @param neuBListener Neutral button listener.
     * @param negativeButton A string resource id for the negative button.
     * @param negBListener Negative button listener.
     * @return An instance of the alert dialog fragment with three buttons.
     * @see AlertDialog
     */
    @SuppressWarnings("WeakerAccess")
    public static AlertDialogFragment newInstance(
            int icon, int title, CharSequence message,
            int positiveButton, DialogInterface.OnClickListener posBListener,
            int neutralButton, DialogInterface.OnClickListener neuBListener,
            int negativeButton, DialogInterface.OnClickListener negBListener) {
        AlertDialogFragment adf = new AlertDialogFragment();
        Bundle args = new Bundle();
        args.putInt("icon", icon);
        args.putInt("title", title);
        args.putCharSequence("message", message);
        args.putInt("positiveButton", positiveButton);
        args.putInt("neutralButton", neutralButton);
        args.putInt("negativeButton", negativeButton);
        adf.setArguments(args);
        positiveButtonListener = posBListener;
        neutralButtonListener = neuBListener;
        negativeButtonListener = negBListener;
        return adf;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        try {
            Bundle args = getArguments();
            if (args == null) {
                throw new java.lang.NullPointerException("NO args to create DIALOG!");
            }
            final FragmentActivity activity = getActivity();
            if (activity == null) {
                throw new java.lang.NullPointerException("NO available ACTIVITY to create DIALOG!");
            }
            AlertDialog.Builder adb = new AlertDialog.Builder(activity)
                    .setIcon(args.getInt("icon"))
                    .setTitle(args.getInt("title"))
                    .setMessage(args.getCharSequence("message"));
            if (positiveButtonListener != null) {
                adb.setPositiveButton(args.getInt("positiveButton"), positiveButtonListener);
            }
            if (neutralButtonListener != null) {
                adb.setNeutralButton(args.getInt("neutralButton"), neutralButtonListener);
            }
            if (negativeButtonListener != null) {
                adb.setNegativeButton(args.getInt("negativeButton"), negativeButtonListener);
            }
            return adb.create();
        } catch (Exception e) {
            Log.d(TAG, "onCreateDialog FAILED!", e);
            return new AlertDialog.Builder(getContext()).create();
        }
    }
}
