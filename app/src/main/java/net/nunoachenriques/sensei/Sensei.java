/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import java.util.concurrent.TimeUnit;

/**
 * The application's common and critical definitions such as values to identify,
 * start, stop, regulate with boundaries.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
final class Sensei {

    /** Application start services intent action id. DO NOT CHANGE VALUE! */
    static final String ACTION_START = "net.nunoachenriques.sensei.intent.action.START";

    /** Application stop services intent action id. DO NOT CHANGE VALUE! */
    static final String ACTION_STOP = "net.nunoachenriques.sensei.intent.action.STOP";

    /** Days of {@link net.nunoachenriques.sensei.persistence.Collected} data retention. */
    static final int CACHE_COLLECTED_DAYS = 28;

    /** Days of {@link net.nunoachenriques.sensei.persistence.Parameter} data retention. */
    static final int CACHE_PARAMETER_DAYS = 28;

    /*
     * CACHE_ENTITY_DAYS = infinity
     * Kept during agent's lifetime (while installed)
     * CACHE_CHAT/DIARY_DAYS = infinity
     * Kept during agent's lifetime (while installed)
     */

    /** JobScheduler persistent id. DO NOT CHANGE VALUE! */
    static final int EXPANSE_SERVICE_JOB_ID = 10;

    /** JobScheduler persistent id. DO NOT CHANGE VALUE! */
    static final int HOMEOSTASIS_SERVICE_JOB_ID = 80;

    /** JobScheduler persistent id. DO NOT CHANGE VALUE! */
    static final int HOMEOSTASIS_SERVICE_NOW_JOB_ID = 88;

    /** Maximum minutes without data from a sensor before force accepting next. */
    @SuppressWarnings("WeakerAccess") static final long SENSOR_DATA_MISSING_ELAPSED_MINUTES_MAX = 15;

    /** Maximum milliseconds without data from a sensor before force accepting next. */
    static final long SENSOR_DATA_MISSING_ELAPSED_MILLIS_MAX =
            TimeUnit.MINUTES.toMillis(SENSOR_DATA_MISSING_ELAPSED_MINUTES_MAX);

    /** Maximum nanoseconds without data from a sensor before force accepting next. */
    static final long SENSOR_DATA_MISSING_ELAPSED_NANOS_MAX =
            TimeUnit.MINUTES.toNanos(SENSOR_DATA_MISSING_ELAPSED_MINUTES_MAX);
}
