/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityRecognitionClient;
import com.google.android.gms.location.ActivityTransition;
import com.google.android.gms.location.ActivityTransitionRequest;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * The activity transition (e.g., bike, car, running) feed listener.
 * Catches data from listening Google activity recognition transition API
 * and sends it to the service for processing and storage.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see ActivityService
 */
public final class ActivityFeed {

    public static final String SENSOR_ID = "app.sensor.activity";
    static final int SENSOR_FIXED_ATTRS = 3;
    static final String SENSOR_ATTR_TYPE = "type";
    static final String SENSOR_ATTR_TRANSITION = "transition";
    static final String SENSOR_ATTR_ELAPSED = "elapsed";

    private static final List<ActivityTransition> allTransitions =
            new ArrayList<ActivityTransition>(10) {{
                add(new ActivityTransition.Builder()
                        .setActivityType(DetectedActivity.IN_VEHICLE)
                        .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                        .build());
                add(new ActivityTransition.Builder()
                        .setActivityType(DetectedActivity.IN_VEHICLE)
                        .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                        .build());
                add(new ActivityTransition.Builder()
                        .setActivityType(DetectedActivity.ON_BICYCLE)
                        .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                        .build());
                add(new ActivityTransition.Builder()
                        .setActivityType(DetectedActivity.ON_BICYCLE)
                        .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                        .build());
                add(new ActivityTransition.Builder()
                        .setActivityType(DetectedActivity.RUNNING)
                        .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                        .build());
                add(new ActivityTransition.Builder()
                        .setActivityType(DetectedActivity.RUNNING)
                        .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                        .build());
                add(new ActivityTransition.Builder()
                        .setActivityType(DetectedActivity.STILL)
                        .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                        .build());
                add(new ActivityTransition.Builder()
                        .setActivityType(DetectedActivity.STILL)
                        .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                        .build());
                add(new ActivityTransition.Builder()
                        .setActivityType(DetectedActivity.WALKING)
                        .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                        .build());
                add(new ActivityTransition.Builder()
                        .setActivityType(DetectedActivity.WALKING)
                        .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                        .build());
            }};

    private static volatile ActivityFeed INSTANCE;

    private final PendingIntent activityPendingIntent;
    private final ActivityRecognitionClient activityRecognitionClient;
    private final ActivityTransitionRequest activityTransitionRequest;

    private ActivityFeed(Context c) {
        activityPendingIntent =
                PendingIntent.getService(c, 0, new Intent(c, ActivityService.class), 0);
        activityRecognitionClient = ActivityRecognition.getClient(c);
        activityTransitionRequest = new ActivityTransitionRequest(allTransitions);
    }

    /**
     * Gets an instance of this class. If no single instance is available, i.e.,
     * {@code null}, then creates and returns a single new one
     * (Singleton pattern).
     *
     * @param c Android application {@link Context}.
     * @return The single instance of this class.
     */
    public static ActivityFeed getInstance(Context c) {
        if (INSTANCE == null) {
            synchronized (ActivityFeed.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ActivityFeed(c);
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Starts (requests) receiving activity transition updates.
     *
     * @return a Task for the call, check {@link Task}.
     * @throws SecurityException On permission failure.
     */
    Task<Void> start()
            throws SecurityException {
        return activityRecognitionClient
                .requestActivityTransitionUpdates(activityTransitionRequest, activityPendingIntent);
    }

    /**
     * Stops listening activity transition updates.
     *
     * @return a Task for the call, check {@link Task}.
     */
    Task<Void> shutdown() {
        return activityRecognitionClient
                .removeActivityTransitionUpdates(activityPendingIntent);
    }
}
