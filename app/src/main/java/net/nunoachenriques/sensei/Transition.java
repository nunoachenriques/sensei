/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.Context;
import android.util.Log;

import net.nunoachenriques.sensei.persistence.Collected;
import net.nunoachenriques.sensei.persistence.DataCache;
import net.nunoachenriques.sensei.persistence.DataCacheDao;
import net.nunoachenriques.sensei.utility.DateTime;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The transition common utility. A transition is an entrance or exit
 * of some app activity (e.g., {@link ChartActivity}) by the user.
 * It has the purpose to catch and store all transitions to facilitate
 * accountability of user time in each activity.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
final class Transition {

    private static final String TAG = "Transition";

    /** Common name for all sensors' transition attribute. */
    private static final String SENSOR_ATTR_TRANSITION_COMMON = "transition";

    private static final int ENTER = 1;
    private static final int EXIT = 0;
    private static final Map<Integer, String> NAME =
            new HashMap<Integer, String>(2) {{
                put(ENTER, "ENTER");
                put(EXIT, "EXIT");
            }};

    private static volatile DataCacheDao dataCacheDao;
    private static volatile ExecutorService executorService = Executors.newSingleThreadExecutor();

    private final String sensorId;
    private Collected lastTransition;

    /**
     * Constructor with required arguments.
     *
     * @param ctx The app (activity) context.
     * @param sid The sensor identifier, i.e., id of the activity
     *            (e.g., {@link ChartActivity#SENSOR_ID})
     */
    Transition(Context ctx, String sid) {
        sensorId = sid;
        lastTransition = null;
        if (dataCacheDao == null) {
            dataCacheDao = DataCache.getInstance(ctx).getDao();
        }
    }

    /**
     * Update the status to ENTER and stores it.
     */
    void setEnter() {
        updateStatus(ENTER);
    }

    /**
     * Update the status to EXIT and stores it.
     */
    void setExit() {
        updateStatus(EXIT);
    }

    private void updateStatus(int status) {
        final Collected transition = new Collected(
                new Timestamp(System.currentTimeMillis()), DateTime.getTimeZoneISO8601(),
                sensorId, SENSOR_ATTR_TRANSITION_COMMON, status, NAME.get(status), false);
        try {
            if (executorService.isShutdown()) {
                executorService = Executors.newSingleThreadExecutor();
            }
            executorService.execute(() -> {
                dataCacheDao.set(transition); // Database insert
                Log.d(TAG, "Activity TRANSITION INSERT in DB OK!");
            });
        } catch (Exception e) {
            Log.e(TAG, "Activity TRANSITION INSERT in DB FAILED!");
        }
        long duration = 0;
        if (lastTransition != null
                && lastTransition.sensorValue.intValue() == ENTER
                && status == EXIT) {
            duration = transition.moment.getTime() - lastTransition.moment.getTime();
        }
        lastTransition = transition;
        Log.d(TAG, "TRANSITION [" + NAME.get(status) + "]" +
                (duration > 0
                        ? " duration [" + duration + "] ms in " + sensorId
                        : ""));
    }
}
