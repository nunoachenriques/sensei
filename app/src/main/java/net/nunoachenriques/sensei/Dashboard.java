/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import java.util.ArrayList;
import java.util.List;

/**
 * The Dashboard (information up-to-date) helper class. Prepares the dashboard
 * units default data to be used by the view widgets.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class Dashboard {

    private static volatile Dashboard INSTANCE;

    private final List<DashboardUnit> dashboardUnits;

    /**
     * Sets the dashboard units.
     */
    private Dashboard() {
        dashboardUnits = new ArrayList<DashboardUnit>() {{
            add(new DashboardUnit(R.id.dashboard_you_feel, DashboardUnit.TITLE_PROGRESS_IMAGE));
            add(new DashboardUnit(R.id.dashboard_you_feel_counters, DashboardUnit.TITLE_VALUE_HISTOGRAM));
            add(new DashboardUnit(R.id.dashboard_me_empathy, DashboardUnit.TITLE_VALUE_PROGRESS));
            add(new DashboardUnit(R.id.dashboard_you_activity, DashboardUnit.TITLE_VALUE_IMAGE));
            add(new DashboardUnit(R.id.dashboard_you_activity_still, DashboardUnit.TITLE_VALUE_IMAGE));
            add(new DashboardUnit(R.id.dashboard_you_activity_walking, DashboardUnit.TITLE_VALUE_IMAGE));
            add(new DashboardUnit(R.id.dashboard_you_activity_running, DashboardUnit.TITLE_VALUE_IMAGE));
            add(new DashboardUnit(R.id.dashboard_you_activity_bicycle, DashboardUnit.TITLE_VALUE_IMAGE));
            add(new DashboardUnit(R.id.dashboard_you_activity_vehicle, DashboardUnit.TITLE_VALUE_IMAGE));
        }};
    }

    /**
     * Gets an instance of this class. If no single instance is available, i.e.,
     * {@code null}, then creates and returns a single new one
     * (Singleton pattern).
     *
     * @return The single instance of this class.
     */
    public static Dashboard getInstance() {
        if (INSTANCE == null) {
            synchronized (Dashboard.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Dashboard();
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Gets the Dashboard units.
     *
     * @return The Dashboard units list.
     */
    List<DashboardUnit> getUnits() {
        return dashboardUnits;
    }
}
