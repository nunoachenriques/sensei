/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;

import androidx.annotation.ColorInt;

/**
 * A {@link View} for the {@link HistogramCircularDrawable}.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see HistogramCircularDrawable
 */
public final class HistogramCircularView
        extends View {

    private final HistogramCircularDrawable histogram;

    public HistogramCircularView(Context c, AttributeSet as) {
        super(c, as);
        histogram = new HistogramCircularDrawable(3);
    }

    @Override
    protected void onDraw(Canvas c) {
        super.onDraw(c);
        histogram.setBounds(0, 0, getWidth() - 1, getHeight() - 1);
        histogram.draw(c);
    }

    /**
     * Sets a list of ({@code bin}, {@code value}) pairs.
     *
     * @param binValuePairs Bin (Pair.first) zero-based index [0, n] and the bin's count value (Pair.second).
     */
    public void setValues(Iterable<Pair<Integer, Long>> binValuePairs) {
        histogram.setValues(binValuePairs);
        invalidate();
    }

    /**
     * Sets the {@code bin} with the given color.
     *
     * @param bin Zero-based index, i.e., [0, n].
     * @param c {@link Color} integer value.
     */
    public void setColor(int bin, @ColorInt int c) {
        histogram.setColor(bin, c);
        invalidate();
    }
}
