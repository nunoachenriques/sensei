/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import net.nunoachenriques.sensei.emotion.Polarity;
import net.nunoachenriques.sensei.persistence.Collected;
import net.nunoachenriques.sensei.persistence.DataCache;
import net.nunoachenriques.sensei.persistence.DataCacheDao;
import net.nunoachenriques.sensei.persistence.LongOneTwoThree;
import net.nunoachenriques.sensei.settings.LivePreferenceBoolean;
import net.nunoachenriques.sensei.settings.LivePreferenceDouble;

import java.util.List;

/**
 * The dashboard (information up-to-date) view model. Gets live data from
 * specific sources (e.g., collected from sensors) able to be fetched by the
 * dashboard view widgets.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see AndroidViewModel
 */
public final class DashboardViewModel
        extends AndroidViewModel {

    private final DataCacheDao dataCacheDao;

    private final LivePreferenceDouble empathyLive;
    private final LivePreferenceBoolean onPauseLive;

    public DashboardViewModel(@NonNull Application a) {
        super(a);
        final Context c = a.getApplicationContext();
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        dataCacheDao = DataCache.getInstance(c).getDao();
        empathyLive = new LivePreferenceDouble(sharedPreferences,
                Empathy.EMPATHY_PREF_KEY, Double.NaN);
        onPauseLive = new LivePreferenceBoolean(sharedPreferences,
                a.getResources().getString(R.string.data_pause_mode_pref_key), false);
    }

    @SuppressWarnings("SameParameterValue")
    LiveData<List<Collected>> getActivityLast(int i) {
        return dataCacheDao
                .getCollectedLastLive(ActivityFeed.SENSOR_ID, i * ActivityFeed.SENSOR_FIXED_ATTRS);
    }

    LiveData<List<Collected>> getActivityAll() {
        return dataCacheDao.getCollectedLive(ActivityFeed.SENSOR_ID);
    }

    LiveData<Double> getEmpathy() {
        return empathyLive;
    }

    LiveData<Boolean> getOnPause() {
        return onPauseLive;
    }

    LiveData<List<Collected>> getPolarityAll() {
        return dataCacheDao.getCollectedByAttributeLive(Polarity.SENSOR_ATTR_POLARITY_COMMON);
    }

    LiveData<LongOneTwoThree> getSmileCount() {
        return dataCacheDao
                .getCollectedCountOneTwoThreeLive(
                        SmileReportService.SENSOR_ID, SmileReportService.SENSOR_ATTR_POLARITY,
                        SmileReportService.NEGATIVE, SmileReportService.NEUTRAL, SmileReportService.POSITIVE);
    }
}
