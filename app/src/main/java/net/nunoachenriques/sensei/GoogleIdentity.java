/*
 * Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import net.nunoachenriques.sensei.utility.Assets;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Google Identity Platform authentication and more utilities such as sign in.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see <a href="https://developers.google.com/identity/">Google Identity Platform</a>
 * @see <a href="https://gitlab.com/nunoachenriques/google-sign-in">Google sign in demo</a>
 */
final class GoogleIdentity {

    private static final String TAG = "GoogleIdentity";
    private static final String WEB_APP_CREDENTIALS_FILE_NAME = "sensai_expanse_credentials.json.secret";

    private static volatile JSONObject credentials = null;

    private GoogleIdentity() {}

    /**
     * Get the Google sign in client with email and id token request options.
     *
     * @param c The Android application context.
     * @return The Google sign in client to use.
     */
    static GoogleSignInClient getSignInClient(Context c) {
        try {
            if (credentials == null) {
                Log.d(TAG, "getSignInClient file name: " + WEB_APP_CREDENTIALS_FILE_NAME);
                credentials = new JSONObject(
                        Assets.readJSONDataFromAsset(c, WEB_APP_CREDENTIALS_FILE_NAME));
            }
            return getGoogleSignInClient(c, credentials);
        } catch (Exception e) {
            Log.e(TAG, "getGoogleSignInClient FAILED: " + e.getMessage());
            return null;
        }
    }

    /**
     * Get the Google sign in client with email and id token request options.
     *
     * @param c The Android application context.
     * @param credentials JSON format text with credentials from
     *                    Google Identity Platform for Web app.
     * @return The Google sign in client to use.
     * @throws JSONException on {@code credentials} error.
     */
    private static GoogleSignInClient getGoogleSignInClient(Context c, JSONObject credentials)
            throws JSONException {
        final String clientId = getGoogleClientIdFrom(credentials);
        Log.d(TAG, "getGoogleSignInClient Web app (Expanse) client_id: " + clientId);
        final GoogleSignInOptions gso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(clientId)  // Web app (Expanse) client ID
                .build();
        return GoogleSignIn.getClient(c, gso);
    }

    /**
     * Get {@code client_id} code from Web app credentials JSON object.
     *
     * @param credentials JSON format credentials from Google Identity Platform.
     * @return {@code client_id} code.
     * @throws JSONException on error.
     */
    private static String getGoogleClientIdFrom(JSONObject credentials)
            throws JSONException {
        return credentials.getJSONObject("web").getString("client_id");
    }
}
