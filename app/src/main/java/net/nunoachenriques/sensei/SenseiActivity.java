/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.Manifest;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.Task;

import net.nunoachenriques.sensei.emotion.Polarity;
import net.nunoachenriques.sensei.persistence.Chat;
import net.nunoachenriques.sensei.persistence.DataCacheDaoService;
import net.nunoachenriques.sensei.persistence.Entity;
import net.nunoachenriques.sensei.persistence.Parameter;
import net.nunoachenriques.sensei.settings.SettingsActivity;
import net.nunoachenriques.sensei.utility.AlertDialogFragment;
import net.nunoachenriques.sensei.utility.Crypto;
import net.nunoachenriques.sensei.utility.DateTime;
import net.nunoachenriques.sensei.utility.Notifications;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * The application main activity and view layout. Starts all initial procedures
 * (e.g., launching activities and services, such as, sensors and Twitter feed)
 * and prepares for interaction. Loads the layout pieces, such as,
 * toolbar, dashboard. Authenticates user. Checks for the user agreement,
 * dangerous permissions, and entity.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class SenseiActivity
        extends SenseiAppCompatActivity
        implements SharedPreferences.OnSharedPreferenceChangeListener,
        ActivityCompat.OnRequestPermissionsResultCallback {

    private static final String TAG = "SenseiActivity";

    public static final String EXTRA_RESET_PERMISSIONS = TAG + "reset_permissions";
    public static final String EXTRA_UPGRADE = TAG + "upgrade";

    private static final String SENSOR_ID = "app.human.main";
    private static final int GOOGLE_PLAY_SERVICES_RC = 0x6006;
    private static final int HUMAN_SIGN_IN_RC = 33;
    private static final String HUMAN_ENTITY_DIGEST_ALGORITHM = "SHA-256";
    private static final int ACTIVITY_RECOGNITION_RC = 45;
    private static final List<String> DANGEROUS_PERMISSIONS = new ArrayList<String>() {{ // Android 9- API 28-
        add(Manifest.permission.ACCESS_FINE_LOCATION);
        //add(Manifest.permission.BODY_SENSORS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            add(Manifest.permission.FOREGROUND_SERVICE);
        }
        add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }};
    private static final int DANGEROUS_PERMISSIONS_RC = 0;
    private static final int GEOPOSITIONING_CHECK_SETTINGS_RC = 0x6E0;

    private static String[] identifiableDataKeys;
    private static boolean isThanksInChat = false;

    private Resources res;
    private SharedPreferences sharedPreferences;
    private int paddingSpace;
    private CardView diaryInputView;
    private Dialog googleDialog;
    private GoogleApiAvailability googleApi;
    private GoogleSignInClient googleSignInClient;
    private boolean agreementSet;
    private boolean activityRecognitionPermissionSet;
    private boolean dangerousPermissionsSet;
    private boolean entitySet;
    private SenseiStartStop senseiStartStop;
    private Transition transition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // CREATE + RESOURCES + VIEW
        super.onCreate(savedInstanceState);
        res = getResources();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        setupMainView();
        // REQUIRED: DATA PRIVACY, AGREEMENT, ENTITY, CRITICAL PERMISSIONS.
        identifiableDataKeys = new String[]{
                res.getString(R.string.human_email_pref_key),
                res.getString(R.string.human_name_pref_key),
                res.getString(R.string.human_entity_pref_key),
                res.getString(R.string.source_twitter_screen_name_pref_key)
        };
        googleApi = GoogleApiAvailability.getInstance();
        agreementSet = sharedPreferences.getBoolean(res.getString(R.string.app_agreement_pref_key), false);
        activityRecognitionPermissionSet = checkPermissionActivityRecognition();
        if (Build.VERSION.SDK_INT >= 29) { // Android 10+ API 29+
            DANGEROUS_PERMISSIONS.add(0, Manifest.permission.ACCESS_BACKGROUND_LOCATION);
        }
        dangerousPermissionsSet = checkPermissionList(DANGEROUS_PERMISSIONS.toArray(new String[0]));
        entitySet = (sharedPreferences.getString(res.getString(R.string.human_entity_pref_key), null) != null);
        googleSignInClient = GoogleIdentity.getSignInClient(this);
        if (googleSignInClient == null) {
            throw new RuntimeException("googleSignInClient is NULL! Hint: check assets files...");
        }
        senseiStartStop = new SenseiStartStop();
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(senseiStartStop, new IntentFilter(Sensei.ACTION_START));
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(senseiStartStop, new IntentFilter(Sensei.ACTION_STOP));
        transition = new Transition(this, SENSOR_ID);
    }

    @Override
    protected void onNewIntent(Intent i) {
        super.onNewIntent(i);
        if (i != null) {
            setIntent(i);
        }
        Log.d(TAG, "onNewIntent: " + i);
    }

    @Override
    protected void onResume() {
        super.onResume();
        transition.setEnter();
        // ON NEW INTENT (SINGLE_TOP)
        processIntent(getIntent());
        // REGULAR FLOW (REQUIREMENTS CASCADING)
        int googleResult = googleApi.isGooglePlayServicesAvailable(this);
        if (googleResult != ConnectionResult.SUCCESS) {
            Log.w(TAG, "Google Play Services NOT AVAILABLE: " + googleApi.getErrorString(googleResult));
            if (googleApi.isUserResolvableError(googleResult)) {
                Log.i(TAG, "Google Play Services DIALOG for the USER to RESOLVE!");
                googleDialog = googleApi.getErrorDialog(this, googleResult, GOOGLE_PLAY_SERVICES_RC);
                googleDialog.show();
            } else {
                Log.e(TAG, "Google Play Services FATAL FAILURE: " + googleApi.getErrorString(googleResult));
                exitWithMessage(res.getString(R.string.exit_play_services_required));
            }
        } else if (!agreementSet) {
            showAgreementDialog();
        } else if (!activityRecognitionPermissionSet) {
            if (android.os.Build.VERSION.SDK_INT >= 29) { // CHECK for Android 10+ API 29+
                ActivityCompat.requestPermissions(
                        this, new String[]{Manifest.permission.ACTIVITY_RECOGNITION}, ACTIVITY_RECOGNITION_RC);
                Log.i(TAG, "Permission [" + Manifest.permission.ACTIVITY_RECOGNITION + "] REQUESTED!");
            } else {
                Log.e(TAG, "Activity Recognition FATAL FAILURE: Permission NOT GRANTED!");
                exitWithMessage(res.getString(R.string.exit_activity_recognition_required));
            }
        } else if (!dangerousPermissionsSet) {
            requestPermissions(DANGEROUS_PERMISSIONS.toArray(new String[0]), DANGEROUS_PERMISSIONS_RC);
            Log.i(TAG, "Permissions [" + DANGEROUS_PERMISSIONS + "] REQUESTED!");
        } else if (!entitySet) {
            signInAutoRefresh();
        } else if (!isDemographicsSet()) {
            setDemographicsNotification();
            startActivity(new Intent(this, SettingsActivity.class));
        } else if (!SenseiStartStop.isHomeostasisJobScheduled()) {
            LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(Sensei.ACTION_START));
        } else { // IF SETUP OK: always CHECK for FUNCTIONAL requirements!
            signInAutoRefresh();
            requireLocationSettings();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        transition.setExit();
        if (googleDialog != null && googleDialog.isShowing()) {
            googleDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(senseiStartStop);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        if (key.equals(res.getString(R.string.app_agreement_pref_key))) {
            agreementSet = sp.getBoolean(key, false);
        } else if (key.equals(res.getString(R.string.human_entity_pref_key))) {
            entitySet = (sp.getString(key, null) != null);
        } else if (key.equals(res.getString(R.string.app_theme_dark_pref_key))) {
            recreate();
        } else if (key.equals(res.getString(R.string.human_birthday_pref_key)) || key.equals(res.getString(R.string.human_gender_pref_key))) {
            final String birthDatePref = sp.getString(res.getString(R.string.human_birthday_pref_key), null);
            final Date birthDate =
                    (birthDatePref == null)
                            ? null
                            : Date.valueOf(birthDatePref);
            final String genderPref =
                    (birthDatePref == null)
                            ? null
                            : sp.getString(res.getString(R.string.human_gender_pref_key), null);
            startService(new Intent(this, DataCacheDaoService.class)
                    .putExtra(DataCacheDaoService.EXTRA_ENTITY_RESET,
                            new Entity[]{new Entity(
                                    sp.getString(res.getString(R.string.human_entity_pref_key), null),
                                    birthDate, genderPref, false)}));
            // Thanks in the Chat/Diary
            if (!isThanksInChat && birthDate != null && genderPref != null) {
                final Timestamp ts = new Timestamp(System.currentTimeMillis());
                final String tz = DateTime.getTimeZoneISO8601();
                final String language = Locale.getDefault().getLanguage();
                final String name = sharedPreferences.getString(
                        res.getString(R.string.human_name_pref_key), null);
                startService(new Intent(this, DataCacheDaoService.class)
                        .putExtra(DataCacheDaoService.EXTRA_CHAT_SET,
                                new Chat[]{new Chat(ts, tz, Chat.AGENT,
                                        res.getString(R.string.interaction_thank_you, name),
                                        language, Polarity.NO_POLARITY)}));
                isThanksInChat = true;
            }
        }
        /*
         * Any PREFERENCE CHANGE must be collected for EXPANSE SYNC purposes!
         * EXCEPTION to "empathy" because is also a sensor and already
         * being collected as such! EXCEPTION also to identifiable data keys
         * in order to preserve anonymity.
         */
        if (!(key.equals(Empathy.EMPATHY_PREF_KEY) || isIdentifiableDataKey(key))) {
            Map<String, ?> spm = sharedPreferences.getAll();
            String v = String.valueOf(spm.get(key));
            if (!v.equals("null")) {
                startService(new Intent(this, DataCacheDaoService.class)
                        .putExtra(DataCacheDaoService.EXTRA_PARAMETER_SET,
                                new Parameter[]{new Parameter(
                                        new Timestamp(System.currentTimeMillis()), DateTime.getTimeZoneISO8601(),
                                        key, v, false)}));
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GOOGLE_PLAY_SERVICES_RC:
                Log.i(TAG, "Google Play Services RETURNED!");
                break;
            case HUMAN_SIGN_IN_RC:
                handleSignInResult(GoogleSignIn.getSignedInAccountFromIntent(data));
                break;
            case GEOPOSITIONING_CHECK_SETTINGS_RC:
                if (resultCode == RESULT_OK) {
                    Log.i(TAG, "GEOPOSITIONING settings OK!");
                } else {
                    Log.w(TAG, "LOOPING... Geopositioning CANCELLED!");
                }
                break;
        }
    }

    /*
     *                              D A T A
     */

    private boolean isIdentifiableDataKey(String k) {
        for (String i : identifiableDataKeys) {
            if (i.equals(k)) {
                return true;
            }
        }
        return false;
    }

    private boolean isDemographicsSet() {
        return sharedPreferences.getString(res.getString(R.string.human_birthday_pref_key), null) != null
                && sharedPreferences.getString(res.getString(R.string.human_gender_pref_key), null) != null;
    }

    private void setDemographicsNotification() {
        final NotificationManager nm = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        if (nm != null) {
            Notification n = Notifications.getHeadsUp(this, false, true)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentTitle(res.getString(R.string.demographics_notification_title))
                    .setContentText(res.getString(R.string.demographics_notification_text))
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(res.getString(R.string.demographics_notification_text_big)))
                    .setTimeoutAfter(TimeUnit.SECONDS.toMillis(3))
                    .build();
            nm.notify(0xDD, n);
        }
    }

    private void processIntent(Intent i) {
        if (i != null) {
            if (i.hasExtra(EXTRA_RESET_PERMISSIONS)) {
                dangerousPermissionsSet = !i.getBooleanExtra(EXTRA_RESET_PERMISSIONS, true);
                i.removeExtra(EXTRA_RESET_PERMISSIONS);
                Log.d(TAG, "processIntent EXTRA_RESET_PERMISSIONS: " + !dangerousPermissionsSet);
            }
            if (i.hasExtra(EXTRA_UPGRADE)) {
                i.removeExtra(EXTRA_UPGRADE);
                final String version = res.getString(R.string.app_version);
                startService(DataCacheDaoService.getParameterIntent(this,
                        new Timestamp(System.currentTimeMillis()), DateTime.getTimeZoneISO8601(),
                        "app_version", version, false));
                String s = res.getString(R.string.app_version_upgraded, res.getString(R.string.app_name), version);
                Log.i(TAG, "processIntent " + s);
                showMessage(s);
                // CLEAR account ENTITY
                sharedPreferences.edit().remove(res.getString(R.string.human_entity_pref_key)).apply();
                entitySet = false;
                Log.i(TAG, "processIntent EXTRA_UPGRADE entity CLEARED!");
                // ACKNOWLEDGEMENT
                AlertDialogFragment.newInstance(
                        R.drawable.ic_face_positive,
                        R.string.acknowledgement_title,
                        Html.fromHtml(res.getString(R.string.acknowledgement_message)),
                        R.string.dialog_ok,
                        (dialog, whichButton) -> dialog.dismiss()
                ).show(this.getSupportFragmentManager(), TAG);
            }
        }
    }

    private void requireLocationSettings() {
        Geopositioning.getInstance(this).checkSettings()
                .addOnSuccessListener(this, lsr -> Log.i(TAG, "LOCATION settings SATISFIED."))
                .addOnFailureListener(this, e -> {
                    int statusCode = ((ApiException)e).getStatusCode();
                    switch (statusCode) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            Log.i(TAG, "LOCATION settings NOT SATISFIED! Attempting...");
                            try {
                                ResolvableApiException rae = (ResolvableApiException)e;
                                rae.startResolutionForResult(SenseiActivity.this,
                                        GEOPOSITIONING_CHECK_SETTINGS_RC);
                            } catch (IntentSender.SendIntentException sie) {
                                Log.e(TAG, "LOCATION UNABLE to execute REQUEST!", sie);
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            Log.e(TAG, "LOCATION settings INADEQUATE! Hint: FIX in your device Settings.");
                            exitWithMessage(res.getString(R.string.exit_location_required));
                    }
                });
    }

    /*
     *                       P E R M I S S I O N S
     */

    private boolean checkPermissionActivityRecognition() {
        if (android.os.Build.VERSION.SDK_INT >= 29) { // CHECK for Android 10+ API 29+
            return PackageManager.PERMISSION_GRANTED ==
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACTIVITY_RECOGNITION);
        } else { // AUTO-GRANTED for Android 9- API 28-
            return true;
        }
    }

    private boolean checkPermissionList(@SuppressWarnings("SameParameterValue") String[] pl) {
        boolean result = true;
        for (String p : pl) {
            if (checkSelfPermission(p) != PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "Permission [" + p + "] NOT GRANTED!");
                result = false;
            }
        }
        return result;
    }

    @Override
    public void onRequestPermissionsResult(int rc, @NonNull String[] pl, @NonNull int[] grantResults) {
        if (isGrantResultsOk(pl, grantResults)) {
            switch (rc) {
                case ACTIVITY_RECOGNITION_RC:
                    activityRecognitionPermissionSet = true;
                    break;
                case DANGEROUS_PERMISSIONS_RC:
                    dangerousPermissionsSet = true;
                    break;
                default:
                    Log.e(TAG, "onRequestPermissionsResult FAILED! Hint: wrong request code? " + rc);
            }
        } else {
            exit();
        }
    }

    private boolean isGrantResultsOk(@NonNull String[] pl, @NonNull int[] grantResults) {
        if (grantResults.length == 0) {
            Log.w(TAG, "Permissions [" + Arrays.toString(pl) + "] CANCELLED by user!");
            return false;
        }
        final int resultsLen = grantResults.length;
        for (int i = 0; i < resultsLen; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                Log.w(TAG, "Permission [" + pl[i] + "] DENIED by user!");
                return false;
            }
        }
        Log.i(TAG, "Permissions [" + Arrays.toString(pl) + "] GRANTED!");
        return true;
    }

    /*
     *                           S I G N   I N
     */

    /*
     * 1. Update if already sign in, refresh token if expired.
     * 2. Ask for sign in if: first time, signed out or revoked.
     *
     * https://developers.google.com/android/reference/com/google/android/gms/auth/api/signin/GoogleSignInClient#silentSignIn()
     */
    private void signInAutoRefresh() {
        Task<GoogleSignInAccount> signIn = googleSignInClient.silentSignIn();
        if (signIn.isSuccessful()) {
            Log.d(TAG, "signInAutoRefresh SILENT SIGN IN OK!");
            signInPostProcess(signIn.getResult());
        } else {
            signIn.addOnCompleteListener(task -> {
                try {
                    signInPostProcess(task.getResult(ApiException.class));
                } catch (ApiException e) {
                    final int status = e.getStatusCode();
                    Log.w(TAG, "signInAutoRefresh FAILED: " +
                            "[" + status + "] " +
                            GoogleSignInStatusCodes.getStatusCodeString(status));
                    if (status == GoogleSignInStatusCodes.SIGN_IN_REQUIRED) {
                        signIn();
                    } else {
                        if (status != GoogleSignInStatusCodes.SIGN_IN_CURRENTLY_IN_PROGRESS) {
                            exitWithMessage(res.getString(R.string.exit_google_sign_in_fatal));
                        }
                    }
                }
            });
        }
    }

    private void signInPostProcess(GoogleSignInAccount account) {
        // Cancel Homeostasis sign in notification
        final NotificationManager nm = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        if (nm != null) {
            nm.cancel(HomeostasisService.SIGN_IN_NOTIFICATION_ID);
        }
        if (!entitySet) {  // FIRST time
            String name = account.getGivenName();
            String email = account.getEmail();
            String emailHash;
            try {
                emailHash = Crypto.digestToHexString(
                        HUMAN_ENTITY_DIGEST_ALGORITHM, Objects.requireNonNull(email));
            } catch (Exception e) {
                throw new RuntimeException("Crypto.digestToHexString FAILED: " + e.getMessage());
            }
            sharedPreferences.edit()
                    .putString(res.getString(R.string.human_email_pref_key), email)
                    .putString(res.getString(R.string.human_name_pref_key), name)
                    .putString(res.getString(R.string.human_entity_pref_key), emailHash)
                    .apply();
            // SPECIAL CASE of a) UPGRADE with entity RESET; b) NEW install with entity SET
            final String birthDatePref = sharedPreferences.getString(
                    res.getString(R.string.human_birthday_pref_key), null);
            final Date birthDate =
                    (birthDatePref == null)
                            ? null
                            : Date.valueOf(birthDatePref);
            final String genderPref =
                    (birthDatePref == null)
                            ? null
                            : sharedPreferences.getString(res.getString(R.string.human_gender_pref_key), null);
            final String dbEntityOp =
                    (birthDate == null && genderPref == null)
                            ? DataCacheDaoService.EXTRA_ENTITY_SET  // NEW
                            : DataCacheDaoService.EXTRA_ENTITY_RESET;  // UPGRADE
            startService(new Intent(this, DataCacheDaoService.class)
                    .putExtra(dbEntityOp,
                            new Entity[]{new Entity(emailHash, birthDate, genderPref, false)}));
            Log.d(TAG, "signInPostProcess emailHash: " + emailHash +
                    " birthDate: " + birthDate + " genderPref: " + genderPref);
            // Parameter: INITIAL PREFERENCES setup for sync
            Map<String, ?> spm = sharedPreferences.getAll();
            List<Parameter> lp = new ArrayList<>();
            Timestamp ts = new Timestamp(System.currentTimeMillis());
            String tz = DateTime.getTimeZoneISO8601();
            for (Map.Entry<String, ?> sp : spm.entrySet()) {
                if (!isIdentifiableDataKey(sp.getKey())) {
                    lp.add(new Parameter(ts, tz, sp.getKey(), String.valueOf(sp.getValue()), false));
                }
            }
            startService(new Intent(this, DataCacheDaoService.class)
                    .putExtra(DataCacheDaoService.EXTRA_PARAMETER_SET,
                            lp.toArray(new Parameter[0])));
            Log.i(TAG, "signInPostProcess ACCOUNT OK!");
        } else {
            Log.i(TAG, "signInPostProcess ACCOUNT OK! (not first time)");
        }
    }

    private void signIn() {
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, HUMAN_SIGN_IN_RC);
        Log.d(TAG, "signIn START INTENT for human!");
    }

    private void handleSignInResult(Task<GoogleSignInAccount> doneTask) {
        try {
            GoogleSignInAccount account = doneTask.getResult(ApiException.class);
            if (account == null) {
                throw new NullPointerException("handleSignInResult ACCOUNT is NULL!");
            }
            signInPostProcess(account);
        } catch (ApiException e) {
            final int status = e.getStatusCode();
            Log.w(TAG, "handleSignInResult FAILED: " +
                    "Status [" + status + "] " +
                    GoogleSignInStatusCodes.getStatusCodeString(status));
        } catch (NullPointerException e) {
            Log.e(TAG, "handleSignInResult FAILED: " + e.getMessage());
        }
    }

    /*
     *                    U S E R   I N T E R F A C E
     */

    private void showMessage(final String msg) {
        final Handler h = new Handler();
        h.post(() -> Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show());
    }

    private void showAgreementDialog() {
        AlertDialogFragment adf = AlertDialogFragment.newInstance(
                R.drawable.ic_info,
                R.string.agreement_title,
                Html.fromHtml(res.getString(R.string.agreement_message)),
                R.string.agreement_dialog_agree,
                (dialog, whichButton) -> {
                    Log.i(TAG, "AGREEMENT OK!");
                    sharedPreferences.edit()
                            .putBoolean(res.getString(R.string.app_agreement_pref_key), true)
                            .putString(res.getString(R.string.app_agreement_date_pref_key),
                                    DateTime.getTimestampISO8601())
                            .apply();
                    dialog.dismiss();
                    SenseiActivity.this.onResume();
                },
                R.string.agreement_dialog_disagree,
                (dialog, whichButton) -> {
                    Log.i(TAG, "EXITING... Agreement CANCELLED!");
                    sharedPreferences.edit()
                            .putBoolean(res.getString(R.string.app_agreement_pref_key), false)
                            .putString(res.getString(R.string.app_agreement_date_pref_key),
                                    DateTime.getTimestampISO8601())
                            .apply();
                    dialog.dismiss();
                    exit();
                }
        );
        adf.setCancelable(false); // Deactivate device's back button.
        adf.show(this.getSupportFragmentManager(), TAG);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.sensei, m);
        return super.onCreateOptionsMenu(m);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem mi) {
        switch (mi.getItemId()) {
            case R.id.menu_map:
                Location location = Geopositioning.getInstance(this).getLastStored();
                final Intent intent = new Intent(this, MapActivity.class);
                if (location != null) {
                    intent.putExtra(MapActivity.EXTRA_LOCATION,
                            new double[]{location.getLatitude(), location.getLongitude()});
                }
                startActivity(intent);
                return true;
            case R.id.menu_diary:
                startActivity(new Intent(this, DiaryActivity.class));
                return true;
            case R.id.menu_chart:
                startActivity(new Intent(this, ChartActivity.class));
                return true;
            case R.id.menu_social:
                startActivity(new Intent(this, SocialActivity.class));
                return true;
            case R.id.menu_data_stats:
                startActivity(new Intent(this, DataStatsActivity.class));
                return true;
            case R.id.menu_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.menu_help:
                AlertDialogFragment.newInstance(
                        R.drawable.ic_question_mark,
                        R.string.help_title,
                        Html.fromHtml(res.getString(R.string.help_message)),
                        R.string.dialog_ok,
                        (dialog, whichButton) -> dialog.dismiss()
                ).show(this.getSupportFragmentManager(), TAG);
                return true;
            case R.id.menu_about:
                AlertDialogFragment.newInstance(
                        R.drawable.ic_school,
                        R.string.about_title,
                        Html.fromHtml(res.getString(R.string.about_message, res.getString(R.string.app_name), res.getString(R.string.app_version))),
                        R.string.dialog_ok,
                        (dialog, whichButton) -> dialog.dismiss()
                ).show(this.getSupportFragmentManager(), TAG);
                return true;
            case R.id.menu_acknowledgement:
                AlertDialogFragment.newInstance(
                        R.drawable.ic_face_positive,
                        R.string.acknowledgement_title,
                        Html.fromHtml(res.getString(R.string.acknowledgement_message)),
                        R.string.dialog_ok,
                        (dialog, whichButton) -> dialog.dismiss()
                ).show(this.getSupportFragmentManager(), TAG);
                return true;
            default:
                return super.onOptionsItemSelected(mi);
        }
    }

    private void setupMainView() {
        setContentView(R.layout.sensei);
        setTitle(R.string.app_name);
        setupSmiles();
        setupDiaryInput();
        setupDashboardView();
        hideKeyboard();
    }

    private void setupSmiles() {
        final CardView cvNegative = findViewById(R.id.smiles_feel_negative);
        cvNegative.setOnClickListener(v -> {
            startService(SmileReportService.getIntent(v.getContext(), SmileReportService.NEGATIVE));
            showSmilesSFX(v);
        });
        final CardView cvNeutral = findViewById(R.id.smiles_feel_neutral);
        cvNeutral.setOnClickListener(v -> {
            startService(SmileReportService.getIntent(v.getContext(), SmileReportService.NEUTRAL));
            showSmilesSFX(v);
        });
        final CardView cvPositive = findViewById(R.id.smiles_feel_positive);
        cvPositive.setOnClickListener(v -> {
            startService(SmileReportService.getIntent(v.getContext(), SmileReportService.POSITIVE));
            showSmilesSFX(v);
        });
    }

    private void showSmilesSFX(final View v) {
        final float e = res.getDimensionPixelSize(R.dimen.smiles_unit_elevation);
        final float t = e / 4;
        v.setElevation(0);
        v.setTranslationX(t);
        v.setTranslationY(t);
        v.setEnabled(false);
        v.invalidate();
        Handler h = new Handler();
        h.postDelayed(() -> {
            v.setElevation(e);
            v.setTranslationX(0);
            v.setTranslationY(0);
            v.setEnabled(true);
            v.invalidate();
        }, 500);
    }

    @SuppressWarnings("SameReturnValue")
    private void setupDiaryInput() {
        diaryInputView = findViewById(R.id.diary_input);
        final EditText diaryInputText = findViewById(R.id.diary_input_text);
        diaryInputText.setRawInputType(InputType.TYPE_CLASS_TEXT);
        diaryInputText.setImeOptions(EditorInfo.IME_ACTION_SEND);
        diaryInputText.setOnClickListener(v -> startActivity(new Intent(SenseiActivity.this, DiaryActivity.class)));
        diaryInputText.setOnKeyListener((v, keyCode, keyEvent) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                onBackPressed();
            } else {
                Intent intent = new Intent(SenseiActivity.this, DiaryActivity.class);
                startActivity(intent.putExtra(DiaryActivity.EXTRA_TEXT, diaryInputText.getText().toString().trim()));
            }
            return true;
        });
    }

    private void setupDiaryInputSpacing() {
        final int elevation = (int)res.getDimension(R.dimen.diary_input_elevation);
        ((ViewGroup.MarginLayoutParams)diaryInputView.getLayoutParams())
                .setMargins(paddingSpace + elevation, 0, paddingSpace + elevation, elevation);
    }

    private void setupDashboardView() {
        final GridView dashboardView = findViewById(R.id.dashboard);
        dashboardView.setAdapter(new DashboardAdapter(this, this,
                Dashboard.getInstance().getUnits(), new ViewModelProvider(this).get(DashboardViewModel.class)));
        // Specific stretch for GridView which has android:numColumns="auto_fit" and android:stretchMode="none"
        dashboardView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            // Equal spacing for all (including the border with parent)
            int numColumns = dashboardView.getNumColumns();  // May return -1 = AUTO_FIT (Android 7.1)
            if (numColumns == GridView.AUTO_FIT) {
                numColumns = 3;  // Fail safe value
            }
            int spare = dashboardView.getWidth() - numColumns * dashboardView.getColumnWidth();
            paddingSpace = spare / (numColumns + 1);
            dashboardView.setPadding(paddingSpace, paddingSpace, paddingSpace, paddingSpace);
            dashboardView.setHorizontalSpacing(paddingSpace);
            dashboardView.setVerticalSpacing(paddingSpace);
            // Adapting spacing of other views in the same layout...
            setupDiaryInputSpacing();
        });
    }

    private void hideKeyboard() {
        View v = this.getCurrentFocus();
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if (v != null && imm != null) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    /*
     *                             E X I T
     *
     * To use only on initial requirements (e.g., agreement not accepted).
     */

    private void exitWithMessage(final String msg) {
        showMessage(msg);
        exit();
    }

    private void exit() {
        Log.i(TAG, "exit() STOPPING ALL SERVICES...");
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(Sensei.ACTION_STOP));
        Log.i(TAG, "exit()  ...DONE STOPPING ALL SERVICES!");
        finishAndRemoveTask();
    }
}
