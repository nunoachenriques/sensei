/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.emotion;

import android.content.Context;
import android.util.Log;
import android.util.Pair;

import net.nunoachenriques.empa.affective.text.HumanSentimentIdentifierService;
import net.nunoachenriques.empa.language.HumanLanguageIdentifierService;
import net.nunoachenriques.empa.language.HumanLanguageTranslateService;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The sentiment analysis utilities. Includes integration with language
 * detection, translation and more.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class Sentiment {

    /** Value to denote absence of sentiment valid value. */
    public static final Float NO_SENTIMENT = Float.MAX_VALUE;

    private static final String TAG = "Sentiment";

    private static volatile Sentiment INSTANCE;

    private final HumanLanguageIdentifierService languageService;
    private final HumanLanguageTranslateService translateService;
    private HumanSentimentIdentifierService sentimentService;
    private final Pattern emoticonOnlyPattern;

    private Sentiment(Context c)
            throws Exception {
        languageService = HumanLanguageIdentifierService.getInstance();
        translateService = HumanLanguageTranslateService.getInstance();
        translateService.setup(c.getFilesDir());
        sentimentService = HumanSentimentIdentifierService.getInstance();
        // https://en.wikipedia.org/wiki/List_of_emoticons
        // TODO include Unicode emoticons
        emoticonOnlyPattern = Pattern.compile("([>}])?([:;=x8#%])(')?([-oc^])?([)(|/\\\\*&#$bDPpSsxX])");
    }

    /**
     * Gets an instance of this class. If no single instance is available, i.e.,
     * {@code null}, then creates and returns a single new one
     * (Singleton pattern).
     *
     * @param c Android application {@link Context}.
     * @return The single instance of this class.
     * @throws Exception on failure.
     */
    public static Sentiment getInstance(Context c)
            throws Exception {
        if (INSTANCE == null) {
            synchronized (Sentiment.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Sentiment(c);
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Gets the polarity of the text sample sentiment analysis. Moreover, it
     * translates from the detected language to the one accepted by the
     * sentiment analyzer. Additionally, deals with only ASCII emoticons text.
     * All in a best effort to get the sentiment.
     *
     * @param text The sample to get the sentiment from.
     * @return A pair of (polarity [-1, 1], identified language). {@code null} on failure.
     */
    public Pair<Float, String> getPolarityLanguage(String text) {
        Float polarity = null;
        String identifiedLanguage = null;
        try {
            List<String> ssal = sentimentService.getAvailableLanguages();
            String ssl = ssal.get(0); // The first is the canonical one, i.e., "en"
            Matcher emoticonOnlyMatcher = emoticonOnlyPattern.matcher(text);
            boolean emoticonOnly = emoticonOnlyMatcher.matches();
            identifiedLanguage = emoticonOnly ? ssl : languageService.getLanguage(text); // 2-char ISO 639-1 or 639-3
            if (identifiedLanguage != null) { // Go for sentiment analysis
                if (!ssal.contains(identifiedLanguage)) { // Try to translate to a known language for the sentiment analysis
                    try {
                        String t;
                        if (identifiedLanguage.equals("pt")) { // TODO Fix this special hardcoded case in a general one
                            /*
                             * A pivot language pt-gl-en is needed because there's no pt-en in Apertium (empa-plugin-apertiumlanguagetranslator)
                             */
                            t = translateService.getTranslation(translateService.getTranslation(text, identifiedLanguage, "gl"), "gl", ssl);
                        } else { // Try to translate in general
                            t = translateService.getTranslation(text, identifiedLanguage, ssl);
                        }
                        polarity = sentimentService.getSentiment(t, ssl);
                    } catch (Exception e) { // Translation failed
                        Log.e(TAG, "Translation from [" + identifiedLanguage + "] to [" + ssl + "] FAILED!", e);
                    }
                } else {
                    polarity = sentimentService.getSentiment(text, identifiedLanguage);
                }
            } else if (emoticonOnlyMatcher.find()) {
                /*
                 * If neither emoticon only nor language identified and
                 * text has mixed emoticons then get sentiment only on
                  * extracted emoticons.
                 */
                polarity = sentimentService.getSentiment(emoticonOnlyMatcher.group(), ssl);
            } else {
                Log.d(TAG, "Language NOT identified for [" + text + "] | Languages: " + languageService.getInUseLanguages().toString());
            }
        } catch (Exception e) {
            Log.e(TAG, "Unknown FAILURE!", e);
        }
        if (polarity == null) {
            Log.d(TAG, "Sentiment FAILED to analyze for [" + text + "]");
            return null;
        } else {
            Log.d(TAG, "Polarity: [" + text + "]");
            return new Pair<>(polarity, identifiedLanguage);
        }
    }
}
