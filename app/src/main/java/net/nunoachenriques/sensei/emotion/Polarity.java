/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.emotion;

import net.nunoachenriques.sensei.R;

/**
 * The application's polarity common and required definitions.
 * Emotional valence:
 * <pre>
 * negative [-1.0, -0.5]
 * neutral  (-0.5, 0.5)
 * positive [0.5, 1.0]
 * </pre>
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class Polarity {

    /** Common name for all sensors' polarity attribute. */
    public static final String SENSOR_ATTR_POLARITY_COMMON = "polarity";

    /** Value to denote absence of polarity valid value. */
    public static final double NO_POLARITY = Double.MAX_VALUE;
    /** Negative emotional valence [-1.0, -0.5] */
    public static final double NEGATIVE_MIN = -0.5;
    /** Negative emotional valence [-1.0, -0.5] */
    public static final double NEGATIVE_MAX = -1.0;
    /** Positive emotional valence [0.5, 1.0] */
    public static final double POSITIVE_MIN = 0.5;
    /** Positive emotional valence [0.5, 1.0] */
    public static final double POSITIVE_MAX = 1.0;

    /** Value to denote absence of prediction class valid value. */
    @SuppressWarnings("WeakerAccess")
    public static final int NO_CLASS = Integer.MAX_VALUE;
    /** Negative emotional valence as a prediction class */
    public static final int NEGATIVE_CLASS = -1;
    /** Neutral emotional valence as a prediction class */
    public static final int NEUTRAL_CLASS = 0;
    /** Positive emotional valence as a prediction class */
    public static final int POSITIVE_CLASS = 1;

    private Polarity() {}

    /**
     * Checks if polarity value is out of the accepted range:
     * [{@link #NEGATIVE_MAX}, {@link #POSITIVE_MAX}].
     *
     * @param p Polarity value.
     * @return {@code true} if value not in [{@link #NEGATIVE_MAX}, {@link #POSITIVE_MAX}], {@code false} if valid.
     */
    public static boolean isInvalid(double p) {
        return (p < NEGATIVE_MAX || p > POSITIVE_MAX);
    }

    /**
     * Checks if polarity value is negative.
     *
     * @param p Polarity value in [{@link #NEGATIVE_MAX}, {@link #POSITIVE_MAX}].
     * @return {@code true} if value in [{@link #NEGATIVE_MAX}, {@link #NEGATIVE_MIN}], {@code false} otherwise.
     */
    public static boolean isNegative(double p) {
        return (p >= NEGATIVE_MAX && p <= NEGATIVE_MIN);
    }

    /**
     * Checks if polarity value is neutral.
     *
     * @param p Polarity value in [{@link #NEGATIVE_MAX}, {@link #POSITIVE_MAX}].
     * @return {@code true} if value in ({@link #NEGATIVE_MIN}, {@link #POSITIVE_MIN}), {@code false} otherwise.
     */
    public static boolean isNeutral(double p) {
        return (p > NEGATIVE_MIN && p < POSITIVE_MIN);
    }

    /**
     * Checks if polarity value is positive.
     *
     * @param p Polarity value in [{@link #NEGATIVE_MAX}, {@link #POSITIVE_MAX}].
     * @return {@code true} if value in [{@link #POSITIVE_MIN}, {@link #POSITIVE_MAX}], {@code false} otherwise.
     */
    public static boolean isPositive(double p) {
        return (p >= POSITIVE_MIN && p <= POSITIVE_MAX);
    }

    /**
     * Converts the polarity given value to the following resources identifier:
     * color, string (name), drawable (face). Example:
     * <pre>{@code
     * ...
     * Resources res = getResources();
     * Resources.Theme theme = getTheme();
     * int[] resource = Polarity.polarityToResources(polarity);
     * int color = res.getColor(resource[0], theme);
     * String string = res.getString(resource[1]);
     * Drawable icon = res.getDrawable(resource[2], theme);
     * ...}</pre>
     *
     * @param p Polarity value in [{@link #NEGATIVE_MAX}, {@link #POSITIVE_MAX}].
     * @return
     * On success (polarity-based resources):
     * <pre>
     * {{@link androidx.annotation.ColorRes}, {@link androidx.annotation.StringRes}, {@link androidx.annotation.DrawableRes}}.
     * </pre>
     * On failure:
     * <pre>
     * {android.R.color.transparent, R.string.empty, R.drawable.ic_yin_yang}.
     * </pre>
     */
    public static int[] polarityToResources(double p) {
        if (isNegative(p)) {
            return new int[]{R.color.polarity_negative, R.string.negative, R.drawable.ic_face_negative};
        } else if (isNeutral(p)) {
            return new int[]{R.color.polarity_neutral, R.string.neutral, R.drawable.ic_face_neutral};
        } else if (isPositive(p)) {
            return new int[]{R.color.polarity_positive, R.string.positive, R.drawable.ic_face_positive};
        } else {
            return new int[]{android.R.color.transparent, R.string.empty, R.drawable.ic_yin_yang};
        }
    }

    /**
     * Converts the polarity given to a prediction class.
     *
     * @param p Polarity value in [{@link #NEGATIVE_MAX}, {@link #POSITIVE_MAX}].
     * @return A prediction class, i.e., {@link #NEGATIVE_CLASS}, {@link #NEUTRAL_CLASS}, or {@link #POSITIVE_CLASS}.
     *     Also, if polarity is an invalid value then it will return {@link #NO_CLASS}.
     */
    @SuppressWarnings("unused")
    public static int polarityToClass(double p) {
        if (isNegative(p)) {
            return NEGATIVE_CLASS;
        } else if (isNeutral(p)) {
            return NEUTRAL_CLASS;
        } else if (isPositive(p)) {
            return POSITIVE_CLASS;
        } else {
            return NO_CLASS;
        }
    }
}
