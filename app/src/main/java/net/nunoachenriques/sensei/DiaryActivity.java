/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * The diary messaging activity with search functionality.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see DiaryAdapter
 * @see <a href="https://developer.android.com/training/search/setup">Android Developer Search Setup</a>
 */
public final class DiaryActivity
        extends SenseiAppCompatActivity {

    private static final String TAG = "DiaryActivity";

    public static final String EXTRA_TEXT = TAG + "extra_text";

    private RecyclerView diaryView;
    private DiaryAdapter diaryAdapter;
    private Transition transition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.diary);
        setTitle(R.string.diary_title);
        setSupportActionBar();
        setupDiaryMessages();
        setupDiaryInput();
        transition = new Transition(this, DiaryReportService.SENSOR_ID);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        getMenuInflater().inflate(R.menu.diary, m);
        SearchManager sm = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
        if (sm != null) {
            SearchView sv = (SearchView)m.findItem(R.id.menu_search).getActionView();
            sv.setSearchableInfo(sm.getSearchableInfo(getComponentName())); // res/xml/diary_searchable.xml
            sv.setIconifiedByDefault(true);
            String actionSearchQuery = getActionSearchQuery(getIntent());
            if (actionSearchQuery != null && !actionSearchQuery.isEmpty()) {
                sv.setIconified(false);
            }
            sv.setQuery(actionSearchQuery, false);
            sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return true; // Avoid Intent double action, query is up-to-date
                }
                @Override
                public boolean onQueryTextChange(String newText) {
                    if (newText.isEmpty()) {
                        resetDiaryMessages(null);
                        Log.d(TAG, "resetDiaryMessages(null)");
                        setIntent(null);
                    } else {
                        resetDiaryMessages(newText);
                        Log.d(TAG, "resetDiaryMessages(newText): " + newText);
                    }
                    return false;
                }
            });
        }
        return super.onCreateOptionsMenu(m);
    }

    @Override
    protected void onNewIntent(Intent i) {
        super.onNewIntent(i);
        setIntent(i);
    }

    @Override
    protected void onResume() {
        super.onResume();
        transition.setEnter();
        String searchToken = getActionSearchQuery(getIntent());
        resetDiaryMessages(searchToken);
        Log.d(TAG, "resetDiaryMessages(searchToken): " + searchToken);
    }

    @Override
    protected void onPause() {
        super.onPause();
        transition.setExit();
    }

    @Nullable
    private String getActionSearchQuery(Intent i) {
        if (i != null && Intent.ACTION_SEARCH.equals(i.getAction())) {
            return i.getStringExtra(SearchManager.QUERY);
        }
        return null;
    }

    /*
     *                             D I A R Y
     */

    private void setupDiaryMessages() {
        diaryView = findViewById(R.id.diary_messages);
        diaryView.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(false);
        layoutManager.setStackFromEnd(true);
        diaryView.setLayoutManager(layoutManager);
        diaryAdapter = new DiaryAdapter(this, this,
                diaryView, new ViewModelProvider(this).get(DiaryViewModel.class));
        diaryView.setAdapter(diaryAdapter);
    }

    private void resetDiaryMessages(String searchToken) {
        diaryView.setAdapter(diaryAdapter.resetDataObserver(searchToken));
    }

    private void setupDiaryInput() {
        final EditText diaryInputText = findViewById(R.id.diary_input_text);
        Intent intent = getIntent();
        if (intent.hasExtra(EXTRA_TEXT)) {
            diaryInputText.setText(intent.getStringExtra(EXTRA_TEXT));
        }
        diaryInputText.requestFocus();
        diaryInputText.setRawInputType(InputType.TYPE_CLASS_TEXT);
        diaryInputText.setImeOptions(EditorInfo.IME_ACTION_SEND);
        diaryInputText.setOnKeyListener((v, keyCode, keyEvent) -> {
            if (keyEvent.getAction() == KeyEvent.ACTION_DOWN
                    && keyCode == KeyEvent.KEYCODE_ENTER) {
                String s = diaryInputText.getText().toString().trim();
                Log.d(TAG, "Diary (enter): [" + s + "]");
                if (!s.isEmpty()) {
                    startService(new Intent(getApplicationContext(), DiaryReportService.class)
                            .putExtra(DiaryReportService.EXTRA_TEXT, s));
                }
                diaryInputText.setText(null);
                hideKeyboard(DiaryActivity.this);
                return true;
            }
            return false;
        });
    }

    private void hideKeyboard(Activity a) {
        View v = a.getCurrentFocus();
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if (v != null && imm != null) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }
}
