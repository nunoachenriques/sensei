/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.Nullable;

import net.nunoachenriques.sensei.persistence.Collected;
import net.nunoachenriques.sensei.persistence.DataCacheDaoService;
import net.nunoachenriques.sensei.settings.Settings;
import net.nunoachenriques.sensei.utility.DateTime;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The battery (level and more) service for the feed listener.
 * Gets data from the intent, process and store it.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see BatteryReceiver
 */
public final class BatteryService
        extends IntentService {

    private static final String SENSOR_ID = "app.sensor.battery";

    private static final String TAG = "BatteryService";
    private static final String HEALTH_KEY = "health";
    private static final String LEVEL_DIFF_PREF_KEY = "battery.level.diff_threshold";
    private static final String LEVEL_KEY = "level";
    private static final String PLUGGED_KEY = "plugged";
    private static final String STATUS_KEY = "status";
    private static final int NO_VALUE = -1;

    /**
     * Memory for the previous stored value of interest for some attribute in
     * order to apply a filter. Is used to select only the values of interest.
     */
    private static final Map<String, Integer> lastValue =
            new HashMap<String, Integer>(4) {{
                put(HEALTH_KEY, null);
                put(LEVEL_KEY, null);
                put(PLUGGED_KEY, null);
                put(STATUS_KEY, null);
            }};

    public BatteryService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent i) {
        if (i != null) {
            List<Collected> lc = new ArrayList<>();
            Timestamp ts = new Timestamp(System.currentTimeMillis());
            String tz = DateTime.getTimeZoneISO8601();
            Integer health = i.getIntExtra(BatteryManager.EXTRA_HEALTH, NO_VALUE);
            if (health != NO_VALUE) {
                if (!health.equals(lastValue.get(HEALTH_KEY))) {
                    lc.add(new Collected(ts, tz, SENSOR_ID, HEALTH_KEY, (double)health, null, false));
                    lastValue.put(HEALTH_KEY, health);
                }
            }
            Integer level = i.getIntExtra(BatteryManager.EXTRA_LEVEL, NO_VALUE);
            int scale = i.getIntExtra(BatteryManager.EXTRA_SCALE, NO_VALUE);
            if (level != NO_VALUE && scale != NO_VALUE) {
                final Integer lastValueLevel = lastValue.get(LEVEL_KEY);
                final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                if (lastValueLevel == null
                        || Math.abs(level - lastValueLevel) > Settings.getDouble(sp, LEVEL_DIFF_PREF_KEY)) {
                    lc.add(new Collected(ts, tz, SENSOR_ID, LEVEL_KEY, (level / (double)scale), null, false));
                    lastValue.put(LEVEL_KEY, level);
                }
            }
            Integer plugged = i.getIntExtra(BatteryManager.EXTRA_PLUGGED, NO_VALUE);
            if (plugged != NO_VALUE) {
                if (!plugged.equals(lastValue.get(PLUGGED_KEY))) {
                    lc.add(new Collected(ts, tz, SENSOR_ID, PLUGGED_KEY, (double)plugged, null, false));
                    lastValue.put(PLUGGED_KEY, plugged);
                }
            }
            Integer status = i.getIntExtra(BatteryManager.EXTRA_STATUS, NO_VALUE);
            if (status != NO_VALUE) {
                if (!status.equals(lastValue.get(STATUS_KEY))) {
                    lc.add(new Collected(ts, tz, SENSOR_ID, STATUS_KEY, (double)status, null, false));
                    lastValue.put(STATUS_KEY, status);
                }
            }
            if (lc.size() > 0) {
                startService(
                        new Intent(this, DataCacheDaoService.class)
                                .putExtra(DataCacheDaoService.EXTRA_COLLECTED_SET, lc.toArray(new Collected[0]))
                );
            } else {
                Log.d(TAG, "BATTERY DATA DISCARDED!");
            }
        }
    }
}
