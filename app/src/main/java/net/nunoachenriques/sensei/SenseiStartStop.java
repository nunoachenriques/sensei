/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.util.Log;

import net.nunoachenriques.sensei.utility.Services;

/**
 * Starts and stops application's main (critical) services using the
 * {@link HomeostasisService}.
 *
 * Includes broadcast receiving of {@link Sensei#ACTION_START} when the services
 * must be started (after being stopped) and {@link Sensei#ACTION_STOP} for
 * forcing services to stop.
 *
 * Additionally, receives system {@link Intent#ACTION_BOOT_COMPLETED} and
 * {@link Intent#ACTION_MY_PACKAGE_REPLACED} and wakes up, i.e., schedule
 * {@link HomeostasisService} to run periodically and also immediately.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class SenseiStartStop
        extends BroadcastReceiver {

    private static final String TAG = "SenseiStartStop";

    private static volatile JobScheduler jobScheduler;

    public SenseiStartStop() {
        super();
    }

    @Override
    public void onReceive(Context c, Intent i) {
        if (i != null) {
            if (jobScheduler == null) {
                jobScheduler = (JobScheduler)c.getSystemService(Context.JOB_SCHEDULER_SERVICE);
            }
            if (Intent.ACTION_MY_PACKAGE_REPLACED.equals(i.getAction())) {  //   U P G R A D E
                Log.i(TAG, "UPGRADE");
                jobScheduler.cancelAll();  // Clean up all job scheduled services
                c.startActivity(
                        new Intent(c, SenseiActivity.class)
                                .setFlags(
                                        Intent.FLAG_ACTIVITY_SINGLE_TOP
                                                | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                                | Intent.FLAG_ACTIVITY_NEW_TASK
                                ) // https://stackoverflow.com/a/11563909/8418165
                                .putExtra(SenseiActivity.EXTRA_UPGRADE, true));
            } else if (Sensei.ACTION_START.equals(i.getAction())
                    || Intent.ACTION_BOOT_COMPLETED.equals(i.getAction())) {  //   S T A R T   ||   B O O T
                Log.i(TAG, "START || BOOT");
                if (!HomeostasisService.isRunning
                        && android.os.Build.VERSION.SDK_INT < 26) {  // Force when Android < 8.0 API < 26
                    Services.scheduleOneTimeJobService(
                            c, jobScheduler, HomeostasisService.class,
                            Sensei.HOMEOSTASIS_SERVICE_NOW_JOB_ID, 100, 1000, false
                    );
                }
                if (!Services.isJobScheduled(jobScheduler, Sensei.HOMEOSTASIS_SERVICE_JOB_ID)) {
                    String periodSp = PreferenceManager.getDefaultSharedPreferences(c)
                            .getString("homeostasis.period", null);
                    long period = (periodSp != null) ? Long.parseLong(periodSp) : 900000;
                    Services.schedulePeriodicJobService(
                            c, jobScheduler, HomeostasisService.class,
                            Sensei.HOMEOSTASIS_SERVICE_JOB_ID, period, false, false
                    );
                }
            } else if (Sensei.ACTION_STOP.equals(i.getAction())) {  //   S T O P
                Log.i(TAG, "STOP");
                jobScheduler.cancelAll();  // Clean up all job scheduled services
            }
        }
    }

    /**
     * Checks if {@link HomeostasisService} is already scheduled to run.
     *
     * @return True if scheduled, false otherwise.
     */
    public static boolean isHomeostasisJobScheduled() {
        return jobScheduler != null
                && Services.isJobScheduled(jobScheduler, Sensei.HOMEOSTASIS_SERVICE_JOB_ID);
    }
}
