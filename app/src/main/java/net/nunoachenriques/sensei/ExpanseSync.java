/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.RuntimeExecutionException;
import com.google.android.gms.tasks.Task;

import net.nunoachenriques.sensei.persistence.Collected;
import net.nunoachenriques.sensei.persistence.DataCacheDao;
import net.nunoachenriques.sensei.persistence.Entity;
import net.nunoachenriques.sensei.persistence.Parameter;
import net.nunoachenriques.sensei.utility.Assets;
import net.nunoachenriques.sensei.utility.Connectivity;
import net.nunoachenriques.sensei.utility.DateTime;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * The Expanse data sync of local persistence and cloud storage.
 * Updates the Expanse storage in the cloud with unsynced collected data.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class ExpanseSync {

    static final long NO_SYNC = 0;
    static final Timestamp collectedLastSync = new Timestamp(NO_SYNC);

    private static final String TAG = "ExpanseSync";
    private static final String ID_TOKEN_NAME = "oauth_id_token";
    private static final String REQUEST_NAME = "expanse_request";
    private static final String SET_ENTITY_REQ = "set_entity";
    private static final String SET_PARAMETER_REQ = "set_parameter";
    private static final String SET_COLLECTED_REQ = "set_collected";
    private static final String GET_PREDICTION_REQ = "get_prediction";
    private static final String REQ_RESPONSE_OK = "ok";
    private static final String REQ_RESPONSE_ERROR = "error";
    private static final int NETWORK_TIMEOUT = 10;  // seconds

    private static volatile ExpanseSync INSTANCE;
    private static volatile int BATCH_SIZE;
    private static volatile DataCacheDao dataCacheDao;
    private static volatile String syncURL = null;

    private CountDownLatch googleSignInReady;
    private GoogleSignInAccount googleSignInAccount;

    private ExpanseSync(final DataCacheDao dao) {
        dataCacheDao = dao;
    }

    /**
     * Gets an instance of this class. If no single instance is available, i.e.,
     * {@code null}, then creates and returns a single new one
     * (Singleton pattern).
     *
     * @return The single instance of this class.
     */
    public static ExpanseSync getInstance(final DataCacheDao dao) {
        if (INSTANCE == null) {
            synchronized (ExpanseSync.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ExpanseSync(dao);
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Performs the data sync between local cache and cloud expanse.
     * Checks for requirements (e.g., WiFi) and does the sync if all met.
     * Sync procedure:
     * <pre>
     * 0. Requirements (connectivity) check? If false then return.
     * 1. Prepare network queue for requests.
     *
     * For each data entity (Entity, Collected, Parameter) just sync:
     * 2. Read all (not synced) records from local cache.
     * 3. Iterate over collected local cache:
     *  3.1. Collect each one to a request object (JSON) and to a delete list.
     *  3.2. When collected items reach a batch size threshold:
     *   3.2.1. Call Expanse WS to sync.
     *   3.2.2. (On success) delete from local cache.
     *          (On error) cancel and terminate.
     * 4. Finish sync (Call WS on remaining --- below batch size --- items).
     *
     * Finishing:
     * 5. On error, stop sync and log it.
     * </pre>
     *
     * @param c Android application {@link Context}.
     * @param wifiRequired True if requires WiFi, false otherwise.
     * @param batchSize The maximum number of items for each network request.
     */
    void sync(final Context c, boolean wifiRequired, int batchSize) {
        if (Connectivity.isConnectionOk(c, wifiRequired)) {
            RequestQueue requestQueue = null;
            BATCH_SIZE = batchSize;
            try {
                if (syncURL == null) {
                    syncURL = getSyncURL(c);
                }
                googleSignInAccount = getGoogleSignInAccount(c);
                if (googleSignInAccount != null) {
                    requestQueue = Volley.newRequestQueue(c);
                    // Required sequence: Entity > Parameter > Collected
                    syncEntity(requestQueue);
                    syncParameter(requestQueue);
                    syncCollected(requestQueue);
                }
            } catch (Exception e) {
                Log.e(TAG, "SYNC FAILED!", e);
            } finally {
                if (requestQueue != null) {
                    requestQueue.stop();
                }
            }
        } else {
            Log.w(TAG, "SYNC FAILED! Connectivity UNAVAILABLE."
                    + " Hint: WiFi required and available?");
        }
    }

    private String getSyncURL(Context c)
            throws IOException, JSONException {
        final JSONObject ws = new JSONObject(
                Assets.readJSONDataFromAsset(c, "sensei_expanse_ws.json"));
        return ws.getString(BuildConfig.DEBUG ? "url_debug" : "url");
    }

    private GoogleSignInAccount getGoogleSignInAccount(final Context c)
            throws IllegalStateException, RuntimeExecutionException, InterruptedException {
        GoogleSignInClient googleSignInClient = GoogleIdentity.getSignInClient(c);
        if (googleSignInClient == null) {
            throw new IllegalStateException("googleSignInClient is NULL! Hint: should be temporary.");
        }
        Task<GoogleSignInAccount> signIn = googleSignInClient.silentSignIn();
        if (signIn.isSuccessful()) {
            Log.d(TAG, "getGoogleSignInAccount SILENT SIGN IN OK!");
            return signIn.getResult();
        } else {
            googleSignInReady = new CountDownLatch(1);
            signIn.addOnCompleteListener(task -> {
                try {
                    googleSignInAccount = task.getResult(ApiException.class);
                    Log.d(TAG, "getGoogleSignInAccount SILENT SIGN IN OK! (onComplete)");
                } catch (ApiException e) {
                    googleSignInAccount = null;
                    final int status = e.getStatusCode();
                    Log.w(TAG, "getGoogleSignInAccount SILENT SIGN IN FAILED:"
                            + " [" + status + "] "
                            + GoogleSignInStatusCodes.getStatusCodeString(status));
                } finally {
                    googleSignInReady.countDown();
                }
            });
            googleSignInReady.await(NETWORK_TIMEOUT, TimeUnit.SECONDS);
            return googleSignInAccount;
        }
    }

    /*
     *                             S Y N C S
     */

    // ENTITY

    private void syncEntity(RequestQueue requestQueue)
            throws ExpanseSyncException, JSONException {
        final Entity data = dataCacheDao.getEntity();
        if (data != null) {
            JSONArray dataJSON = new JSONArray();
            dataJSON.put(getJSONFromData(data));
            Log.d(TAG, "SYNC " + data.toString());
            final RequestFuture<JSONObject> requestFuture = RequestFuture.newFuture();
            syncEntityRequest(requestQueue, requestFuture, dataJSON);
            syncEntityResponse(requestQueue, requestFuture, data);  // Blocking on get
            Log.d(TAG, "SYNC " + SET_ENTITY_REQ + " DONE!");
        }
    }

    private JSONObject getJSONFromData(Entity e)
            throws JSONException {
        return new JSONObject()
                .put("birthday", (e.birthday != null)
                        ? DateTime.epochMillisToDateISO8601(e.birthday.getTime())
                        : JSONObject.NULL)
                .put("gender", e.gender);
    }

    private void syncEntityRequest(RequestQueue requestQueue,
                                   RequestFuture<JSONObject> requestFuture,
                                   JSONArray data)
            throws JSONException {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                syncURL,
                getJSONRequestFromData(data, SET_ENTITY_REQ),
                requestFuture,
                requestFuture
        );
        request.setTag(SET_ENTITY_REQ);
        requestQueue.add(request);
    }

    private void syncEntityResponse(RequestQueue requestQueue,
                                    RequestFuture<JSONObject> requestFuture,
                                    Entity data)
            throws ExpanseSyncException {
        try {
            final JSONObject response = requestFuture.get(NETWORK_TIMEOUT, TimeUnit.SECONDS);
            if (response.has(REQ_RESPONSE_OK)) {
                data.synced = true;
                dataCacheDao.reset(data);
            } else {
                throw new ExpanseSyncException("Server NOT OK response: ["
                        + response.getString(REQ_RESPONSE_ERROR) + "]");
            }
        } catch (InterruptedException|ExecutionException|TimeoutException e) {
            requestQueue.cancelAll(SET_ENTITY_REQ);
            throw new ExpanseSyncException("Request GET FAILED!", e);
        } catch (JSONException e) {
            throw new ExpanseSyncException("Response GET ERROR FAILED!", e);
        }
    }

    // PARAMETER

    private void syncParameter(RequestQueue requestQueue)
            throws ExpanseSyncException, JSONException {
        List<Parameter> data = dataCacheDao.getParameterNotSynced();
        JSONArray dataJSON = new JSONArray();
        List<Parameter> dataUpdate = new ArrayList<>(BATCH_SIZE);
        Parameter p;
        int batch = 0;
        int i;
        for (i = 0; i < data.size(); i++) {
            p = data.get(i);
            dataJSON.put(getJSONFromData(p));
            p.synced = true;
            dataUpdate.add(p);
            Log.d(TAG, "SYNC " + p.toString());
            if (((i + 1) % BATCH_SIZE) == 0) {
                batch++;
                syncParameterAction(requestQueue, dataJSON, dataUpdate, batch, BATCH_SIZE);
                dataJSON = new JSONArray();
            }
        }
        if ((i % BATCH_SIZE) != 0) {
            batch++;
            final int batchItems = i - ((batch - 1) * BATCH_SIZE);
            syncParameterAction(requestQueue, dataJSON, dataUpdate, batch, batchItems);
        }
    }

    private void syncParameterAction(RequestQueue requestQueue,
                                     JSONArray dataJSON, List<Parameter> dataUpdate,
                                     int batch, int batchItems)
            throws ExpanseSyncException, JSONException {
        final RequestFuture<JSONObject> requestFuture = RequestFuture.newFuture();
        syncParameterRequest(requestQueue, requestFuture, dataJSON);
        syncParameterResponse(requestQueue, requestFuture, dataUpdate);  // Blocking on get
        Log.d(TAG, "SYNC " + SET_PARAMETER_REQ
                + " BATCH #" + batch + " [" + batchItems + "] items DONE!");
    }

    private JSONObject getJSONFromData(Parameter p)
            throws JSONException {
        return new JSONObject()
                .put("moment", DateTime.epochMillisToTimestampISO8601(p.moment.getTime()))
                .put("moment_tz", p.momentTz)
                .put("key", p.key)
                .put("value", p.value);
    }

    private void syncParameterRequest(RequestQueue requestQueue,
                                      RequestFuture<JSONObject> requestFuture,
                                      JSONArray data)
            throws JSONException {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                syncURL,
                getJSONRequestFromData(data, SET_PARAMETER_REQ),
                requestFuture,
                requestFuture
        );
        request.setTag(SET_PARAMETER_REQ);
        requestQueue.add(request);
    }

    private void syncParameterResponse(RequestQueue requestQueue,
                                       RequestFuture<JSONObject> requestFuture,
                                       List<Parameter> dataUpdate)
            throws ExpanseSyncException {
        try {
            final JSONObject response = requestFuture.get(NETWORK_TIMEOUT, TimeUnit.SECONDS);
            if (response.has(REQ_RESPONSE_OK)) {
                dataCacheDao.reset(dataUpdate.toArray(new Parameter[0]));
                dataUpdate.clear();
            } else {
                throw new ExpanseSyncException("Server NOT OK response: ["
                        + response.getString(REQ_RESPONSE_ERROR) + "]");
            }
        } catch (InterruptedException|ExecutionException|TimeoutException e) {
            requestQueue.cancelAll(SET_PARAMETER_REQ);
            throw new ExpanseSyncException("Request GET FAILED!", e);
        } catch (JSONException e) {
            throw new ExpanseSyncException("Response GET ERROR FAILED!", e);
        }
    }

    // COLLECTED

    private void syncCollected(RequestQueue requestQueue)
            throws ExpanseSyncException, JSONException {
        List<Collected> data = dataCacheDao.getCollectedNotSynced();
        JSONArray dataJSON = new JSONArray();
        List<Collected> dataUpdate = new ArrayList<>(BATCH_SIZE);
        Collected c;
        int batch = 0;
        int i;
        for (i = 0; i < data.size(); i++) {
            c = data.get(i);
            dataJSON.put(getJSONFromData(c));
            c.synced = true;
            dataUpdate.add(c);
            Log.d(TAG, "SYNC " + c.toString());
            if (((i + 1) % BATCH_SIZE) == 0) {
                batch++;
                syncCollectedAction(requestQueue, dataJSON, dataUpdate, batch, BATCH_SIZE);
                dataJSON = new JSONArray();
            }
        }
        if ((i % BATCH_SIZE) != 0) {
            batch++;
            final int batchItems = i - ((batch - 1) * BATCH_SIZE);
            syncCollectedAction(requestQueue, dataJSON, dataUpdate, batch, batchItems);
        }
    }

    private void syncCollectedAction(RequestQueue requestQueue,
                                     JSONArray dataJSON, List<Collected> dataUpdate,
                                     int batch, int batchItems)
            throws ExpanseSyncException, JSONException {
        final RequestFuture<JSONObject> requestFuture = RequestFuture.newFuture();
        syncCollectedRequest(requestQueue, requestFuture, dataJSON);
        final long lastTs = dataUpdate.get(batchItems - 1).moment.getTime();
        syncCollectedResponse(requestQueue, requestFuture, dataUpdate);  // Blocking on get
        collectedLastSync.setTime(lastTs);
        Log.d(TAG, "SYNC " + SET_COLLECTED_REQ
                + " BATCH #" + batch + " [" + batchItems + "] items DONE!");
    }

    private JSONObject getJSONFromData(Collected c)
            throws JSONException {
        return new JSONObject()
                .put("moment", DateTime.epochMillisToTimestampISO8601(c.moment.getTime()))
                .put("moment_tz", c.momentTz)
                .put("sensor_type", c.sensorType)
                .put("sensor_attribute", c.sensorAttribute)
                .put("sensor_value", c.sensorValue)
                .put("sensor_value_extra", (c.sensorValueExtra == null)
                        ? JSONObject.NULL
                        : c.sensorValueExtra);
    }

    private void syncCollectedRequest(RequestQueue requestQueue,
                                      RequestFuture<JSONObject> requestFuture,
                                      JSONArray data)
            throws JSONException {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                syncURL,
                getJSONRequestFromData(data, SET_COLLECTED_REQ),
                requestFuture,
                requestFuture
        );
        request.setTag(SET_COLLECTED_REQ);
        requestQueue.add(request);
    }

    private void syncCollectedResponse(RequestQueue requestQueue,
                                       RequestFuture<JSONObject> requestFuture,
                                       List<Collected> dataUpdate)
            throws ExpanseSyncException {
        try {
            final JSONObject response = requestFuture.get(NETWORK_TIMEOUT, TimeUnit.SECONDS);
            if (response.has(REQ_RESPONSE_OK)) {
                dataCacheDao.reset(dataUpdate.toArray(new Collected[0]));
                dataUpdate.clear();
            } else {
                throw new ExpanseSyncException("Server NOT OK response: ["
                        + response.getString(REQ_RESPONSE_ERROR) + "]");
            }
        } catch (InterruptedException|ExecutionException|TimeoutException e) {
            requestQueue.cancelAll(SET_COLLECTED_REQ);
            throw new ExpanseSyncException("Request GET FAILED!", e);
        } catch (JSONException e) {
            throw new ExpanseSyncException("Response GET ERROR FAILED!", e);
        }
    }

    /*
     *                            C O M M O N
     */

    /**
     * Wraps an Expanse JSON format request with extra for OAuth ID token
     * validation.
     * <pre>
     * {
     *     ID_TOKEN_NAME: "...",
     *     REQUEST_NAME: {
     *         ACTION_NAME: [
     *             {
     *                 FIRST ENTRY
     *             },
     *             {
     *                 LAST ENTRY
     *             }
     *         ]
     *     }
     * }
     * </pre>
     *
     * @see <a href="https://gitlab.com/nunoachenriques/sensei-expanse/tree/master/memory/secure">Schema JSON</a>
     */
    private JSONObject getJSONRequestFromData(JSONArray data, String actionName)
            throws JSONException {
        return new JSONObject()
                .put(ID_TOKEN_NAME, googleSignInAccount.getIdToken())
                .put(REQUEST_NAME, new JSONObject().put(actionName, data));
    }

    /*
     *                        P R E D I C T I O N
     */

    /**
     * Get a sequence of predictions for a sequence of requests with context
     * data. Example:
     * <pre>
     * JSON full request (only context is required in contextData):
     * {
     *   "oauth_id_token": "x.x.x",
     *   "expanse_request": {
     *     "get_prediction": {
     *       "mode": "best",
     *       "context": [
     *         {
     *           "moment": "2001-01-01T01:01:01.000+00:00",
     *           "latitude": 38.0,
     *           "longitude": 9.0
     *         },
     *         {
     *           "moment": "2002-02-02T02:02:02.000+00:00",
     *           "latitude": 38.72,
     *           "longitude": -9.34
     *         }
     *       ],
     *       "estimator": null,
     *       "probabilities": True
     *     }
     *   }
     * }
     *
     * JSON full response (only sequence after "ok" is returned):
     * {
     *   "ok": [
     *     null,
     *     {"xgboost.sklearn.XGBClassifier": {
     *       "score": "0.9883720930232558",
     *       "prediction_classes": [0, 1],
     *       "prediction": "[[0.95415205 0.04584794]]",
     *       "feature_importances": "[0.         0.         0.         ... ]"}}
     *   ]
     * }
     * </pre>
     *
     * @param c Android application {@link Context}.
     * @param wifiRequired True if requires WiFi, false otherwise.
     * @param contextData A sequence of requests with the required data
     *     (e.g., latitude, longitude, moment).
     * @param probabilities Request prediction with all classes and respective
     *     probabilities.
     * @param featureImportances Request the list of features with importance
     *     values if supported by the estimator.
     * @return A sequence of the prediction results, null if no prediction for
     *     a specific request, order is kept. Null if a network exception
     *     occurred. Empty sequence if no connection available (e.g., WiFi).
     */
    @SuppressWarnings("SameParameterValue")
    JSONArray syncPrediction(final Context c, boolean wifiRequired,
                             JSONArray contextData, boolean probabilities, boolean featureImportances) {
        JSONArray result = null;
        if (Connectivity.isConnectionOk(c, wifiRequired)) {
            RequestQueue requestQueue = null;
            try {
                if (syncURL == null) {
                    syncURL = getSyncURL(c);
                }
                googleSignInAccount = getGoogleSignInAccount(c);
                if (googleSignInAccount != null) {
                    requestQueue = Volley.newRequestQueue(c);
                    result = syncPredictionGet(requestQueue, contextData, probabilities, featureImportances);  // Blocking on get
                }
            } catch (Exception e) {
                Log.e(TAG, "SYNC PREDICTIONS FAILED!", e);
            } finally {
                if (requestQueue != null) {
                    requestQueue.stop();
                }
            }
        } else {
            Log.w(TAG, "SYNC PREDICTIONS FAILED! Connectivity UNAVAILABLE."
                    + " Hint: is WiFi required and available?");
            result = new JSONArray();
        }
        return result;
    }

    private JSONArray syncPredictionGet(RequestQueue requestQueue,
                                        JSONArray contextData, boolean probabilities, boolean featureImportances)
            throws ExpanseSyncException {
        try {
            final RequestFuture<JSONObject> requestFuture = RequestFuture.newFuture();
            // REQUEST CREATE
            JSONObject jsonContextData = new JSONObject()
                    .put(ID_TOKEN_NAME, googleSignInAccount.getIdToken())
                    .put(REQUEST_NAME, new JSONObject()
                            .put(GET_PREDICTION_REQ, new JSONObject()
                                    .put("mode", "best")
                                    .put("estimator", JSONObject.NULL)
                                    .put("probabilities", probabilities)
                                    .put("feature_importances", featureImportances)
                                    .put("context", contextData)));
            JsonObjectRequest request = new JsonObjectRequest(
                    Request.Method.POST, syncURL, jsonContextData, requestFuture, requestFuture
            );
            request.setTag(GET_PREDICTION_REQ);
            requestQueue.add(request);
            // RESPONSE GET
            final JSONObject response = requestFuture.get(
                    NETWORK_TIMEOUT * (BuildConfig.DEBUG ? 2 : 1), TimeUnit.SECONDS);
            Log.d("ExpanseSync response:", response.toString(4));
            if (response.has(REQ_RESPONSE_OK)) {
                return response.getJSONArray(REQ_RESPONSE_OK);
            } else {
                throw new ExpanseSyncException("Server NOT OK response: ["
                        + response.getString(REQ_RESPONSE_ERROR) + "]");
            }
        } catch (InterruptedException|ExecutionException|TimeoutException e) {
            throw new ExpanseSyncException("Request GET FAILED! CHECK Web service log...", e);
        } catch (JSONException e) {
            throw new ExpanseSyncException("FAILED! CHECK JSON data format...", e);
        } finally {
            requestQueue.cancelAll(GET_PREDICTION_REQ);
        }
    }
}
