/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import net.nunoachenriques.sensei.settings.Settings;
import net.nunoachenriques.sensei.utility.Availability;
import net.nunoachenriques.sensei.utility.Calc;
import net.nunoachenriques.sensei.utility.Notifications;

import java.util.concurrent.TimeUnit;

/**
 * Creates and manages the main notification and updates empathy progress bar.
 * The service will provide all the basic defaults, i.e., determinate progress
 * bar with empathy values, the action buttons, title and text.
 * Notice: although not mandatory but IF PROVIDED then the progress bar
 * {@link #EXTRA_VALUE_MAX} and {@link #EXTRA_VALUE} MUST BE in [0, 100].
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see DiaryReportService
 * @see FeedService
 * @see HomeostasisService
 * @see ScreenReceiver
 * @see SmileReportService
 */
public final class EmpathyNotifyService
        extends IntentService {

    private static final String TAG = "EmpathyNService";

    public static final int NOTIFICATION_ID = 0xE45;  // ENS
    public static final String EXTRA_VALUE_MAX = TAG + "progress_bar_value_max";
    public static final String EXTRA_VALUE = TAG + "progress_bar_value";

    private static final int CONTENT_RC = 0x5E45E1;  // SENSEI
    private static final int ACTION1_RC = 0x5412E + 1;  // SMILE
    private static final int ACTION2_RC = 0x5412E + 2;
    private static final int ACTION3_RC = 0x5412E + 3;
    private static final long SFX_DURATION = 500;  // ms

    public EmpathyNotifyService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent i) {
        if (i != null && !Availability.isAtRest(getApplicationContext())) {
            final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
            final int max = i.getIntExtra(EmpathyNotifyService.EXTRA_VALUE_MAX,
                    Calc.round(Empathy.MAX_DEFAULT));
            final Integer value = i.getIntExtra(EmpathyNotifyService.EXTRA_VALUE,
                    Calc.round(Settings.getDouble(sp, Empathy.EMPATHY_PREF_KEY)));
            final boolean pause = sp.getBoolean(getResources().getString(R.string.data_pause_mode_pref_key), false);
            final NotificationManager nm = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            if (nm != null) {
                // MAX (e.g., smile report): special effect
                if (value.equals(max)) {
                    nm.notify(NOTIFICATION_ID,
                            buildNotification(max, value, true, pause, true));
                    try {
                        TimeUnit.MILLISECONDS.sleep(SFX_DURATION);
                    } catch (InterruptedException e) {
                        Log.d(TAG, "Sleep SFX_DURATION INTERRUPTED! " + e.getMessage());
                    }
                }
                // REGULAR
                nm.notify(NOTIFICATION_ID,
                        buildNotification(max, value, false, pause, false));
            } else {
                Log.e(TAG, "NotificationManager FAILED!");
            }
        }
    }

    private Notification buildNotification(int max, int value,
                                           boolean indeterminate,
                                           boolean pause,
                                           boolean hideActions) {
        Resources res = getResources();
        NotificationCompat.Builder notificationBuilder = Notifications
                .getDefault(this, true, false)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentIntent(PendingIntent.getActivity(this, CONTENT_RC,
                        new Intent(this, SenseiActivity.class)
                                // https://stackoverflow.com/a/11563909/8418165
                                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                        | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                        | Intent.FLAG_ACTIVITY_NEW_TASK),
                        PendingIntent.FLAG_UPDATE_CURRENT))
                .setColor(Empathy.getInstance(getApplicationContext()).getProgressTint(pause))
                .setProgress(max, value, indeterminate)
                .setContentTitle(res.getString(R.string.empathy_notification_title))
                .setContentText(pause
                        ? res.getString(R.string.empathy_notification_text_pause, value)
                        : res.getString(R.string.empathy_notification_text, value));
        if (!hideActions && !pause) {
            notificationBuilder
                    .addAction(buildAction(
                            R.drawable.ic_face_negative_tint,
                            res.getString(R.string.negative),
                            SmileReportService.getPendingIntent(this,
                                    ACTION1_RC, SmileReportService.NEGATIVE)))
                    .addAction(buildAction(
                            R.drawable.ic_face_neutral_tint,
                            res.getString(R.string.neutral),
                            SmileReportService.getPendingIntent(this,
                                    ACTION2_RC, SmileReportService.NEUTRAL)))
                    .addAction(buildAction(
                            R.drawable.ic_face_positive_tint,
                            res.getString(R.string.positive),
                            SmileReportService.getPendingIntent(this,
                                    ACTION3_RC, SmileReportService.POSITIVE)));
        }
        return notificationBuilder.build();
    }

    private NotificationCompat.Action buildAction(@DrawableRes int icon,
                                                  CharSequence text,
                                                  PendingIntent pi) {
        return new NotificationCompat.Action.Builder(icon, text, pi).build();
    }
}
