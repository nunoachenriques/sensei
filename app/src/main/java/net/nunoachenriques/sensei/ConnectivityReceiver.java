/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Monitor for changes in connectivity (e.g., Internet access).
 * Gets broadcast of state change, process and store it.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class ConnectivityReceiver
        extends BroadcastReceiver {

    public ConnectivityReceiver() {
        super();
    }

    @Override
    public void onReceive(Context c, Intent i) {
        try {
            c.startService(new Intent(c, ConnectivityService.class).putExtras(i));
        } catch (Exception e) { // IllegalState or other...
            Log.e("ConnectivityReceiver", "Start service FAILED!", e);
        }
    }
}
