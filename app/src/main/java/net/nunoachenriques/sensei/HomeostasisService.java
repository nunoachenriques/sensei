/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.tasks.Task;

import net.nunoachenriques.sensei.emotion.Polarity;
import net.nunoachenriques.sensei.persistence.Chat;
import net.nunoachenriques.sensei.persistence.Collected;
import net.nunoachenriques.sensei.persistence.DataCache;
import net.nunoachenriques.sensei.persistence.DataCacheDao;
import net.nunoachenriques.sensei.persistence.Parameter;
import net.nunoachenriques.sensei.settings.Settings;
import net.nunoachenriques.sensei.utility.Availability;
import net.nunoachenriques.sensei.utility.DateTime;
import net.nunoachenriques.sensei.utility.Notifications;
import net.nunoachenriques.sensei.utility.Services;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * The homeostasis {@link JobService} which manages all about the regulation
 * of the agent's stable condition.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class HomeostasisService
        extends JobService {

    private static final String TAG = "HomeostasisService";

    public static final int REPORTING_NOTIFICATION_ID = 0x2E;
    public static final int SIGN_IN_NOTIFICATION_ID = 0x51;

    public static volatile boolean isRunning = false;

    private static final String SENSOR_ID_NOTIFICATION = "app.agent.notification";
    private static final String SENSOR_ATTR_REPORTING = "reporting";
    private static final int SENSEI_ACTIVITY_INTENT_RC = 0x5A5C;
    private enum Service {
        EXPANSE(),
        FEED();
        private long lastTimestamp;  // ms since UNIX Epoch UTC
        Service() {
            lastTimestamp = 0L;
        }
        long getLastTimestamp() {
            return lastTimestamp;
        }
        void setLastTimestamp(long ts) {
            lastTimestamp = ts;
        }
    }
    /** Congruent with sentiment polarity scale (e.g., VADER): [25% neg., 50% neutral, 25% pos.] */
    private static final double REPORTING_EMPATHY_THRESHOLD =
            Empathy.MIN_DEFAULT + 0.25 * (Empathy.MAX_DEFAULT - Empathy.MIN_DEFAULT);
    private static final long CACHE_THRESHOLD_COLLECTED_MS =
            Sensei.CACHE_COLLECTED_DAYS * DateTime.DAY_MS;
    private static final long CACHE_THRESHOLD_PARAMETER_MS =
            Sensei.CACHE_PARAMETER_DAYS * DateTime.DAY_MS;
    private static final ExecutorService executorService = Executors.newSingleThreadExecutor();

    private static volatile DataCache dataCache;
    private static volatile DataCacheDao dataCacheDao;
    private static volatile Intent feedServiceIntent;
    private static volatile JobScheduler jobScheduler;
    private static volatile SharedPreferences sharedPreferences;

    /** Interaction happy birthday message only once a year. */
    private static volatile boolean happyBirthdayMessage = false;
    /** Last emotional valence report timestamp in milliseconds since UTC. */
    private static volatile Calendar lastEmotionalValenceReport = null;

    private long regulateReportingInterval;
    private long regulateServicesInterval;

    @Override
    public boolean onStartJob(final JobParameters jp) {
        isRunning = true;
        Log.i(TAG, "onStartJob JobService [" + jp.getJobId() + "] STARTED!");
        try {
            final Context c = getApplicationContext();
            if (dataCache == null) {
                dataCache = DataCache.getInstance(c);
            }
            if (dataCacheDao == null) {
                dataCacheDao = DataCache.getInstance(c).getDao();
            }
            if (feedServiceIntent == null) {
                feedServiceIntent = new Intent(c, FeedService.class);
            }
            if (jobScheduler == null) {
                jobScheduler = (JobScheduler)getSystemService(Context.JOB_SCHEDULER_SERVICE);
            }
            if (sharedPreferences == null) {
                sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
            }
            String reportingSp = sharedPreferences.getString("homeostasis.reporting.period", null);
            regulateReportingInterval = (reportingSp != null) ? Long.parseLong(reportingSp) : 10800000;
            String serviceSp = sharedPreferences.getString("homeostasis.services.period", null);
            regulateServicesInterval = (serviceSp != null) ? Long.parseLong(serviceSp) : 3600000;
            final boolean now = (jp.getJobId() == Sensei.HOMEOSTASIS_SERVICE_NOW_JOB_ID);
            executorService.execute(() -> {
                regulateServices(Service.FEED, now);
                regulateServices(Service.EXPANSE, now);
            });
            executorService.execute(() -> {
                regulateSignIn();
                Geopositioning.getInstance(c).refresh(); // Pushes location updates
                regulateReporting();
                regulateCache();
                regulatePerformance();
                regulateInteraction();
            });
            executorService.execute(() -> isRunning = false);
        } catch (Exception e) {
            Log.e(TAG, "onStartJob JobService [" + jp.getJobId() + "] FAILED!" + e.getMessage());
            isRunning = false;
        }
        jobFinished(jp, false);
        return true; // True if async processing and calling jobFinished.
    }

    @Override
    public boolean onStopJob(JobParameters jp) {
        Log.w(TAG, "onStopJob JobService [" + jp.getJobId() + "] INTERRUPTING...");
        Services.stopPersistentService(getApplicationContext(), feedServiceIntent);
        Services.stopJobService(jobScheduler, Sensei.EXPANSE_SERVICE_JOB_ID);
        ActivityManager am = ((ActivityManager)getSystemService(Context.ACTIVITY_SERVICE));
        if (am != null) {
            List<ActivityManager.AppTask> tl = am.getAppTasks();
            for (ActivityManager.AppTask t : tl) {
                Log.d(TAG, "onStopJob Task [" + t + "] FINISHED!");
                t.finishAndRemoveTask();
            }
        }
        isRunning = false;
        jobFinished(jp, false);
        Log.w(TAG, "onStopJob JobService [" + jp.getJobId() + "] INTERRUPTED!");
        return false; // False to drop the job. True to reschedule.
    }

    /*
     *                        R E G U L A T O R S
     */

    private void regulateCache() {
        long cacheThresholdMillis;
        // COLLECTED
        cacheThresholdMillis = System.currentTimeMillis();  // (no retention)
        //  * Synced SENSORS except ACTIVITY
        List<Collected> lc = dataCacheDao.getCollectedSyncedOutdatedSensor(new Timestamp(cacheThresholdMillis));
        Log.d(TAG, "regulateCache [" + lc.size() + "] Collected (SENSORS) items SYNCED and OLDER THAN ["
                + DateTime.epochMillisToDateISO8601(cacheThresholdMillis) + "]!");
        if (lc.size() > 0) {
            dataCacheDao.delete(lc.toArray(new Collected[0]));
            Log.i(TAG, "CACHE CLEANER [" + lc.size() + "] Collected (SENSORS) items SYNCED and OLDER THAN ["
                    + DateTime.epochMillisToDateISO8601(cacheThresholdMillis) + "] DONE!");
        }
        //  * Synced EMPATHY
        lc = dataCacheDao.getCollectedSyncedOutdated(new Timestamp(cacheThresholdMillis), Empathy.SENSOR_ID);
        Log.d(TAG, "regulateCache [" + lc.size() + "] Collected (EMPATHY) items SYNCED and OLDER THAN ["
                + DateTime.epochMillisToDateISO8601(cacheThresholdMillis) + "]!");
        if (lc.size() > 0) {
            dataCacheDao.delete(lc.toArray(new Collected[0]));
            Log.i(TAG, "CACHE CLEANER [" + lc.size() + "] Collected (EMPATHY) items SYNCED and OLDER THAN ["
                    + DateTime.epochMillisToDateISO8601(cacheThresholdMillis) + "] DONE!");
        }
        cacheThresholdMillis = System.currentTimeMillis() - CACHE_THRESHOLD_COLLECTED_MS;  // (older than 28 days)
        //  * Synced ALL
        lc = dataCacheDao.getCollectedSyncedOutdated(new Timestamp(cacheThresholdMillis));
        Log.d(TAG, "regulateCache [" + lc.size() + "] Collected items SYNCED and OLDER THAN ["
                + DateTime.epochMillisToDateISO8601(cacheThresholdMillis) + "]!");
        if (lc.size() > 0) {
            dataCacheDao.delete(lc.toArray(new Collected[0]));
            Log.i(TAG, "CACHE CLEANER [" + lc.size() + "] Collected items SYNCED and OLDER THAN ["
                    + DateTime.epochMillisToDateISO8601(cacheThresholdMillis) + "] DONE!");
        }
        // PARAMETER
        cacheThresholdMillis = System.currentTimeMillis() - CACHE_THRESHOLD_PARAMETER_MS;
        List<Parameter> lp = dataCacheDao.getParameterSyncedOutdated(new Timestamp(cacheThresholdMillis));
        Log.d(TAG, "regulateCache [" + lp.size() + "] Parameter items SYNCED and OLDER THAN ["
                + DateTime.epochMillisToDateISO8601(cacheThresholdMillis) + "]!");
        if (lp.size() > 0) {
            dataCacheDao.delete(lp.toArray(new Parameter[0]));
            Log.i(TAG, "CACHE CLEANER [" + lp.size() + "] Parameter items SYNCED and OLDER THAN ["
                    + DateTime.epochMillisToDateISO8601(cacheThresholdMillis) + "] DONE!");
        }
        // ENTITY
        // Kept during agent's lifetime (while installed)
        // CHAT/DIARY
        // Kept during agent's lifetime (while installed)
    }

    private void regulateInteraction() {
        final Resources res = getResources();
        final Timestamp ts = new Timestamp(System.currentTimeMillis());
        final String tz = DateTime.getTimeZoneISO8601();
        final String language = Locale.getDefault().getLanguage();
        // Check BIRTHDAY
        final Calendar now = GregorianCalendar.getInstance();
        final String birthdayText = sharedPreferences.getString(
                res.getString(R.string.human_birthday_pref_key), null);
        Log.d(TAG, "regulateInteraction now [" + now + "] birthday [" + birthdayText + "]");
        if (birthdayText != null) {
            final Calendar birthday = GregorianCalendar.getInstance();
            birthday.setTimeInMillis(Date.valueOf(birthdayText).getTime());
            if (now.get(Calendar.MONTH) == birthday.get(Calendar.MONTH)
                    && now.get(Calendar.DAY_OF_MONTH) == birthday.get(Calendar.DAY_OF_MONTH)
                    && !happyBirthdayMessage) {
                final String name = sharedPreferences.getString(
                        res.getString(R.string.human_name_pref_key), null);
                dataCacheDao.set(new Chat(ts, tz, Chat.AGENT,
                        res.getString(R.string.interaction_happy_birthday, name), language, Polarity.NO_POLARITY));
                happyBirthdayMessage = true;
            }
            if (now.get(Calendar.MONTH) != birthday.get(Calendar.MONTH)
                    || now.get(Calendar.DAY_OF_MONTH) != birthday.get(Calendar.DAY_OF_MONTH)) {
                happyBirthdayMessage = false;
            }
        }
        // EMOTIONAL VALENCE Report (every **period** hours)
        //final int period = 1;  // Hours DEBUG
        final int period = 4;  // Hours
        if ((now.get(Calendar.HOUR_OF_DAY) % period) == 0) {
            if (lastEmotionalValenceReport == null) {
                emotionalValenceReportChat(ts, tz, language);
            } else {
                now.add(Calendar.HOUR_OF_DAY, -period);
                if (now.after(lastEmotionalValenceReport)) {
                    emotionalValenceReportChat(ts, tz, language);
                }
            }
        }
    }

    private void emotionalValenceReportChat(Timestamp ts, String tz, String language) {
        Resources res = getResources();
        List<Collected> collectedPolarity =
                dataCacheDao.getCollectedByAttribute(Polarity.SENSOR_ATTR_POLARITY_COMMON);
        double emotionalValence = Reasoning.currentEmotionalValence(collectedPolarity);
        if (!Polarity.isInvalid(emotionalValence)) {
            final int[] r = Polarity.polarityToResources(emotionalValence);
            dataCacheDao.set(new Chat(ts, tz, Chat.AGENT,
                    res.getString(R.string.interaction_emotional_valence_report,
                            res.getString(r[1]), emotionalValence, getLastActivity()),
                    language, emotionalValence));
            lastEmotionalValenceReport = GregorianCalendar.getInstance();
        }
    }

    private int getLastActivity() {
        List<Collected> lc =
                dataCacheDao.getCollectedLast(ActivityFeed.SENSOR_ID, ActivityFeed.SENSOR_FIXED_ATTRS);
        if (lc.size() == 3) {
            /*
             * The select for the last activity order by id desc (reversed):
             *  0 = activity elapsed
             *  1 = activity transition
             *  2 = activity type
             */
            return lc.get(2).sensorValue.intValue();
        } else {
            return DetectedActivity.UNKNOWN;
        }

    }

    private void regulatePerformance() {
        // SQLite database performance each Monday at dawn if not awake.
        final Calendar calendar = GregorianCalendar.getInstance();
        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY
                && calendar.get(Calendar.HOUR_OF_DAY) > 0
                && calendar.get(Calendar.HOUR_OF_DAY) < 5
                && Availability.isAtRest(getApplicationContext())) {
            Log.i(TAG, "PRAGMA optimize STARTED...");
            dataCache.query("PRAGMA optimize", null);
            Log.i(TAG, "PRAGMA optimize ENDED!");
        }
    }

    private void regulateReporting() {
        final Calendar calendar = GregorianCalendar.getInstance();
        final long elapsedSinceLast = calendar.getTimeInMillis() - SmileReportService.lastTimestamp;
        final Resources res = getResources();
        final boolean pause = sharedPreferences.getBoolean(res.getString(R.string.data_pause_mode_pref_key), false);
        Log.d(TAG, "pause [" + pause + "] elapsedSinceLast [" + elapsedSinceLast
                + "] regulateReportingInterval [" + regulateReportingInterval + "]");
        if (!pause
                && elapsedSinceLast > regulateReportingInterval
                && calendar.get(Calendar.HOUR_OF_DAY) > 8
                && calendar.get(Calendar.HOUR_OF_DAY) < 23
                && !Availability.isAtRest(getApplicationContext())) {
            double empathy = Settings.getDouble(sharedPreferences, Empathy.EMPATHY_PREF_KEY);
            if (!Double.isNaN(empathy) && empathy < REPORTING_EMPATHY_THRESHOLD) {
                final NotificationManager nm = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
                if (nm != null) {
                    nm.notify(REPORTING_NOTIFICATION_ID, buildNotification(
                            res.getString(R.string.homeostasis_reporting_notification_title),
                            res.getString(R.string.homeostasis_reporting_notification_text_big),
                            res.getString(R.string.homeostasis_reporting_notification_text_big),
                            TimeUnit.SECONDS.toMillis(10)));
                    final Timestamp ts = new Timestamp(System.currentTimeMillis());
                    final String tz = DateTime.getTimeZoneISO8601();
                    final String language = Locale.getDefault().getLanguage();
                    // Collect notification event
                    dataCacheDao.set(new Collected(ts, tz,
                            SENSOR_ID_NOTIFICATION, SENSOR_ATTR_REPORTING, 1.0, null, false));
                    // Chat about it
                    dataCacheDao.set(new Chat(ts, tz, Chat.AGENT,
                            res.getString(R.string.homeostasis_reporting_notification_text_big),
                            language, Polarity.NO_POLARITY));
                }
            }
        }
    }

    private void regulateServices(Service s, boolean now) {
        Log.d(TAG, String.format("[%13d]ms %21s | %7s | %s",
                regulateServicesInterval, "regulateServices", s.name(), (now ? "now" : "NOT now")));
        if (isPermissionOk(this)) {
            final long t = System.currentTimeMillis();
            switch (s) {
                case EXPANSE: // job scheduled SERVICE
                    final long elapsedExpanse = t - Service.EXPANSE.getLastTimestamp();
                    Log.d(TAG, String.format("[%13d]ms %21s | %7s",
                            elapsedExpanse, "elapsed", s.name()));
                    if (now || elapsedExpanse > regulateServicesInterval) {
                        if (now) {
                            Services.stopJobService(jobScheduler, Sensei.EXPANSE_SERVICE_JOB_ID);
                        }
                        if (!Services.isJobScheduled(jobScheduler, Sensei.EXPANSE_SERVICE_JOB_ID)) {
                            String periodSp = sharedPreferences.getString("expanse.period", null);
                            long period = (periodSp != null) ? Long.parseLong(periodSp) : 3600000;
                            String wifi_key = getResources().getString(R.string.data_wifi_pref_key);
                            boolean wifi = sharedPreferences.getBoolean(wifi_key, true);
                            Services.schedulePeriodicJobService(
                                    this, jobScheduler, ExpanseService.class,
                                    Sensei.EXPANSE_SERVICE_JOB_ID, period, false, wifi);
                        }
                        Service.EXPANSE.setLastTimestamp(t);
                        Log.d(TAG, String.format("[%13d]ms %21s | %7s",
                                Service.EXPANSE.getLastTimestamp(), "lastTimestamp updated", s.name()));
                    }
                    break;
                case FEED: // persistent foreground SERVICE
                    final long elapsedFeed = t - Service.FEED.getLastTimestamp();
                    Log.d(TAG, String.format("[%13d]ms %21s | %7s",
                            elapsedFeed, "elapsed", s.name()));
                    if (now || elapsedFeed > regulateServicesInterval) {
                        ActivityManager am = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE));
                        if (now) {
                            Services.stopPersistentService(getApplicationContext(), feedServiceIntent);
                        }
                        if (!Services.isServiceRunning(am, FeedService.class)) {
                            Services.startPersistentService(getApplicationContext(), feedServiceIntent);
                        }
                        Service.FEED.setLastTimestamp(t);
                        Log.d(TAG, String.format("[%13d]ms %21s | %7s",
                                Service.FEED.getLastTimestamp(), "lastTimestamp updated", s.name()));
                    }
                    break;
            }
        } else {
            startActivity(new Intent(this, SenseiActivity.class)
                    // https://stackoverflow.com/a/11563909/8418165
                    .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                            | Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | Intent.FLAG_ACTIVITY_NEW_TASK)
                    .putExtra(SenseiActivity.EXTRA_RESET_PERMISSIONS, true));
        }
    }

    private void regulateSignIn() {
        GoogleSignInClient googleSignInClient = GoogleIdentity.getSignInClient(getApplicationContext());
        if (googleSignInClient != null) {
            Task<GoogleSignInAccount> signIn = googleSignInClient.silentSignIn();
            if (signIn.isSuccessful()) {
                Log.d(TAG, "regulateSignIn SILENT SIGN IN OK!");
            } else {
                signIn.addOnCompleteListener(task -> {
                    try {
                        task.getResult(ApiException.class);
                        Log.d(TAG, "regulateSignIn SILENT SIGN IN OK! (onComplete)");
                    } catch (ApiException e) {
                        final int status = e.getStatusCode();
                        if (status == GoogleSignInStatusCodes.SIGN_IN_REQUIRED) {
                            Log.w(TAG, "regulateSignIn FAILED:"
                                    + " [" + status + "] "
                                    + GoogleSignInStatusCodes.getStatusCodeString(status));
                            final NotificationManager nm =
                                    (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
                            if (nm != null) {
                                final Resources res = getResources();
                                nm.notify(SIGN_IN_NOTIFICATION_ID, buildNotification(
                                        res.getString(R.string.homeostasis_sign_in_notification_title),
                                        res.getString(R.string.google_sign_in_required),
                                        res.getString(R.string.google_sign_in_required),
                                        TimeUnit.MINUTES.toMillis(10)));
                            }
                        } else {
                            Log.e(TAG, "regulateSignIn FAILED:"
                                    + " [" + status + "] "
                                    + GoogleSignInStatusCodes.getStatusCodeString(status));
                        }
                    }
                });
            }
        } else {
            Log.e(TAG, "googleSignInClient is NULL! Hint: should be temporary.");
        }
    }

    /*
     *                         U T I L I T I E S
     */

    private boolean isPermissionOk(Context c) {
        try {
            String pn = c.getPackageName();
            PackageManager pm = c.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(pn, PackageManager.GET_PERMISSIONS);
            for (String p : pi.requestedPermissions) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P
                        && p.equals(Manifest.permission.FOREGROUND_SERVICE)) {
                    continue;
                }
                if (pm.isPermissionRevokedByPolicy(p, pn)) {
                    Log.w(TAG, p + " REVOKED!");
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            Log.e(TAG, "isPermissionOk FAILED! ", e);
            return false;
        }
    }

    private Notification buildNotification(CharSequence title,
                                           CharSequence text,
                                           CharSequence bigText,
                                           long durationMs) {
        return Notifications.getHeadsUp(getApplicationContext(), false, true)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(title)
                .setContentText(text)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(bigText))
                .setTimeoutAfter(durationMs)
                .setContentIntent(PendingIntent.getActivity(getApplicationContext(),
                        SENSEI_ACTIVITY_INTENT_RC,
                        new Intent(this, SenseiActivity.class)
                                // https://stackoverflow.com/a/11563909/8418165
                                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                        | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                        | Intent.FLAG_ACTIVITY_NEW_TASK),
                        PendingIntent.FLAG_UPDATE_CURRENT))
                .build();
    }
}
