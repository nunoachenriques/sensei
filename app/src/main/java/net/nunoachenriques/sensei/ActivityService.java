/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;

import com.google.android.gms.location.ActivityTransition;
import com.google.android.gms.location.ActivityTransitionEvent;
import com.google.android.gms.location.ActivityTransitionResult;
import com.google.android.gms.location.DetectedActivity;

import net.nunoachenriques.sensei.persistence.Collected;
import net.nunoachenriques.sensei.persistence.DataCacheDaoService;
import net.nunoachenriques.sensei.utility.DateTime;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The activity transition (e.g., bike, car, running) service for the feed
 * listener. Gets data from the intent, process and store it.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see ActivityFeed
 */
public final class ActivityService
        extends IntentService {

    private static final String TAG = "ActivityService";
    private static final Map<Integer, String> TYPE_NAME =
            new HashMap<Integer, String>(5) {{
                put(DetectedActivity.IN_VEHICLE, "IN_VEHICLE");
                put(DetectedActivity.ON_BICYCLE, "ON_BICYCLE");
                put(DetectedActivity.RUNNING, "RUNNING");
                put(DetectedActivity.STILL, "STILL");
                put(DetectedActivity.WALKING, "WALKING");
            }};
    private static final Map<Integer, String> TRANSITION_NAME =
            new HashMap<Integer, String>(2) {{
                put(ActivityTransition.ACTIVITY_TRANSITION_ENTER, "ENTER");
                put(ActivityTransition.ACTIVITY_TRANSITION_EXIT, "EXIT");
            }};

    public ActivityService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent i) {
        if (ActivityTransitionResult.hasResult(i)) {
            ActivityTransitionResult atr = ActivityTransitionResult.extractResult(i);
            if (atr != null) {
                List<Collected> lc = new ArrayList<>(ActivityFeed.SENSOR_FIXED_ATTRS);
                String tz = DateTime.getTimeZoneISO8601();
                int ateType;
                int ateTransition;
                long ateElapsed;
                int addTimeMillis = 0;  // To avoid duplicates (moment as index)
                for (ActivityTransitionEvent ate : atr.getTransitionEvents()) {
                    ateType = ate.getActivityType();
                    ateTransition = ate.getTransitionType();
                    ateElapsed = ate.getElapsedRealTimeNanos();
                    Timestamp ts = DateTime.bootNanosToTimestamp(ateElapsed);
                    ts.setTime(ts.getTime() + addTimeMillis);
                    lc.add(new Collected(ts, tz,
                            ActivityFeed.SENSOR_ID, ActivityFeed.SENSOR_ATTR_TYPE,
                            ateType, TYPE_NAME.get(ateType),
                            false));
                    lc.add(new Collected(ts, tz,
                            ActivityFeed.SENSOR_ID, ActivityFeed.SENSOR_ATTR_TRANSITION,
                            ateTransition, TRANSITION_NAME.get(ateTransition),
                            false));
                    lc.add(new Collected(ts, tz,
                            ActivityFeed.SENSOR_ID, ActivityFeed.SENSOR_ATTR_ELAPSED,
                            (double)ateElapsed, null,
                            false));
                    // To distinguish close events: add 1 second to next transition within the same timestamp
                    addTimeMillis += 1000;
                }
                try {
                    startService(new Intent(this, DataCacheDaoService.class)
                            .putExtra(DataCacheDaoService.EXTRA_COLLECTED_SET, lc.toArray(new Collected[0])));
                } catch (Exception e) {
                    Log.d(TAG, "ACTIVITY DATA DISCARDED! " + e.getMessage());
                }
            }
        }
    }

    /**
     * Gets the corresponding name of the activity type.
     *
     * @param t The type value as in {@link DetectedActivity}.
     * @return A text name corresponding to the type value.
     */
    public static String getTypeName(int t) {
        return TYPE_NAME.get(t);
    }

    /**
     * Gets the corresponding name of the activity transition.
     *
     * @param t The transition value as in {@link ActivityTransition}.
     * @return A text name corresponding to the transition value.
     */
    public static String getTransitionName(int t) {
        return TRANSITION_NAME.get(t);
    }

    /**
     * Converts the detected activity given value to the following resources
     * identifier: string (name), drawable (activity). Example:
     * <pre>{@code
     * ...
     * Resources res = getResources();
     * Resources.Theme theme = getTheme();
     * int[] resource = ActivityService.activityToResources(activity);
     * String name = res.getString(resource[0]);
     * Drawable icon = res.getDrawable(resource[1], theme);
     * ...}</pre>
     *
     * @param activity From {@link DetectedActivity}.
     * @return
     * On success (activity-based resources):
     * <pre>
     * {{@link androidx.annotation.StringRes}, {@link androidx.annotation.DrawableRes}}.
     * </pre>
     * On failure:
     * <pre>
     * {R.string.question_mark, R.drawable.ic_question_mark}.
     * </pre>
     */
    public static int[] activityToResources(int activity) {
        switch (activity) {
            case DetectedActivity.IN_VEHICLE:
                return new int[]{R.string.dashboard_you_activity_vehicle, R.drawable.ic_activity_car};
            case DetectedActivity.ON_BICYCLE:
                return new int[]{R.string.dashboard_you_activity_bicycle, R.drawable.ic_activity_bike};
            case DetectedActivity.RUNNING:
                return new int[]{R.string.dashboard_you_activity_running, R.drawable.ic_activity_run};
            case DetectedActivity.STILL:
                return new int[]{R.string.dashboard_you_activity_still, R.drawable.ic_activity_still};
            case DetectedActivity.WALKING:
                return new int[]{R.string.dashboard_you_activity_walking, R.drawable.ic_activity_walk};
            default:
                return new int[]{R.string.question_mark, R.drawable.ic_question_mark};
        }
    }
}
