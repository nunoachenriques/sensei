/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import android.util.Pair;

import androidx.annotation.Nullable;

import net.nunoachenriques.sensei.emotion.Polarity;
import net.nunoachenriques.sensei.emotion.Sentiment;
import net.nunoachenriques.sensei.persistence.Chat;
import net.nunoachenriques.sensei.persistence.Collected;
import net.nunoachenriques.sensei.persistence.DataCacheDaoService;
import net.nunoachenriques.sensei.utility.DateTime;

import java.sql.Timestamp;

/**
 * The diary report service for the text input by the human.
 * Gets data from the intent, process and store it.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see DiaryActivity
 */
public final class DiaryReportService
        extends IntentService {

    private static final String TAG = "DiaryReportService";
    private static final String SENSOR_ATTR_TEXT = "text";
    private static final String SENSOR_ATTR_POLARITY = Polarity.SENSOR_ATTR_POLARITY_COMMON;

    public static final String SENSOR_ID = "app.human.chat";
    public static final String EXTRA_TEXT = TAG + SENSOR_ATTR_TEXT;

    public DiaryReportService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent i) {
        if (i != null) {
            try {
                String text = i.getStringExtra(DiaryReportService.EXTRA_TEXT);
                if (text != null && !text.trim().isEmpty()) {
                    Timestamp ts = new Timestamp(System.currentTimeMillis());
                    String tz = DateTime.getTimeZoneISO8601();
                    // LANGUAGE + SENTIMENT
                    Float polarity = Sentiment.NO_SENTIMENT;
                    String language = null;
                    Pair<Float, String> polarityLanguage =
                            Sentiment.getInstance(getApplicationContext()).getPolarityLanguage(text);
                    if (polarityLanguage != null) {
                        polarity = polarityLanguage.first;
                        language = polarityLanguage.second;
                    }
                    // STORE (local with full content, cloud anonymized with sentiment only)
                    startService(new Intent(this, DataCacheDaoService.class)
                            .putExtra(DataCacheDaoService.EXTRA_CHAT_SET,
                                    new Chat[]{new Chat(ts, tz, Chat.HUMAN, text, language, polarity.doubleValue())}));
                    startService(new Intent(this, DataCacheDaoService.class)
                            .putExtra(DataCacheDaoService.EXTRA_COLLECTED_SET,
                                    new Collected[]{new Collected(
                                            ts, tz,
                                            SENSOR_ID, SENSOR_ATTR_POLARITY, polarity.doubleValue(), language,
                                            false)}));
                    // EMPATHY reset to MAX!
                    Empathy.getInstance(this).set(Empathy.MAX_DEFAULT);
                    // NOTIFICATION
                    startService(new Intent(this, EmpathyNotifyService.class)
                            .putExtra(EmpathyNotifyService.EXTRA_VALUE_MAX, (int)Empathy.MAX_DEFAULT)
                            .putExtra(EmpathyNotifyService.EXTRA_VALUE, (int)Empathy.MAX_DEFAULT));
                    // LOCATION (GPS)
                    Geopositioning.getInstance(this).get()
                            .addOnSuccessListener(v -> Log.d(TAG, "GEOPOSITIONING NOW successful!"))
                            .addOnFailureListener(e -> Log.e(TAG, "GEOPOSITIONING NOW FAILED!", e));
                } else {
                    Log.d(TAG, "DIARY reported DATA DISCARDED! Text is EMPTY!");
                }
            } catch (Exception e) {
                Log.e(TAG, "UNKNOWN FAILURE!", e);
            }
        }
    }
}
