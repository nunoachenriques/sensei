/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.util.Pair;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.nunoachenriques.sensei.utility.Calc;

import java.util.Arrays;

/**
 * A {@link Drawable} for a circular histogram with {@code n} bins (quantity).
 * A ring shape with {@code n} different colors shaped proportionally to the
 * total (sum).
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
final class HistogramCircularDrawable
        extends Drawable {

    private static final String TAG = "HistogramCDrawable";
    private static final int START_ANGLE = 270;
    private static final float STROKE_WIDTH_DEFAULT = 16;
    private static final float HALF_STROKE_WIDTH_DEFAULT = STROKE_WIDTH_DEFAULT / 2;

    private final int quantity;
    private final HistogramBin[] histogramBin;
    private final long[] histogramBinValue;
    private final Paint[] histogramBinPaint;

    static class HistogramBin {
        float angle;
        float sweep;
        HistogramBin() {}
    }

    /**
     * Creates the drawable with {@code n} bins.
     *
     * @param n The number of bins for the histogram.
     */
    HistogramCircularDrawable(@SuppressWarnings("SameParameterValue") int n) {
        super();
        quantity = n;
        histogramBin = new HistogramBin[n];
        histogramBinValue = new long[n];
        histogramBinPaint = new Paint[n];
        // Defaults
        int step = 360 / n;
        for (int i = 0; i < n; i++) {
            Paint p = new Paint();
            p.setColor(Color.HSVToColor(255, new float[]{step * i, 1, 0.6f}));
            p.setStyle(Paint.Style.STROKE);
            p.setStrokeWidth(STROKE_WIDTH_DEFAULT);
            p.setAntiAlias(true);
            histogramBinPaint[i] = p;
        }
        Arrays.fill(histogramBinValue, step);
        setBins();
    }

    @SuppressWarnings("SuspiciousNameCombination")
    @Override
    public void draw(@NonNull Canvas c) {
        int width = getBounds().width();
        int height = getBounds().height();
        for (int i = 0; i < quantity; i++) {
            c.drawArc(HALF_STROKE_WIDTH_DEFAULT, HALF_STROKE_WIDTH_DEFAULT, width - HALF_STROKE_WIDTH_DEFAULT, height - HALF_STROKE_WIDTH_DEFAULT, histogramBin[i].angle, histogramBin[i].sweep, false, histogramBinPaint[i]);
        }
    }

    @Override
    public void setAlpha(int a) {
        for (int i = 0; i < quantity; i++) {
            histogramBinPaint[i].setAlpha(a);
        }
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter cf) {
        for (int i = 0; i < quantity; i++) {
            histogramBinPaint[i].setColorFilter(cf);
        }
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

    /**
     * Sets a list of ({@code bin}, {@code value}) pairs.
     *
     * @param binValuePairs Bin (Pair.first) zero-based index [0, n] and the bin's count value (Pair.second).
     */
    void setValues(Iterable<Pair<Integer, Long>> binValuePairs) {
        for (Pair<Integer, Long> p : binValuePairs) {
            histogramBinValue[p.first] = p.second;
        }
        setBins();
    }

    /**
     * Sets the {@code bin} index with the given color.
     *
     * @param bin Zero-based index, i.e., [0, n].
     * @param c {@link Color} integer value.
     */
    void setColor(int bin, @ColorInt int c) {
        histogramBinPaint[bin].setColor(c);
    }

    private void setBins() {
        long sum = 0;
        for (int i = 0; i < quantity; i++) {
            sum += histogramBinValue[i];
        }
        for (int i = 0; i < quantity; i++) {
            float angle = (i == 0 ? START_ANGLE : histogramBin[i - 1].angle + histogramBin[i - 1].sweep);
            float sweep = (float)Calc.convertCodomain(histogramBinValue[i], 0, sum, 0, 360);
            if (histogramBin[i] == null) {
                histogramBin[i] = new HistogramBin();
                Log.d(TAG, "setBins #" + i + " | new HistogramBin()");
            }
            histogramBin[i].angle = angle;
            histogramBin[i].sweep = sweep;
            Log.d(TAG, "setBins #" + i + " | angle: " + histogramBin[i].angle + " | sweep: " + histogramBin[i].sweep + " | Color: " + histogramBinPaint[i].getColor());
        }
    }
}
