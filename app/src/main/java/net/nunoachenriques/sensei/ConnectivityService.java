/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.app.IntentService;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;

import androidx.annotation.Nullable;

import net.nunoachenriques.sensei.persistence.Collected;
import net.nunoachenriques.sensei.persistence.DataCacheDaoService;
import net.nunoachenriques.sensei.utility.Connectivity;
import net.nunoachenriques.sensei.utility.DateTime;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The connectivity (type and more) service for the feed listener.
 * Gets data from the intent, process and store it.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see ConnectivityReceiver
 */
public final class ConnectivityService
        extends IntentService {

    private static final String SENSOR_ID = "app.sensor.connectivity";

    private static final String TAG = "ConnectivityService";
    private static final String CONNECTED_KEY = "connected";
    private static final String TYPE_KEY = "type";
    private static final String BSSID_KEY = "bssid";
    private static final String SSID_KEY = "ssid";
    private static final int NO_VALUE = -1;

    /**
     * Previous stored value of interest of {@link #CONNECTED_KEY} and
     * {@link #TYPE_KEY} attributes. Is used to filter and store only the values
     * of interest, i.e., different from the previous.
     */
    private static final Map<String, Double> lastValue =
            new HashMap<String, Double>(2) {{
                put(CONNECTED_KEY, null);
                put(TYPE_KEY, null);
            }};

    /**
     * Previous stored value of interest of {@link #BSSID_KEY} and
     * {@link #SSID_KEY} attributes. Is used to filter and store only the values
     * of interest, i.e., different from the previous.
     */
    private static final Map<String, String> lastValueWifi =
            new HashMap<String, String>(2) {{
                put(BSSID_KEY, null);
                put(SSID_KEY, null);
            }};

    public ConnectivityService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent i) {
        if (i != null) {
            List<Collected> lc = new ArrayList<>();
            Timestamp ts = new Timestamp(System.currentTimeMillis());
            String tz = DateTime.getTimeZoneISO8601();
            boolean connected = true;
            if (i.hasExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY)) { // NOT CONNECTED?
                connected = !i.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, true);
            }
            double v = (connected ? 1.0 : 0.0);
            Double lastValueConnected = lastValue.get(CONNECTED_KEY);
            if (lastValueConnected == null
                    || (int) v != lastValueConnected.intValue()) {
                lc.add(new Collected(ts, tz, SENSOR_ID, CONNECTED_KEY, v, null, false));
                lastValue.put(CONNECTED_KEY, v);
            }
            if (connected) {
                Double type = (double)i.getIntExtra(ConnectivityManager.EXTRA_NETWORK_TYPE, NO_VALUE);
                if (type != (double)NO_VALUE
                        && !type.equals(lastValue.get(TYPE_KEY))) {
                    lc.add(new Collected(ts, tz, SENSOR_ID, TYPE_KEY, type, null, false));
                    lastValue.put(TYPE_KEY, type);
                    // WiFi?
                    if (type.intValue() == ConnectivityManager.TYPE_WIFI) {
                        String[] wifiId = Connectivity.getWiFiId(this);
                        if (wifiId[0] != null
                                && !wifiId[0].equals(lastValueWifi.get(BSSID_KEY))
                                && !wifiId[1].equals(lastValueWifi.get(SSID_KEY))) {
                            lc.add(new Collected(ts, tz, SENSOR_ID, BSSID_KEY, 0.0, wifiId[0], false));
                            lastValueWifi.put(BSSID_KEY, wifiId[0]);
                            lc.add(new Collected(ts, tz, SENSOR_ID, SSID_KEY, 0.0, wifiId[1], false));
                            lastValueWifi.put(SSID_KEY, wifiId[1]);
                        }
                    }
                }
            }
            if (lc.size() > 0) {
                startService(new Intent(this, DataCacheDaoService.class)
                        .putExtra(DataCacheDaoService.EXTRA_COLLECTED_SET, lc.toArray(new Collected[0])));
            } else {
                Log.d(TAG, "CONNECTIVITY DATA DISCARDED!");
            }
        }
    }
}
