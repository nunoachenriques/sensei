/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import net.nunoachenriques.sensei.persistence.Collected;
import net.nunoachenriques.sensei.persistence.DataCache;
import net.nunoachenriques.sensei.persistence.DataCacheDao;
import net.nunoachenriques.sensei.utility.DateTime;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * The geopositioning (location and more) feed listener.
 * Catches data from listening fused location services and sends it to the
 * service for processing and storage.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class Geopositioning {

    private static final String SENSOR_ID = "app.sensor.geopositioning";

    private static final String TAG = "Geopositioning";
    private static final long ET_INTERVAL_RESTRICTION = TimeUnit.MILLISECONDS.toNanos(100);

    private static volatile Geopositioning INSTANCE;
    private static volatile Location lastLocation = null;
    private static volatile long lastCollectedDataTime = 0;

    private final Long interval;
    private final Long intervalFastest;
    private final Float displacementThreshold;
    private final DataCacheDao dataCacheDao;
    private final ExecutorService executorService;
    private final SettingsClient settingsClient;
    private final LocationCallback locationCallback;
    private final FusedLocationProviderClient fusedLocation;

    private LocationRequest locationRequestNow;
    private LocationRequest locationRequestBalanced;
    private LocationSettingsRequest locationSettingsRequest;
    private boolean started;

    private Geopositioning(Context c) {
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(c);
        final String intervalSp = sp.getString("geopositioning.interval", null);
        interval = (intervalSp != null) ? Long.parseLong(intervalSp) : 60000;
        final String intervalFastestSp = sp.getString("geopositioning.interval_fastest", null);
        intervalFastest = (intervalFastestSp != null) ? Long.parseLong(intervalFastestSp) : 5000;
        final String displacementThresholdSp = sp.getString("geopositioning.displacement_threshold", null);
        displacementThreshold = (displacementThresholdSp != null) ? Float.parseFloat(displacementThresholdSp) : 10.0f;
        Log.d(TAG, "PREFERENCES: interval [" + interval
                + "]ms fastest [" + intervalFastest
                + "]ms displacement [" + displacementThreshold + "]m");
        dataCacheDao = DataCache.getInstance(c).getDao();
        executorService = Executors.newSingleThreadExecutor();
        locationCallback = locationCallback();
        fusedLocation = LocationServices.getFusedLocationProviderClient(c);
        createLocationRequestNow();
        createLocationRequestBalanced();
        buildLocationSettingsRequest();
        settingsClient = LocationServices.getSettingsClient(c);
        started = false;
    }

    /**
     * Gets an instance of this class. If no single instance is available,
     * i.e., {@code null}, then creates and returns a single new one
     * (Singleton pattern).
     *
     * @param c Android application {@link Context}.
     * @return The single instance of this class.
     */
    public static Geopositioning getInstance(Context c) {
        if (INSTANCE == null) {
            synchronized (Geopositioning.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Geopositioning(c);
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Checks all the defined types of requests settings.
     *
     * @return a Task for the call, check {@link Task}.
     * @see SettingsClient
     */
    Task<LocationSettingsResponse> checkSettings() {
        return settingsClient.checkLocationSettings(locationSettingsRequest);
    }

    /**
     * Starts (requests) receiving location updates.
     *
     * @return a Task for the call, check {@link Task}.
     * @throws SecurityException On permission failure.
     */
    Task<Void> start()
            throws SecurityException {
        return fusedLocation.requestLocationUpdates(
                locationRequestBalanced, locationCallback, null)
                .addOnCompleteListener(t -> {
                    if (t.isSuccessful()) {
                        started = true;
                    }
                });
    }

    /**
     * Stops listening location services.
     *
     * @return a Task for the call, check {@link Task}.
     */
    Task<Void> shutdown() {
        started = false;
        return fusedLocation.removeLocationUpdates(locationCallback);
    }

    /**
     * Gets one single location update on demand and with high accuracy.
     * NOTICE: Geopositioning must be in started mode.
     *
     * @return a Task for the call, check {@link Task}.
     * @throws SecurityException On permission failure.
     * @see #start()
     */
    Task<Void> get()
            throws SecurityException {
        if (started) {
            return fusedLocation.requestLocationUpdates(
                    locationRequestNow.setExpirationDuration(interval), locationCallback, null);
        }
        return Tasks.forResult(null);
    }

    /**
     * Refresh the last known locations, i.e., flushes any locations currently
     * being batched. NOTICE: Geopositioning must be in started mode.
     *
     * @see FusedLocationProviderClient#flushLocations()
     * @see #start()
     */
    void refresh() {
        if (started) {
            fusedLocation.flushLocations()
                    .addOnCompleteListener(t -> {
                        Log.d(TAG, "refresh (flushLocations()) COMPLETED! Success: " + t.isSuccessful());
                        storeLast();
                    });
        }
    }

    /**
     * Get the last location update.
     *
     * @return the location last stored, null if not yet updated.
     */
    @Nullable
    Location getLastStored() {
        return lastLocation;
    }

    private void storeLast()
            throws SecurityException {
        fusedLocation.getLastLocation()
                .addOnSuccessListener(l -> {
                    Log.d(TAG, "storeLast onSuccess!");
                    if (l != null) {
                        Log.d(TAG, "storeLast Location: " + l.toString());
                        executorService.execute(() -> {
                            List<Location> ll = new ArrayList<>(1);
                            ll.add(l);
                            locationProcess(LocationResult.create(ll));
                        });
                    }
                })
                .addOnFailureListener(e -> Log.e(TAG, "storeLast onFailure!", e));
    }

    private void createLocationRequestNow() {
        locationRequestNow = LocationRequest.create()
                .setNumUpdates(1)
                .setInterval(0)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        Log.d(TAG, "createLocationRequestNow: interval [" + locationRequestNow.getInterval()
                + "]ms expiration [" + (locationRequestNow.getExpirationTime() - SystemClock.elapsedRealtime())
                + "]ms updates [" + locationRequestNow.getNumUpdates()
                + "] priority [" + locationRequestNow.getPriority() + "]");
    }

    private void createLocationRequestBalanced() {
        locationRequestBalanced = LocationRequest.create()
                .setInterval(interval)
                .setFastestInterval(intervalFastest)
                .setMaxWaitTime(3 * interval)
                .setSmallestDisplacement(displacementThreshold)
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        Log.d(TAG, "createLocationRequestBalanced: interval [" + locationRequestBalanced.getInterval()
                + "]ms fastest [" + locationRequestBalanced.getFastestInterval()
                + "]ms displacement [" + locationRequestBalanced.getSmallestDisplacement()
                + "]m wait [" + locationRequestBalanced.getMaxWaitTime()
                + "]ms priority [" + locationRequestBalanced.getPriority() + "]");
    }

    private void buildLocationSettingsRequest() {
        locationSettingsRequest = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequestNow)
                .addLocationRequest(locationRequestBalanced)
                .setAlwaysShow(true)
                .build();
    }

    private LocationCallback locationCallback() {
        return new LocationCallback() {
            @Override
            public void onLocationResult(final LocationResult lr) {
                super.onLocationResult(lr);
                Log.d(TAG, "onLocationResult: " + lr);
                if (lr != null) {
                    executorService.execute(() -> locationProcess(lr));
                }
            }
            @Override
            public void onLocationAvailability(LocationAvailability la) {
                super.onLocationAvailability(la);
                Log.d(TAG, "onLocationAvailability: " + la.toString());
            }
        };
    }

    private void locationProcess(@NonNull LocationResult lr) {
        final List<Collected> lc = new ArrayList<>();
        final String tz = DateTime.getTimeZoneISO8601();
        final long currentTime = System.currentTimeMillis();
        long lastEt = 0;
        for (final Location l : lr.getLocations()) {
            final Timestamp ts = new Timestamp(l.getTime());
            final long et = l.getElapsedRealtimeNanos();
            if (ts.after(new Timestamp(currentTime - DateTime.HOUR_MS)) &&  // Geo data within the hour
                    (lastLocation == null ||
                            (currentTime - lastCollectedDataTime > Sensei.SENSOR_DATA_MISSING_ELAPSED_MILLIS_MAX) ||
                            (et - lastEt > ET_INTERVAL_RESTRICTION && displacementOfInterest(l)))) {
                lc.add(new Collected(ts, tz,
                        SENSOR_ID, "latitude", l.getLatitude(), null, false));
                lc.add(new Collected(ts, tz,
                        SENSOR_ID, "longitude", l.getLongitude(), null, false));
                if (l.hasAccuracy()) {
                    lc.add(new Collected(ts, tz,
                            SENSOR_ID, "h_accuracy", l.getAccuracy(), null, false));
                }
                if (l.hasAltitude()) {
                    lc.add(new Collected(ts, tz,
                            SENSOR_ID, "altitude", l.getAltitude(), null, false));
                }
                if (l.hasBearing()) {
                    lc.add(new Collected(ts, tz,
                            SENSOR_ID, "bearing", l.getBearing(), null, false));
                }
                if (l.hasSpeed()) {
                    lc.add(new Collected(ts, tz,
                            SENSOR_ID, "speed", l.getSpeed(), null, false));
                }
                if (l.getProvider() != null) {
                    lc.add(new Collected(ts, tz,
                            SENSOR_ID, "provider", 0.0, l.getProvider(), false));
                }
                lastCollectedDataTime = currentTime;
                lastLocation = l;
                lastEt = et;
            }
        }
        if (lc.size() >= 2) {
            try {
                dataCacheDao.set(lc.toArray(new Collected[0]));
                Log.d(TAG, "GEOPOSITIONING SET: " + lc);
            } catch (Exception e) {
                Log.e(TAG, "GEOPOSITIONING DATA DISCARDED! ", e);
            }
        } else {
            Log.d(TAG, "GEOPOSITIONING DATA DISCARDED!");
        }
    }

    /*
     * The displacement is of interest if meets at least one of the following:
     * 1. distance < accuracy in meters
     * OR 2. distance > displacement threshold
     * OR 3. vertical displacement > displacement threshold
     */
    private boolean displacementOfInterest(Location location) {
        float distance = location.distanceTo(lastLocation);
        return (location.hasAccuracy() && distance < location.getAccuracy()) ||
                distance > displacementThreshold ||
                (location.hasAltitude()
                        && lastLocation.hasAltitude()
                        && Math.abs(location.getAltitude() - lastLocation.getAltitude()) > displacementThreshold);
    }
}
