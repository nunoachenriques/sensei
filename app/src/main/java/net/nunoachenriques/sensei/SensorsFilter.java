/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * The device hardware sensors by Android feed filter.
 * Lists the sensors of interest for listening and filtering.
 * Uses the application shared preferences.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see Sensors
 * @see SharedPreferences
 * @see "res/xml/preferences.xml"
 */
public final class SensorsFilter {

    private static final String TAG = "SensorsFilter";
    /** If no threshold preference is set then all values are of interest, i.e., > {@link Float#MIN_VALUE} */
    private static final float ERROR_FLOAT = Float.MIN_VALUE;
    static final int ERROR_INT = Integer.MIN_VALUE;

    private static volatile SensorsFilter INSTANCE;

    private final SharedPreferences sharedPreferences;
    private final Set<Integer> sensorsOfInterest;

    private SensorsFilter(Context c) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        sensorsOfInterest = new HashSet<>();
        Log.d(TAG, "*************** getAll: " + sharedPreferences.getAll().toString());
        for (String k : sharedPreferences.getAll().keySet()) {
            // Gets the Sensor type id and collects it as unique.
            String[] s = k.split("\\.", 2);
            Log.d(TAG, "*************** k.split: " + Arrays.toString(s));
            // Only of interest if an Android hardware sensor with an id number!
            if (s.length > 1 && !s[0].equals("")) {
                try {
                    sensorsOfInterest.add(Integer.valueOf(s[0]));
                } catch (Exception e) {
                    Log.d(TAG, "sensorsOfInterest ADD [" + s[0] + "] FAILED! " + e.getMessage());
                }
            }
        }
    }

    /**
     * Gets an instance of this class. If no single instance is available, i.e.,
     * {@code null}, then creates and returns a single new one
     * (Singleton pattern).
     *
     * @param c Android application {@link Context}.
     * @return The single instance of this class.
     */
    public static SensorsFilter getInstance(Context c) {
        if (INSTANCE == null) {
            synchronized (SensorsFilter.class) {
                if (INSTANCE == null) {
                    INSTANCE = new SensorsFilter(c);
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Gets the count of all the sensors of interest declared and defined in the
     * file {@code res/xml/preferences_sensors.xml}.
     *
     * @return The sensors of interest count.
     */
    int getSensorsCount() {
        return sensorsOfInterest.size();
    }

    /**
     * Gets all the sensors of interest declared and defined in the file
     * {@code res/xml/preferences_sensors.xml}.
     *
     * @return A set of integers with the sensors of interest id.
     * @see android.hardware.Sensor
     */
    Iterable<Integer> getSensorsOfInterest() {
        return sensorsOfInterest;
    }

    /**
     * Gets enabled state value.
     *
     * @param sensorType The sensor integer type.
     * @return Enabled state: {@code true} or {@code false}.
     */
    boolean isEnabled(int sensorType) {
        return sharedPreferences.getBoolean(String.valueOf(sensorType).concat(".enabled"), false);
    }

    /**
     * Gets rate value.
     *
     * @param sensorType The sensor integer type.
     * @return Rate integer value.
     */
    int getRate(int sensorType) {
        String k = String.valueOf(sensorType).concat(".rate");
        String v = sharedPreferences.getString(k, null);
        return (v != null ? Integer.parseInt(v) : ERROR_INT);
    }

    /**
     * Gets latency value.
     *
     * @param sensorType The sensor integer type.
     * @return Latency integer value (microseconds).
     */
    int getLatency(int sensorType) {
        String k = String.valueOf(sensorType).concat(".latency");
        String v = sharedPreferences.getString(k, null);
        return (v != null ? Integer.parseInt(v) : ERROR_INT);
    }

    /**
     * Gets the number of values of interest, i.e., 1, 2 or 3.
     *
     * @param sensorType The sensor integer type.
     * @return Number of values of interest.
     */
    int getValuesCount(int sensorType) {
        String k = String.valueOf(sensorType).concat(".values_count");
        String v = sharedPreferences.getString(k, null);
        return (v != null ? Integer.parseInt(v) : ERROR_INT);
    }

    /**
     * Checks if the absolute difference between {@code current} and
     * {@code last} values is greater than or equal to the differential
     * threshold preference specified by the {@code sensorType} and sensor
     * specific value by {@code index}.
     *
     * @param sensorType The sensor integer type.
     * @param index The value index: 0, 1 ... n.
     * @param current The current value.
     * @param last The last (previous) value.
     * @return True if {@code |current - last| >= the differential threshold}, false otherwise.
     * @see #getDiffThreshold(int, int)
     */
    boolean isGTEDiffThreshold(int sensorType, int index, float current, float last) {
        return Math.abs(current - last) >= getDiffThreshold(sensorType, index);
    }

    /**
     * Gets the differential threshold, below it there's no interest in the
     * value, for each one of the values specified by index.
     *
     * @param sensorType The sensor integer type.
     * @param index The value index: 0, 1 or 2.
     * @return The differential threshold.
     */
    private float getDiffThreshold(int sensorType, int index) {
        String k = getSensorValueKey(sensorType, index).concat(".diff_threshold");
        String v = sharedPreferences.getString(k, null);
        return (v != null ? Float.parseFloat(v) : ERROR_FLOAT);
    }

    /**
     * Gets the sensor value key prefix (e.g., "8.0" is Sensor.TYPE_PROXIMITY
     * and values[0]).
     *
     * @param sensorType The sensor integer type.
     * @param index The value index: 0, 1 or 2.
     * @return The sensor value key prefix.
     */
    String getSensorValueKey(int sensorType, int index) {
        return String.valueOf(sensorType).concat("." + index);
    }
}
