/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.Nullable;

import net.nunoachenriques.sensei.persistence.Collected;
import net.nunoachenriques.sensei.persistence.DataCacheDaoService;
import net.nunoachenriques.sensei.utility.Connectivity;
import net.nunoachenriques.sensei.utility.DateTime;
import net.nunoachenriques.sensei.utility.Notifications;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * The app feed {@link Service} which manages all about it: configuration,
 * starting and stopping the feed processing. Encompass all sensors: regular
 * and trigger one-shot, broadcast, location, human activity detection.
 * Includes listeners for text feeds (e.g., Twitter). Moreover, manages
 * empathy value and decay.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see ActivityFeed
 * @see BatteryReceiver
 * @see ConnectivityReceiver
 * @see Empathy
 * @see EmpathyNotifyService
 * @see Geopositioning
 * @see Sensors
 * @see TwitterFeed
 */
public final class FeedService
        extends Service
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String SENSOR_ID = "app.agent.feed";
    public static final String SENSOR_ATTR_STATUS = "status";
    public static final int STATUS_FEED = 1;
    public static final int STATUS_PAUSE = 2;
    public static final int STATUS_DESTROY = 3;

    private static final Map<Integer, String> STATUS_NAME =
            new HashMap<Integer, String>(3) {{
                put(STATUS_FEED, "FEED");
                put(STATUS_PAUSE, "PAUSE");
                put(STATUS_DESTROY, "DESTROY");
            }};
    private static final String TAG = "FeedService";
    private static final int SET_FEED_ALL = 1;
    private static final int UNSET_FEED_ALL = 0;
    private static final int RESET_FEED_NETWORKING = 401;
    private static final int RESET_FEED_TWITTER = 301;
    private static final long EMPATHY_SCHEDULER_UPDATE = TimeUnit.MINUTES.toMillis(1);
    private static final double EMPATHY_COLLECTED_THRESHOLD = 1.0;  // % [0, 100]
    private static final long WIFI_SCAN_SCHEDULER_UPDATE = TimeUnit.MINUTES.toMillis(5);

    private static volatile boolean onSensorsFeed;
    private static volatile boolean onPause;
    private static volatile boolean wifiRequired;

    private static double empathyLastValue;
    private static List<ScanResult> lastWifiScan;
    private static Resources res;

    private ActivityFeed activityFeed;
    private BatteryReceiver battery;
    private static final IntentFilter batteryIFilter =
            new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
    static {
        batteryIFilter.addAction(Intent.ACTION_BATTERY_LOW);
        batteryIFilter.addAction(Intent.ACTION_BATTERY_OKAY);
    }
    private ConnectivityReceiver connectivity;
    private static final IntentFilter connectivityIFilter =
            new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
    static {
        connectivityIFilter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
    }
    private ScreenReceiver screen;
    private Empathy empathy;
    private Geopositioning geopositioning;
    private Sensors sensors;
    private TwitterFeed twitter;
    private EmpathyScheduler empathyScheduler;
    private SensorsScheduler sensorsScheduler;
    private long sensorsSchedulerActiveDelay;
    private long sensorsSchedulerInactiveDelay;
    private WifiScanScheduler wifiScanScheduler;
    private Handler handlerEmpathyScheduler;
    private Handler handlerSensorsScheduler;
    private Handler handlerWifiScanScheduler;
    @SuppressWarnings("FieldCanBeLocal")
    private Looper serviceLooper;
    private ServiceHandler serviceHandler;
    private SharedPreferences sharedPreferences;

    private final class ServiceHandler
            extends Handler {
        ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message m) {
            switch (m.what) {
                case FeedService.SET_FEED_ALL:
                    Log.d(TAG, "FeedService.SET_FEED_ALL");
                    setFeedTwitter();
                    setFeedActivity();
                    setFeedGeopositioning();
//                    handlerSensorsScheduler.post(sensorsScheduler);
//                    handlerWifiScanScheduler.post(wifiScanScheduler);
                    break;
                case FeedService.UNSET_FEED_ALL:
                    Log.d(TAG, "FeedService.UNSET_FEED_ALL");
                    unsetFeedTwitter();
                    unsetFeedActivity();
                    unsetFeedGeopositioning();
//                    handlerSensorsScheduler.removeCallbacks(sensorsScheduler);
//                    unsetFeedSensors();
//                    handlerWifiScanScheduler.removeCallbacks(wifiScanScheduler);
                    break;
                case FeedService.RESET_FEED_NETWORKING:
                    Log.d(TAG, "FeedService.RESET_FEED_NETWORKING");
                    resetFeedTwitter();
                    break;
                case FeedService.RESET_FEED_TWITTER:
                    Log.d(TAG, "FeedService.RESET_FEED_TWITTER");
                    resetFeedTwitter();
                    break;
                default:
                    Log.e(TAG, "Message type UNKNOWN: " + m.what);
                    break;
            }
        }
    }

    @Override
    public void onCreate() {
        Context c = getApplicationContext();
        res = getResources();
        // SERVICE MESSAGES
        HandlerThread thread = new HandlerThread(TAG, Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        serviceLooper = thread.getLooper();
        serviceHandler = new ServiceHandler(serviceLooper);
        // EMPATHY
        empathyLastValue = 0.0;
        empathy = Empathy.getInstance(c);
        empathyScheduler = new EmpathyScheduler();
        handlerEmpathyScheduler = new Handler();
        handlerEmpathyScheduler.postDelayed(empathyScheduler, EMPATHY_SCHEDULER_UPDATE);
        // SHARED PREFERENCES
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public int onStartCommand(Intent i, int flags, int sid) {
        Context c = getApplicationContext();
        // NOTIFICATION: EMPATHY VALUE
        // NOTICE: KEEP THIS 2-STEP CODE AS IS!
        //         Started service visible notification required for background.
        startForeground(EmpathyNotifyService.NOTIFICATION_ID,
                Notifications
                        .getDefault(c, true, false)
                        .setSmallIcon(R.drawable.ic_notification)
                        .build()); // To avoid ANR!
        updateEmpathyNotification();
        // SENSORS SCHEDULER + HANDLER THREAD
        handlerSensorsScheduler = new Handler();
        String sensorsActiveSp = sharedPreferences.getString("feed.sensors.active", null);
        sensorsSchedulerActiveDelay = (sensorsActiveSp != null) ? Long.parseLong(sensorsActiveSp) : 2000;
        String sensorsInactiveSp = sharedPreferences.getString("feed.sensors.inactive", null);
        sensorsSchedulerInactiveDelay = (sensorsInactiveSp != null) ? Long.parseLong(sensorsInactiveSp) : 8000;
        onSensorsFeed = false;
        onPause = false;
        wifiRequired = sharedPreferences.getBoolean(res.getString(R.string.data_wifi_pref_key), true);
        // WiFi SCHEDULER
        wifiScanScheduler = new WifiScanScheduler();
        handlerWifiScanScheduler = new Handler();
        lastWifiScan = null;
        // FEED
        activityFeed = ActivityFeed.getInstance(c);
        battery = new BatteryReceiver();
        connectivity = new ConnectivityReceiver();
        screen = new ScreenReceiver();
        IntentFilter screenIFilter = new IntentFilter();
        screenIFilter.addAction(Intent.ACTION_SCREEN_ON);
        screenIFilter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(screen, screenIFilter);
        geopositioning = Geopositioning.getInstance(c);
        sensors = Sensors.getInstance(c);
        sensorsScheduler = new SensorsScheduler();
        try {
            twitter = TwitterFeed.getInstance(c);
        } catch (Exception e) {
            twitter = null;
            Log.e(TAG, "TwitterFeed FAILED!", e);
        }
        onSharedPreferenceChanged(sharedPreferences, res.getString(R.string.data_pause_mode_pref_key));
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Log.w(TAG, "SERVICE INTERRUPTING...");
        try {
            updateFeedStatus(STATUS_DESTROY);
            TimeUnit.MILLISECONDS.sleep(200);
        } catch (IllegalStateException e) {
            Log.e(TAG, "onDestroy updateFeedStatus sleep 200 ms ILLEGAL STATE!" + e.getMessage());
        } catch (InterruptedException e) {
            Log.e(TAG, "onDestroy updateFeedStatus sleep 200 ms INTERRUPTED!" + e.getMessage());
        }
        try {
            unregisterReceiver(screen);
        } catch (Exception e) {
            Log.e(TAG, "Unregister SCREEN FAILED!" + e.getMessage());
        }
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
        serviceHandler.sendEmptyMessage(UNSET_FEED_ALL);
        handlerEmpathyScheduler.removeCallbacks(empathyScheduler);
        // Last chance (wait 200 ms) for all sensors to gracefully stop
        if (onSensorsFeed) {
            try {
                TimeUnit.MILLISECONDS.sleep(200);
            } catch (InterruptedException e) {
                Log.e(TAG, "onDestroy onSensorsFeed sleep 200 ms INTERRUPTED!" + e.getMessage());
            }
        }
        serviceHandler.removeCallbacksAndMessages(null);
        stopForeground(true);
        Log.w(TAG, "SERVICE INTERRUPTED!");
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        if (key.equals(res.getString(R.string.data_pause_mode_pref_key))) {
            onPause = sp.getBoolean(key, false);
            updateFeedStatus(onPause ? STATUS_PAUSE : STATUS_FEED);
            if (onPause) {
                empathy.setDecayFactor(Empathy.DECAY_FACTOR_ON_PAUSE);
                serviceHandler.sendEmptyMessage(UNSET_FEED_ALL);
            } else {
                empathy.setDecayFactor(Empathy.DECAY_FACTOR_DEFAULT);
                serviceHandler.sendEmptyMessage(SET_FEED_ALL);
            }
            updateEmpathyNotification();
        } else if (key.equals(res.getString(R.string.data_wifi_pref_key))) {
            wifiRequired = sp.getBoolean(key, true);
            serviceHandler.sendEmptyMessage(RESET_FEED_NETWORKING);
        } else if (key.equals(res.getString(R.string.source_twitter_screen_name_pref_key))) {
            serviceHandler.sendEmptyMessage(RESET_FEED_TWITTER);
        } else if (key.equals(Empathy.EMPATHY_PREF_KEY)) {
            // Empathy value changed by other means (e.g., smile report) than regular decay
            double e = empathy.get();
            if (e == Empathy.MIN_DEFAULT
                    || e == Empathy.MAX_DEFAULT
                    || e == Empathy.START_DEFAULT
                    || Math.abs(e - empathyLastValue) > EMPATHY_COLLECTED_THRESHOLD) {
                startService(new Intent(this, DataCacheDaoService.class)
                        .putExtra(DataCacheDaoService.EXTRA_COLLECTED_SET,
                                new Collected[]{new Collected(
                                        new Timestamp(System.currentTimeMillis()), DateTime.getTimeZoneISO8601(),
                                        Empathy.SENSOR_ID, Empathy.SENSOR_ATTR_EMPATHY, e, null,
                                        false)}));
                empathyLastValue = e;
            }
        }
    }

    /*
     *                           U P D A T E S
     */

    private void updateEmpathyNotification() {
        startService(new Intent(getApplicationContext(), EmpathyNotifyService.class)
                .putExtra(EmpathyNotifyService.EXTRA_VALUE_MAX, (int)Empathy.MAX_DEFAULT)
                .putExtra(EmpathyNotifyService.EXTRA_VALUE, empathy.getRound()));
    }

    private void updateFeedStatus(int s) {
        startService(new Intent(getApplicationContext(), DataCacheDaoService.class)
                .putExtra(DataCacheDaoService.EXTRA_COLLECTED_SET,
                        new Collected[]{new Collected(
                                new Timestamp(System.currentTimeMillis()), DateTime.getTimeZoneISO8601(),
                                SENSOR_ID, SENSOR_ATTR_STATUS, s, STATUS_NAME.get(s),
                                false)}));
    }

    /*
     *                        S C H E D U L E R S
     */

    private class EmpathyScheduler
            implements Runnable {
        @Override
        public void run() {
            empathy.setAfterDecay();
            updateEmpathyNotification();
            handlerEmpathyScheduler.postDelayed(this, EMPATHY_SCHEDULER_UPDATE);
        }
    }

    private class SensorsScheduler
            implements Runnable {
        @Override
        public void run() {
            if (onSensorsFeed) {
                unsetFeedSensors();
                handlerSensorsScheduler.postDelayed(this, sensorsSchedulerInactiveDelay);
            } else if (!onPause) {
                setFeedSensors();
                handlerSensorsScheduler.postDelayed(this, sensorsSchedulerActiveDelay);
            }
        }
    }

    private class WifiScanScheduler
            implements Runnable {
        private static final String WIFI_SCAN_SENSOR_ID = "app.sensor.wifiscan";
        private static final String WIFI_SCAN_SENSOR_ATTR_RSSI = "rssi";
        private static final int WIFI_SCAN_AP_MAX = 10;
        private static final int WIFI_SCAN_RSSI_THRESHOLD = 10;  // https://en.wikipedia.org/wiki/DBm
        @Override
        public void run() {
            WifiManager wm = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            if (wm != null) {
                List<ScanResult> srl = wm.getScanResults();
                // ORDER by level DESC
                Collections.sort(srl, (o1, o2) -> Integer.compare(o2.level, o1.level));
                if (srl.size() > WIFI_SCAN_AP_MAX) {  // TRUNCATE list to a maximum
                    srl.subList(WIFI_SCAN_AP_MAX, srl.size()).clear();
                }
                Log.d(TAG, "WifiScan (sort + truncate): " + srl);
                if (lastWifiScan == null
                        || !equal(srl, lastWifiScan)) {  // NOT EQUALS then PROCESS and STORE
                    List<Collected> lc = new ArrayList<>();
                    String tz = DateTime.getTimeZoneISO8601();
                    if (srl.isEmpty()) {
                        lc.add(new Collected(new Timestamp(System.currentTimeMillis()), tz,
                                WIFI_SCAN_SENSOR_ID, WIFI_SCAN_SENSOR_ATTR_RSSI, 0.0, null, false));
                    } else {
                        // Restrict Wifi within the hour
                        Timestamp ts = DateTime.bootMicrosToTimestamp(srl.get(0).timestamp);
                        if (ts.after(new Timestamp(System.currentTimeMillis() - DateTime.HOUR_MS))) {
                            for (ScanResult sr : srl) {
                                lc.add(new Collected(ts, tz,
                                        WIFI_SCAN_SENSOR_ID, WIFI_SCAN_SENSOR_ATTR_RSSI, sr.level, sr.BSSID, false));
                            }
                        }
                    }
                    if (lc.size() > 0) {
                        try {
                            startService(new Intent(getApplicationContext(), DataCacheDaoService.class)
                                    .putExtra(DataCacheDaoService.EXTRA_COLLECTED_SET,
                                            lc.toArray(new Collected[0])));
                        } catch (IllegalStateException e) {
                            Log.e(TAG, e.toString());
                        }
                    } else {
                        Log.d(TAG, "WIFI SCAN DATA DISCARDED!");
                    }
                    lastWifiScan = srl;
                }
            }
            handlerWifiScanScheduler.postDelayed(this, WIFI_SCAN_SCHEDULER_UPDATE);
        }
        private boolean equal(List<ScanResult> l1, List<ScanResult> l2) {
            if (l1.size() != l2.size()) {
                return false;
            }
            for (int i1 = 0; i1 < l1.size(); i1++) {
                boolean contains = false;
                for (int i2 = 0; i2 < l2.size(); i2++) {
                    if (l1.get(i1).BSSID.equals(l2.get(i2).BSSID)
                            && Math.abs(l1.get(i1).level - l2.get(i2).level) < WIFI_SCAN_RSSI_THRESHOLD) {
                        contains = true;
                    }
                }
                if (!contains) {
                    return false;
                }
            }
            return true;
        }
    }

    /*
     *                             F E E D S
     */

    private void setFeedActivity() {
        activityFeed.start()
                .addOnSuccessListener(v -> Log.i(TAG, "ACTIVITY recognition START successful!"))
                .addOnFailureListener(e -> Log.e(TAG, "ACTIVITY recognition START FAILED!", e));
    }

    private void unsetFeedActivity() {
        activityFeed.shutdown()
                .addOnSuccessListener(v -> Log.i(TAG, "ACTIVITY recognition SHUTDOWN successful!"))
                .addOnFailureListener(e -> Log.e(TAG, "ACTIVITY recognition SHUTDOWN FAILED!", e));
    }

    private void setFeedGeopositioning() {
        geopositioning.start()
                .addOnSuccessListener(v -> Log.i(TAG, "GEOPOSITIONING START successful!"))
                .addOnFailureListener(e -> Log.e(TAG, "GEOPOSITIONING START FAILED!", e));
    }

    private void unsetFeedGeopositioning() {
        geopositioning.shutdown()
                .addOnSuccessListener(v -> Log.i(TAG, "GEOPOSITIONING SHUTDOWN successful!"))
                .addOnFailureListener(e -> Log.e(TAG, "GEOPOSITIONING SHUTDOWN FAILED!", e));
    }

    private void setFeedSensors() {
        Log.d(TAG, "setFeedSensors()");
        if (!onSensorsFeed) {
            onSensorsFeed = true;
            sensors.setAllSensors();
            registerReceiver(battery, batteryIFilter);
            registerReceiver(connectivity, connectivityIFilter);
        }
    }

    private void unsetFeedSensors() {
        Log.d(TAG, "unsetFeedSensors()");
        if (onSensorsFeed) {
            sensors.unsetAllSensors();
            try {
                unregisterReceiver(battery);
            } catch (Exception e) {
                Log.e(TAG, "Unregister BATTERY FAILED!" + e.getMessage());
            }
            try {
                unregisterReceiver(connectivity);
            } catch (Exception e) {
                Log.e(TAG, "Unregister CONNECTIVITY FAILED!" + e.getMessage());
            }
            onSensorsFeed = false;
        }
    }

    private void resetFeedTwitter() {
        unsetFeedTwitter();
        setFeedTwitter();
    }

    private void setFeedTwitter() {
        if (twitter == null) {
            Log.w(TAG, "Twitter START FAILED! Hint: twitter instance failed!");
        } else if (onPause) {
            Log.w(TAG, "Twitter START FAILED! Hint: onPause!");
        } else if (!Connectivity.isConnectionOk(getApplicationContext(), wifiRequired)) {
            Log.w(TAG, "Twitter START FAILED! Hint: WiFi required and OFF!");
        } else if (!twitter.start()) {
            Log.w(TAG, "Twitter START FAILED! Hint: Check authorization!");
        }
    }

    private void unsetFeedTwitter() {
        if (twitter != null) {
            twitter.shutdown();
        }
    }
}
