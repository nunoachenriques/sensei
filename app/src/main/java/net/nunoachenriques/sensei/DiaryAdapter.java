/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.DetectedActivity;

import net.nunoachenriques.sensei.emotion.Polarity;
import net.nunoachenriques.sensei.persistence.Chat;
import net.nunoachenriques.sensei.utility.DateTime;

import java.util.Collections;
import java.util.List;

/**
 * The adapter to deal with the several messages to fill the Diary view.
 * It uses the ViewHolder design pattern to improve performance by holding
 * previously inflated views.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see "res/layout/diary.xml"
 * @see DiaryActivity
 */
final class DiaryAdapter
        extends RecyclerView.Adapter {

    private static final int MESSAGE_AGENT_VIEW = Chat.AGENT;
    private static final int MESSAGE_HUMAN_VIEW = Chat.HUMAN;

    private final Context ctx;
    private final Resources res;
    private final LifecycleOwner lifecycleOwner;
    private final RecyclerView diaryView;
    private final DiaryViewModel diaryViewModel;
    private List<Chat> data;

    DiaryAdapter(Context c, LifecycleOwner lo, RecyclerView dv, DiaryViewModel dvm) {
        ctx = c;
        res = c.getResources();
        data = Collections.emptyList();
        lifecycleOwner = lo;
        diaryView = dv;
        diaryViewModel = dvm;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        Chat message = data.get(position);
        if (message.entity == Chat.AGENT) {
            return MESSAGE_AGENT_VIEW;
        } else {
            return MESSAGE_HUMAN_VIEW;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == MESSAGE_AGENT_VIEW) {
            view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.diary_unit_agent, parent, false);
            return new MessageAgentViewHolder(view);
        } else {
            view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.diary_unit_human, parent, false);
            return new MessageHumanViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vh, int position) {
        Chat message = data.get(position);
        switch (vh.getItemViewType()) {
            case MESSAGE_AGENT_VIEW:
                ((MessageAgentViewHolder)vh).bind(message);
                break;
            case MESSAGE_HUMAN_VIEW:
            default:
                ((MessageHumanViewHolder)vh).bind(message);
                break;
        }
    }

    /*
     * AGENT
     */
    private class MessageAgentViewHolder
            extends RecyclerView.ViewHolder {

        final View view;
        final TextView textView;
        final TextView timestampView;
        final ImageView borderView;
        final ImageView polarityView;
        final ImageView activityView;

        MessageAgentViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            textView = itemView.findViewById(R.id.diary_unit_text_view);
            timestampView = itemView.findViewById(R.id.diary_unit_timestamp_view);
            borderView = itemView.findViewById(R.id.diary_unit_border_view);
            polarityView = itemView.findViewById(R.id.diary_unit_polarity_view);
            activityView = itemView.findViewById(R.id.diary_unit_activity_view);
        }

        void bind(Chat message) {
            Resources.Theme theme = ctx.getTheme();
            StringBuilder newText = new StringBuilder();
            timestampView.setText(DateTime.format(DateTime.DATE_TIME_PATTERN, message.moment.getTime()));
            // Border NO POLARITY!
            borderView.setImageTintList(ColorStateList.valueOf(
                    res.getColor(android.R.color.transparent, theme)));
            borderView.setContentDescription(res.getString(R.string.empty));
            // POLARITY + ACTIVITY
            if (message.polarity == Polarity.NO_POLARITY) {
                // NO EMOTIONAL VALENCE
                polarityView.setVisibility(View.GONE);
                activityView.setVisibility(View.GONE);
                newText.append(message.text);
            } else {
                // EMOTIONAL VALENCE Report
                int[] r = Polarity.polarityToResources(message.polarity);
                final Drawable smile = res.getDrawable(r[2], theme);
                smile.setTint(res.getColor(r[0], theme));
                polarityView.setImageDrawable(smile);
                polarityView.setContentDescription(res.getText(r[1]));
                // ACTIVITY Report
                String[] messageSplit = message.text.split("\\|");
                newText.append(messageSplit[0]);
                int activity = Integer.parseInt(messageSplit[1]);
                if (activity == DetectedActivity.UNKNOWN) {
                    activityView.setVisibility(View.GONE);
                    newText.append(res.getString(R.string.dashboard_you_activity_unknown));
                } else {
                    r = ActivityService.activityToResources(activity);
                    activityView.setContentDescription(res.getText(r[0]));
                    activityView.setImageResource(r[1]);
                    newText.append(res.getString(r[0]));
                }
            }
            textView.setText(newText.toString());
            // On CLICK
            view.setOnClickListener(v -> Toast.makeText(ctx, res.getText(R.string.diary_unit_agent_description), Toast.LENGTH_SHORT).show());
        }
    }

    /*
     * HUMAN
     */
    private class MessageHumanViewHolder
            extends RecyclerView.ViewHolder {

        final View view;
        final TextView textView;
        final TextView timestampView;
        final ImageView borderView;

        MessageHumanViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            textView = itemView.findViewById(R.id.diary_unit_text_view);
            timestampView = itemView.findViewById(R.id.diary_unit_timestamp_view);
            borderView = itemView.findViewById(R.id.diary_unit_border_view);
        }

        void bind(Chat message) {
            textView.setText(message.text);
            timestampView.setText(DateTime.format(DateTime.DATE_TIME_PATTERN, message.moment.getTime()));
            final int[] r = Polarity.polarityToResources(message.polarity);
            borderView.setImageTintList(ColorStateList.valueOf(res.getColor(r[0], ctx.getTheme())));
            borderView.setContentDescription(res.getString(r[1]));
            CharSequence description = borderView.getContentDescription();
            if (description.equals("")) {
                view.setContentDescription(
                        res.getString(R.string.diary_unit_human_no_description));
            } else {
                view.setContentDescription(
                        res.getString(R.string.diary_unit_human_description, description, message.polarity));
            }
            view.setOnClickListener(v -> Toast.makeText(ctx, v.getContentDescription(), Toast.LENGTH_SHORT).show());
        }
    }

    /**
     * Changes the data filter on retrieving diary messages.
     *
     * @param searchToken Search query token to filter results.
     * @return This object in order to be able to chain calls.
     */
    DiaryAdapter resetDataObserver(@Nullable String searchToken) {
        diaryViewModel.getDiaryMessage(searchToken).observe(lifecycleOwner, lc -> {
            if (lc != null) {
                int diff = lc.size() - data.size();
                data = lc;
                if (diff == 1) {  // one (new) entry (typical case)
                    int position = data.size() - 1;
                    notifyItemInserted(position);
                    diaryView.scrollToPosition(position);
                } else {  // zero or >1 (new) entries
                    notifyDataSetChanged();  // notify all!
                }
            }
        });
        return this;
    }
}
