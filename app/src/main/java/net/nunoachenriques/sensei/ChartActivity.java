/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.ScatterData;
import com.github.mikephil.charting.data.ScatterDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import net.nunoachenriques.sensei.chart.EntryData;
import net.nunoachenriques.sensei.chart.TimestampXAxisValueFormatter;
import net.nunoachenriques.sensei.chart.ValueTimestampMarkerView;
import net.nunoachenriques.sensei.emotion.Polarity;
import net.nunoachenriques.sensei.persistence.Collected;
import net.nunoachenriques.sensei.persistence.DataCache;
import net.nunoachenriques.sensei.persistence.DataCacheDao;
import net.nunoachenriques.sensei.utility.AlertDialogFragment;
import net.nunoachenriques.sensei.utility.Calc;
import net.nunoachenriques.sensei.utility.DateTime;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * The chart showing smiles, diary and social messages sentiment analysis
 * emotional valence (polarity) all combined.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see "res/layout/chart.xml"
 * @see <a href="https://github.com/PhilJay/MPAndroidChart">MPAndroidChart</a>
 */
public final class ChartActivity
        extends SenseiAppCompatActivity
        implements OnChartValueSelectedListener {

    public static final String SENSOR_ID = "app.human.chart";

    private static final String TAG = "ChartActivity";
    private static final long VISIBLE_X_RANGE = DateTime.DAY_S * 2;
    private static final ExecutorService executorService = Executors.newSingleThreadExecutor();

    private Resources res;
    private DataCacheDao dataCacheDao;
    private FragmentManager fragmentManager;
    private Resources.Theme theme;
    private @DrawableRes int adfIcon;
    private @StringRes int adfTitle;
    private String adfMessage;
    private Transition transition;
    private CombinedChart polarityChart;
    private boolean scaleHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        res = getResources();
        dataCacheDao = DataCache.getInstance(this).getDao();
        fragmentManager = getSupportFragmentManager();
        theme = getTheme();
        setContentView(R.layout.chart);
        setTitle(R.string.chart_title);
        setSupportActionBar();
        transition = new Transition(this, SENSOR_ID);
        scaleHelper = true;
        if (setupChartView()) {
            setupChartDataObserver();
        } else {
            AlertDialogFragment.newInstance(
                    R.drawable.ic_question_mark,
                    R.string.failure,
                    res.getString(R.string.chart_failure),
                    R.string.dialog_ok,
                    (dialog, whichButton) -> {
                        dialog.dismiss();
                        finishAndRemoveTask();
                    }
            ).show(fragmentManager, TAG);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        transition.setEnter();
    }

    @Override
    protected void onPause() {
        super.onPause();
        transition.setExit();
    }

    @Override
    public void onValueSelected(final Entry e, Highlight h) {
        // Log.d(TAG, "OnChartValueSelectedListener onValueSelected()");
        executorService.execute(() -> {
            Object o = e.getData();
            if (o != null) {
                EntryData ed = (EntryData)o;
                switch (ed.type) {
                    case DiaryReportService.SENSOR_ID:
                        adfIcon = R.drawable.ic_message;
                        adfTitle = R.string.diary_title;
                        adfMessage = dataCacheDao.getDiaryMessageText((Timestamp)ed.data);
                        break;
                    case TwitterFeed.SENSOR_ID:
                        adfIcon = R.drawable.ic_twitter;
                        adfTitle = R.string.social_title;
                        adfMessage = dataCacheDao
                                .getCollectedExtra((Timestamp)ed.data,
                                        TwitterFeed.SENSOR_ID, TwitterFeed.SENSOR_ATTR_TEXT);
                        break;
                }
                runOnUiThread(() -> AlertDialogFragment.newInstance(
                        adfIcon,
                        adfTitle,
                        adfMessage,
                        R.string.dialog_ok,
                        (dialog, whichButton) -> dialog.dismiss()
                ).show(fragmentManager, TAG));
            }

        });
    }

    @Override
    public void onNothingSelected() {
        // Log.d(TAG, "OnChartValueSelectedListener onNothingSelected()");
    }

    /*
     *                             C H A R T
     */

    private boolean setupChartView() {
        try {
            TypedValue tv = new TypedValue();
            theme.resolveAttribute(R.attr.chart_lines, tv, true);
            @ColorInt int chartLines = tv.data;
            //theme.resolveAttribute(R.attr.chart_plot, tv, true);
            //@ColorInt final int chartPlot = tv.data;
            theme.resolveAttribute(R.attr.chart_text, tv, true);
            @ColorInt int chartText = tv.data;
            // CHART
            polarityChart = findViewById(R.id.polarity_chart);
            polarityChart.setOnChartValueSelectedListener(this);
            polarityChart.setDrawGridBackground(false);
            polarityChart.setDrawBorders(false);
            polarityChart.setDragYEnabled(false);
            polarityChart.setScaleYEnabled(false);
            polarityChart.getAxisRight().setEnabled(false);
            polarityChart.setVisibleXRange(DateTime.DAY_S * Sensei.CACHE_COLLECTED_DAYS, 180); // 3 min < X < cache max
            polarityChart.setDescription(null);
            polarityChart.setNoDataText(res.getString(R.string.chart_polarity_report_no_data));
            polarityChart.setNoDataTextColor(chartText);
            polarityChart.setMaxHighlightDistance(res.getDimension(R.dimen.chart_marker_highlight_distance_max));
            // DESCRIPTION + LEGEND
            Description description = new Description();
            description.setText(res.getString(R.string.chart_polarity_description));
            description.setTextColor(chartText);
            polarityChart.setDescription(description);
            polarityChart.getLegend().setEnabled(false);
            // X AXIS
            XAxis xAxis = polarityChart.getXAxis();
            xAxis.setDrawGridLines(false);
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setAxisLineColor(chartLines);
            xAxis.setTextColor(chartText);
            xAxis.setLabelCount(5);
            xAxis.setAvoidFirstLastClipping(true);
            // Y AXIS
            YAxis yAxis = polarityChart.getAxisLeft();
            yAxis.setAxisMinimum((float) (Polarity.NEGATIVE_MAX + Polarity.NEGATIVE_MAX * 0.1));
            yAxis.setAxisMaximum((float) (Polarity.POSITIVE_MAX + Polarity.POSITIVE_MAX * 0.1));
            yAxis.setDrawGridLines(false);
            yAxis.setDrawZeroLine(false);
            yAxis.setDrawAxisLine(false);
            yAxis.setDrawLabels(false);
            LimitLine llNegativeThreshold = new LimitLine((float) Polarity.NEGATIVE_MIN);
            llNegativeThreshold.setLineColor(res.getColor(R.color.polarity_negative, theme));
            yAxis.addLimitLine(llNegativeThreshold);
            LimitLine llPositiveThreshold = new LimitLine((float) Polarity.POSITIVE_MIN);
            llPositiveThreshold.setLineColor(res.getColor(R.color.polarity_positive, theme));
            yAxis.addLimitLine(llPositiveThreshold);
            yAxis.setDrawLimitLinesBehindData(true);
            return true;
        } catch (Exception e) {
            Log.e(TAG, "setupChartView() FAILED!", e);
            return false;
        }
    }

    private void setupChartDataObserver() {
        final Observer<List<Collected>> chartObserver = lc -> {
            CombinedData combinedData = null;
            if (lc != null && lc.size() > 0) {
                polarityChart.setNoDataText(res.getString(R.string.chart_polarity_report_processing));
                polarityChart.setMaxVisibleValueCount(lc.size() + 1);
                List<Entry> polarityChartEntries = new ArrayList<>(lc.size());
                long polarityChartEntriesBaseTimestamp = 0;
                for (int i = 0; i < lc.size(); i++) {
                    Collected c = lc.get(i);
                    Double polarity = c.sensorValue;
                    if (Polarity.isInvalid(polarity)) {
                        continue; // Ignore this invalid value and continue
                    }
                    int[] resource = Polarity.polarityToResources(polarity);
                    EntryData entryData = null;
                    long moment = TimeUnit.MILLISECONDS.toSeconds(c.moment.getTime());
                    if (i == 0) {
                        polarityChartEntriesBaseTimestamp = moment;
                    }
                    moment -= polarityChartEntriesBaseTimestamp;
                    Drawable icon;
                    String type = c.sensorType;
                    switch (type) {
                        case DiaryReportService.SENSOR_ID:
                            icon = res.getDrawable(R.drawable.ic_message, theme).mutate();
                            entryData = new EntryData(c.id, type, c.moment);
                            break;
                        case SmileReportService.SENSOR_ID:
                            icon = res.getDrawable(resource[2], theme);
                            break;
                        case TwitterFeed.SENSOR_ID:
                            icon = res.getDrawable(R.drawable.ic_twitter, theme).mutate();
                            entryData = new EntryData(c.id, type, c.moment);
                            break;
                        default:
                            Log.e(TAG, "Sensor type NOT FOUND: " + type);
                            icon = res.getDrawable(R.drawable.ic_yin_yang, theme);
                            break;
                    }
                    icon.setTint(res.getColor(resource[0], theme));
                    polarityChartEntries.add(new Entry(moment, polarity.floatValue(), icon, entryData));
                }
                ScatterDataSet dataSet = new ScatterDataSet(polarityChartEntries, res.getString(R.string.empty));
                //dataSet.setColor(chartPlot);
                dataSet.setScatterShapeSize(0); // Disable point shape draw
                dataSet.setDrawValues(false);
                // COMBINE DATA
                combinedData = new CombinedData();
                combinedData.setData(new ScatterData(dataSet));
                // AXIS + MARKERS
                polarityChart.getXAxis()
                        .setValueFormatter(
                                new TimestampXAxisValueFormatter(polarityChart, polarityChartEntriesBaseTimestamp));
                polarityChart
                        .setMarker(
                                new ValueTimestampMarkerView(
                                        getApplicationContext(),
                                        getCurrentThemeStyleId(),
                                        R.layout.chart_marker, R.id.chart_marker_view, R.string.chart_value_timestamp_marker,
                                        polarityChart, polarityChartEntriesBaseTimestamp));
            }
            // SET AND DRAW
            polarityChart.setData(combinedData);
            if (scaleHelper) {
                scaleHelper = false;
                float convertX = (float)Calc.convertCodomain(1,
                        polarityChart.getXChartMin(), polarityChart.getXChartMax(),
                        0, VISIBLE_X_RANGE);
                float scaleX = convertX < 1f ? 1f + (1f - convertX) : 1f;
                // Zoom in X if range > visible_range
                polarityChart.zoom(scaleX, 1f, polarityChart.getXChartMax(), 0f);
                polarityChart.moveViewToX(polarityChart.getXChartMax());
            } else {
                polarityChart.invalidate();
            }
        };
        final ChartViewModel chartViewModel = new ViewModelProvider(this).get(ChartViewModel.class);
        chartViewModel.getPolarityAll().observe(this, chartObserver);
    }
}
