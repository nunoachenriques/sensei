/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;

import com.google.android.gms.location.ActivityTransition;
import com.google.android.gms.location.DetectedActivity;

import net.nunoachenriques.sensei.emotion.Polarity;
import net.nunoachenriques.sensei.utility.Calc;
import net.nunoachenriques.sensei.utility.DateTime;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * The adapter to deal with the several units to fill the Dashboard.
 * Prepared for a list or grid view with a numeric position for every item.
 * Moreover, it creates the observers to change live data from the view model.
 * It uses the ViewHolder design pattern to improve performance.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see "res/layout/dashboard.xml"
 * @see SenseiActivity
 * @see <a href="https://developer.android.com/training/improving-layouts/smooth-scrolling.html#ViewHolder">
 *      ViewHolder design pattern</a>
 */
final class DashboardAdapter
        extends BaseAdapter {

    private static final String TAG = "DashboardAdapter";

    private final Context ctx;
    private final Resources res;
    private final LifecycleOwner lifecycleOwner;
    private final List<DashboardUnit> dashboardUnits;
    private final DashboardViewModel dashboardViewModel;
    private final SharedPreferences sharedPreferences;

    DashboardAdapter(Context c, LifecycleOwner lo, List<DashboardUnit> du, DashboardViewModel dvm) {
        super();
        ctx = c;
        res = c.getResources();
        lifecycleOwner = lo;
        dashboardUnits = du;
        dashboardViewModel = dvm;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
    }

    @Override
    public int getCount() {
        return dashboardUnits.size();
    }

    @Override
    public Object getItem(int position) {
        return dashboardUnits.get(position);
    }

    @Override
    public long getItemId(int position) {
        return -1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CardView cv = (CardView)convertView;
        if (cv == null) {
            ViewHolder vh = new ViewHolder();
            DashboardUnit du = dashboardUnits.get(position);
            switch (du.getType()) {
                case DashboardUnit.TITLE_PROGRESS_IMAGE:
                    cv = (CardView)LayoutInflater.from(ctx)
                            .inflate(R.layout.dashboard_unit_title_progress_image, parent, false);
                    vh.progressView = cv.findViewById(R.id.dashboard_unit_progress_view);
                    vh.imageView = cv.findViewById(R.id.dashboard_unit_image_view);
                    break;
                case DashboardUnit.TITLE_VALUE_HISTOGRAM:
                    cv = (CardView)LayoutInflater.from(ctx)
                            .inflate(R.layout.dashboard_unit_title_value_histogram, parent, false);
                    vh.valueView = cv.findViewById(R.id.dashboard_unit_value_view);
                    vh.histogramView = cv.findViewById(R.id.dashboard_unit_histogram_view);
                    break;
                case DashboardUnit.TITLE_VALUE_IMAGE:
                    cv = (CardView)LayoutInflater.from(ctx)
                            .inflate(R.layout.dashboard_unit_title_value_image, parent, false);
                    vh.valueView = cv.findViewById(R.id.dashboard_unit_value_view);
                    vh.imageView = cv.findViewById(R.id.dashboard_unit_image_view);
                    break;
                case DashboardUnit.TITLE_VALUE_PROGRESS:
                    cv = (CardView)LayoutInflater.from(ctx)
                            .inflate(R.layout.dashboard_unit_title_value_progress, parent, false);
                    vh.valueView = cv.findViewById(R.id.dashboard_unit_value_view);
                    vh.progressView = cv.findViewById(R.id.dashboard_unit_progress_view);
                    break;
            }
            vh.titleView = Objects.requireNonNull(cv).findViewById(R.id.dashboard_unit_title_view);
            cv.setTag(vh);
            cv.setId(du.getId());
            setViewObserver(cv);
            return cv;
        } else {
            return convertView;
        }
    }

    /**
     * The common view holder pattern class for every dashboard unit.
     *
     * @see <a href="https://developer.android.com/training/improving-layouts/smooth-scrolling.html#ViewHolder">
     *      ViewHolder design pattern</a>
     */
    static class ViewHolder {
        TextView titleView;
        TextView valueView;
        ImageView imageView;
        ProgressBar progressView;
        HistogramCircularView histogramView;
    }

    private void setViewObserver(final CardView cv) {
        cv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                cv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                setDataObserver(cv);
            }
        });
    }

    private void setDataObserver(final CardView cv) {
        final ViewHolder vh  = (ViewHolder)cv.getTag();
        switch (cv.getId()) {
            case R.id.dashboard_you_feel:

                // TODO learned human current sentiment!

                cv.setContentDescription(res.getString(R.string.dashboard_you_feel_description));
                final TypedValue tv = new TypedValue();
                dashboardViewModel.getPolarityAll().observe(lifecycleOwner, lc -> {
                    vh.titleView.setText(R.string.dashboard_you_feel_title);
                    double polarity = Reasoning.currentEmotionalValence(lc);
                    if (polarity != Polarity.NO_POLARITY) {
                        int polarityPercent = (int)Calc.convertCodomain(
                                polarity, -1, 1, 0, 100);
                        vh.progressView.setIndeterminate(false);
                        vh.progressView.setProgress(polarityPercent);
                        vh.progressView.setProgressTintList(Empathy
                                .getProgressTintList(false, polarityPercent));
                        if (Polarity.isNegative(polarity)) {
                            vh.imageView.setImageResource(R.drawable.ic_face_negative);
                        } else if (Polarity.isNeutral(polarity)) {
                            vh.imageView.setImageResource(R.drawable.ic_face_neutral);
                        } else if (Polarity.isPositive(polarity)) {
                            vh.imageView.setImageResource(R.drawable.ic_face_positive);
                        }
                        Resources.Theme theme = ctx.getTheme();
                        theme.resolveAttribute(R.attr.dashboard_icon, tv, true);
                        vh.imageView.setImageTintList(res.getColorStateList(tv.resourceId, theme));
                        vh.imageView.setImageTintMode(PorterDuff.Mode.SRC_IN);
                    }
                });
                break;
            case R.id.dashboard_you_feel_counters:
                cv.setContentDescription(res.getString(R.string.dashboard_you_feel_counters_description));
                vh.titleView.setText(R.string.dashboard_you_feel_counters_title);
                vh.histogramView.setColor(0, ContextCompat.getColor(ctx, R.color.polarity_positive));
                vh.histogramView.setColor(1, ContextCompat.getColor(ctx, R.color.polarity_neutral));
                vh.histogramView.setColor(2, ContextCompat.getColor(ctx, R.color.polarity_negative));
                dashboardViewModel.getSmileCount().observe(lifecycleOwner, l -> {
                    if (l.one != null && l.two != null && l.three != null) {
                        long sum = l.one + l.two + l.three;
                        vh.valueView.setText(String.valueOf(sum));
                        vh.histogramView.setValues(new ArrayList<Pair<Integer, Long>>(3) {{
                            add(Pair.create(0, l.three));
                            add(Pair.create(1, l.two));
                            add(Pair.create(2, l.one));
                        }});
                    } else {
                        vh.valueView.setText("");
                    }
                });
                break;
            case R.id.dashboard_me_empathy:
                cv.setContentDescription(res.getString(R.string.dashboard_me_empathy_description));
                vh.titleView.setText(R.string.dashboard_me_empathy_title);
                dashboardViewModel.getEmpathy().observe(lifecycleOwner, empathy -> {
                    int e = Calc.round(empathy);
                    vh.progressView.setProgress(e);
                    vh.progressView.setProgressTintList(Empathy.getInstance(ctx)
                            .getProgressTintList(
                                    sharedPreferences.getBoolean(
                                            res.getString(R.string.data_pause_mode_pref_key), false)));
                    vh.valueView.setText(
                            (new DecimalFormat("##0%")).format(e / (float)Empathy.MAX_DEFAULT));
                });
                dashboardViewModel.getOnPause().observe(lifecycleOwner, value -> vh.progressView.setProgressTintList(Empathy.getInstance(ctx).getProgressTintList(value)));
                break;
            case R.id.dashboard_you_activity:
                cv.setContentDescription(res.getString(R.string.dashboard_you_activity_current_description));
                vh.imageView.setImageResource(R.drawable.ic_question_mark);
                vh.valueView.setText(R.string.dashboard_you_activity_unknown);
                vh.titleView.setText(res.getString(R.string.dashboard_you_activity_current_title));
                dashboardViewModel.getActivityLast(1).observe(lifecycleOwner, lc -> {
                    if (lc.size() > 0) {
                        /*
                         * The select for the last activity order by id desc (reversed):
                         *  0 = activity elapsed
                         *  1 = activity transition
                         *  2 = activity type
                         */
                        if (lc.get(1).sensorValue.intValue() == ActivityTransition.ACTIVITY_TRANSITION_ENTER) {
                            final int[] r = ActivityService.activityToResources(lc.get(2).sensorValue.intValue());
                            vh.valueView.setText(r[0]);
                            vh.imageView.setImageResource(r[1]);
                        } else {
                            vh.valueView.setText(R.string.dashboard_you_activity_unknown);
                            vh.imageView.setImageResource(R.drawable.ic_question_mark);
                        }
                        Log.d(TAG, "ActivityFeed: "
                                + ActivityService.getTypeName(lc.get(2).sensorValue.intValue())
                                + " | " + ActivityService.getTransitionName(lc.get(1).sensorValue.intValue()));
                    }
                });
                break;
            case R.id.dashboard_you_activity_vehicle:
                setActivityDataObserver(DetectedActivity.IN_VEHICLE, R.string.dashboard_you_activity_vehicle, R.drawable.ic_activity_car, vh, cv);
                break;
            case R.id.dashboard_you_activity_bicycle:
                setActivityDataObserver(DetectedActivity.ON_BICYCLE, R.string.dashboard_you_activity_bicycle, R.drawable.ic_activity_bike, vh, cv);
                break;
            case R.id.dashboard_you_activity_running:
                setActivityDataObserver(DetectedActivity.RUNNING, R.string.dashboard_you_activity_running, R.drawable.ic_activity_run, vh, cv);
                break;
            case R.id.dashboard_you_activity_still:
                setActivityDataObserver(DetectedActivity.STILL, R.string.dashboard_you_activity_still, R.drawable.ic_activity_still, vh, cv);
                break;
            case R.id.dashboard_you_activity_walking:
                setActivityDataObserver(DetectedActivity.WALKING, R.string.dashboard_you_activity_walking, R.drawable.ic_activity_walk, vh, cv);
                break;
        }
        cv.setOnClickListener(v -> Toast.makeText(ctx, v.getContentDescription(), Toast.LENGTH_SHORT).show());
    }

    private void setActivityDataObserver(final int activityType,
                                         int titleId,
                                         int imageId,
                                         final ViewHolder vh,
                                         final View v) {
        v.setContentDescription(res.getQuantityString(R.plurals.dashboard_you_activity_description, Sensei.CACHE_COLLECTED_DAYS, Sensei.CACHE_COLLECTED_DAYS));
        vh.imageView.setImageResource(imageId);
        vh.titleView.setText(titleId);
        /*
         * Despite having the activityType input, the "get all" logic is still
         * required due to the Collected way of storing events: if trying to
         * filter by sensorAttribute (e.g., '1.0' = ON_BICYCLE) then will fail
         * getting the proper transitions and elapsed times.
         */
        dashboardViewModel.getActivityAll().observe(lifecycleOwner, lc -> {
            if (lc != null) {
                /*
                 * Every detected activity EXIT transition closes the ENTER
                 * interval and duration may be calculated. Each duration is
                 * accumulated to give the total time per activity.
                 */
                long sum = 0;
                int lastType = Integer.MIN_VALUE;
                int lastTransition = Integer.MIN_VALUE;
                long lastElapsed = 0;
                for (int i = 0; i < lc.size(); i += ActivityFeed.SENSOR_FIXED_ATTRS) {
                    lastType = lc.get(i).sensorValue.intValue();
                    if (lastType == activityType) {
                        int transition = lc.get(i + 1).sensorValue.intValue();
                        long elapsed = lc.get(i + 2).sensorValue.longValue();
                        if (transition == ActivityTransition.ACTIVITY_TRANSITION_EXIT) {
                            if (lastTransition == ActivityTransition.ACTIVITY_TRANSITION_ENTER) {
                                sum += TimeUnit.NANOSECONDS.toMillis(Math.max(elapsed - lastElapsed, 0));
                            }
                        }
                        lastTransition = transition;
                        lastElapsed = elapsed;
                    }
                }
                if (lastType == activityType && lastTransition == ActivityTransition.ACTIVITY_TRANSITION_ENTER) {
                    sum += TimeUnit.NANOSECONDS
                            .toMillis(Math.max(SystemClock.elapsedRealtimeNanos() - lastElapsed, 0));
                }
                long[] d = DateTime.durationMillisToDayHourMinute(sum);
                vh.valueView.setText(res.getString(R.string.dashboard_you_activity_value, d[0], d[1], d[2]));
            }
        });
    }
}
