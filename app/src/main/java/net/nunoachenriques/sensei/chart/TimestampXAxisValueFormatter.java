/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.chart;

import android.util.Log;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * An axis values formatter for the MPAndroidChart charts.
 * Given a timestamp in seconds, a text with date time conversion is returned.
 *
 * NOTICE: the need for a base timestamp in seconds, all the values are
 * expected to be relative to this reference. This is a workaround for the
 * {@code float} data type 32-bit restriction, millisecond resolution needs a
 * 64-bit data type such as {@code long}.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see java.text.SimpleDateFormat
 */
public final class TimestampXAxisValueFormatter
        implements IAxisValueFormatter {

    private static final String TAG = "TSXAxisVFormatter";
    private static final long DAY_S = 86400;

    private final BarLineChartBase<?> baseChart;
    private final long baseTimestamp;
    private SimpleDateFormat sdfYearMonthDay;
    private SimpleDateFormat sdfMonthDay;
    private SimpleDateFormat sdfDayHourMinute;
    private SimpleDateFormat sdfHourMinute;

    /**
     * Sets timestamp base to add to the {@link #getFormattedValue(float, AxisBase)} value.
     *
     * @param chart The chart with the axis to be formatted.
     * @param tsReference Timestamp reference in seconds.
     */
    public TimestampXAxisValueFormatter(BarLineChartBase<?> chart, long tsReference) {
        baseTimestamp = tsReference;
        baseChart = chart;
        try {
            sdfYearMonthDay = new SimpleDateFormat("yyyy-MM-dd", Locale.ROOT);
            sdfMonthDay = new SimpleDateFormat("MM-dd", Locale.ROOT);
            sdfDayHourMinute = new SimpleDateFormat("dd HH:mm", Locale.ROOT);
            sdfHourMinute = new SimpleDateFormat("HH:mm", Locale.ROOT);
        } catch (Exception e) {
            Log.e(TAG, "Hint: check arguments pattern and locale!", e);
        }
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        long millis = TimeUnit.SECONDS.toMillis(baseTimestamp + (long)value);
        float range = baseChart.getVisibleXRange();
        try {
            if (range < DAY_S) {
                return sdfHourMinute.format(millis);
            } else if (range < DAY_S * 28) {
                return sdfDayHourMinute.format(millis);
            } else if (range < DAY_S * 365) {
                return sdfMonthDay.format(millis);
            } else {
                return sdfYearMonthDay.format(millis);
            }
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "Hint: check format(argument)!", e);
            return "NaN";
        }
    }
}
