/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.chart;

import java.io.Serializable;

/**
 * Chart data holder helper class for extra functionality.
 * Use case: on value click listener show extra data in a dialogue.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class EntryData
        implements Serializable {

    private final long id;
    public final String type;
    public final Object data;

    public EntryData(long i, String t, Object d) {
        id = i;
        type = t;
        data = d;
    }

    @Override
    public String toString() {
        return "EntryData{" +
                "id=" + id +
                ", type=" + type +
                ", data=" + data +
                "}";
    }
}
