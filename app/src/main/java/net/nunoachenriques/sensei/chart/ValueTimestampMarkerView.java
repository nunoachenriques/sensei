/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.chart;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;

import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * A pop up view marker on point highlighting for the MPAndroidChart charts.
 * Shows entry y value as a float and x axis value as timestamp in a text view.
 *
 * NOTICE: the need for a base timestamp in seconds, all the values are
 * expected to be relative to this reference. This is a workaround for the
 * {@code float} data type 32-bit restriction, millisecond resolution needs a
 * 64-bit data type such as {@code long}.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see SimpleDateFormat
 * @see <a href="https://github.com/PhilJay/MPAndroidChart/wiki/IMarker-Interface">MarkerView custom how to</a>
 * @see <a href="https://github.com/PhilJay/MPAndroidChart/blob/master/MPChartLib/src/main/java/com/github/mikephil/charting/components/MarkerView.java">MarkerView source code</a>
 */
@SuppressLint("ViewConstructor")
public final class ValueTimestampMarkerView
        extends MarkerView {

    private static final String TAG = "ValueTSMarkerView";

    private final Chart<?> chart;
    private final TextView textView;
    private final @StringRes int resString;
    private final long baseTimestamp;
    private SimpleDateFormat simpleDateFormat;
    private MPPointF offset;
    private final MPPointF offsetForDrawing;

    /**
     * Constructor. Sets up the MarkerView with a custom layout resource.
     *
     * @param ctx The app context.
     * @param theme Theme id to be set to (e.g., R.style.SenseiDark).
     * @param layout Marker layout widget id (e.g., R.layout.chart_marker).
     * @param view Marker {@link TextView} id in layout widget (e.g., R.id.chart_marker_view).
     * @param sid Marker string id (e.g., R.string.chart_value_timestamp_marker) with two formatter fields %f for value and %s for timestamp.
     * @param inChart The chart in which the markers will be rendered.
     * @param tsReference Timestamp reference in seconds.
     */
    public ValueTimestampMarkerView(Context ctx, @StyleRes int theme, @LayoutRes int layout, @IdRes int view, @StringRes int sid, Chart<?> inChart, long tsReference) {
        super(ctx, layout);
        ctx.setTheme(theme);
        chart = inChart; // Workaround to avoid clipping on the right most label! See getOffsetForDrawingAtPoint()
        textView = findViewById(view);
        resString = sid;
        baseTimestamp = tsReference;
        try {
            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ROOT);
        } catch (Exception e) {
            Log.e(TAG, "Hint: check arguments pattern and locale!", e);
        }
        offsetForDrawing = new MPPointF();
    }

    @Override
    public void refreshContent(final Entry e, Highlight highlight) {
        textView.setText(getResources().getString(resString, e.getY(), simpleDateFormat.format(TimeUnit.SECONDS.toMillis(baseTimestamp + (long)e.getX()))));
        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        if (offset == null) {
            // center the marker horizontally (x >> 1 = x / 2)
            offset = new MPPointF(-(getWidth() >> 1), 0);
        }
        return offset;
    }

    @Override
    public MPPointF getOffsetForDrawingAtPoint(float posX, float posY) {
        MPPointF offset = getOffset();
        float width = getWidth();
        float height = getHeight();

        offsetForDrawing.x = offset.x;
        offsetForDrawing.y = offset.y;
        if (posX + offsetForDrawing.x < 0) {
            offsetForDrawing.x = - posX;
        } else if (chart != null && posX + width + offsetForDrawing.x > chart.getWidth()) {
            offsetForDrawing.x = chart.getWidth() - posX - width;
        }
        if (posY + offsetForDrawing.y < 0) {
            offsetForDrawing.y = - posY;
        } else if (chart != null && posY + height + offsetForDrawing.y > chart.getHeight()) {
            offsetForDrawing.y = chart.getHeight() - posY - height;
        }
        return offsetForDrawing;
    }
}
