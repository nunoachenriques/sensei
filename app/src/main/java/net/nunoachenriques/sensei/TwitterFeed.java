/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.Context;
import android.util.Log;
import android.util.Pair;

import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;

import net.nunoachenriques.sensei.emotion.Polarity;
import net.nunoachenriques.sensei.emotion.Sentiment;
import net.nunoachenriques.sensei.persistence.Collected;
import net.nunoachenriques.sensei.persistence.DataCache;
import net.nunoachenriques.sensei.persistence.DataCacheDao;
import net.nunoachenriques.sensei.utility.DateTime;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import twitter4j.ConnectionLifeCycleListener;
import twitter4j.DirectMessage;
import twitter4j.FilterQuery;
import twitter4j.RateLimitStatus;
import twitter4j.RateLimitStatusEvent;
import twitter4j.RateLimitStatusListener;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.User;
import twitter4j.UserList;
import twitter4j.UserStreamListener;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * The Twitter social network feed listener.
 * Catches data from listening user's Twitter timeline
 * and sends it to the service for processing and storage.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class TwitterFeed {

    private static final String TAG = "TwitterFeed";

    public static final String SENSOR_ID = "app.human.tweet";
    static final String SENSOR_ATTR_TEXT = "text";
    private static final String SENSOR_ATTR_POLARITY = Polarity.SENSOR_ATTR_POLARITY_COMMON;

    /**
     * Twitter's date pattern "{@code EEE MMM dd HH:mm:ss Z yyyy}"
     * (e.g., <a href="https://dev.twitter.com/rest/reference/post/statuses/update">Wed Sep 05 00:37:15 +0000 2012</a>).
     */
    private static final String TWITTER_DATE_PATTERN = "EEE MMM dd HH:mm:ss Z yyyy";
    /** Twitter's time zone */
    private static final TimeZone TWITTER_TIMEZONE = TimeZone.getTimeZone("UTC");
    /** Twitter's locale */
    private static final Locale TWITTER_LOCALE = Locale.US;
    private static final SimpleDateFormat twitterDateFormat;
    static {
        twitterDateFormat = new SimpleDateFormat(TWITTER_DATE_PATTERN, TWITTER_LOCALE);
        twitterDateFormat.setTimeZone(TWITTER_TIMEZONE);
    }

    private static volatile TwitterFeed INSTANCE;

    private TwitterStream twitterStream;
    private Sentiment sentiment;
    private ExecutorService executorService;
    private DataCacheDao dataCacheDao;

    private TwitterFeed(Context c)
            throws Exception {
        sentiment = Sentiment.getInstance(c);
        executorService = Executors.newSingleThreadExecutor();
        dataCacheDao = DataCache.getInstance(c).getDao();
        // Twitter initialization with consumer app key and secret from config file. REQUIRED before any call to Twitter kit!
        final TwitterConfig config = new TwitterConfig.Builder(c)
                .twitterAuthConfig(new TwitterAuthConfig(c.getResources().getString(R.string.com_twitter_sdk_android_CONSUMER_KEY), c.getResources().getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET)))
                .logger(new DefaultLogger(BuildConfig.DEBUG ? Log.DEBUG : Log.INFO))
                .debug(BuildConfig.DEBUG)
                .build();
        Twitter.initialize(config);
    }

    /**
     * Gets an instance of this class. If no single instance is available, i.e.,
     * {@code null}, then creates and returns a single new one
     * (Singleton pattern).
     *
     * @param c Android application {@link Context}.
     * @return The single instance of this class.
     */
    public static TwitterFeed getInstance(Context c)
            throws Exception {
        if (INSTANCE == null) {
            synchronized (TwitterFeed.class) {
                if (INSTANCE == null) {
                    INSTANCE = new TwitterFeed(c);
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Starts (requests) receiving Twitter status updates.
     *
     * @return {@code true} on success, {@code false} otherwise.
     */
    boolean start() {
        try {
            final TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
            if (session != null) {
                TwitterAuthToken tat = session.getAuthToken();
                TwitterAuthConfig tac = TwitterCore.getInstance().getAuthConfig();
                Configuration twitterConfiguration = new ConfigurationBuilder()
                        .setDebugEnabled(BuildConfig.DEBUG)
                        .setOAuthConsumerKey(tac.getConsumerKey())
                        .setOAuthConsumerSecret(tac.getConsumerSecret())
                        .setOAuthAccessToken(tat.token)
                        .setOAuthAccessTokenSecret(tat.secret)
                        .setIncludeMyRetweetEnabled(false)
                        .build();
                twitterStream = new TwitterStreamFactory(twitterConfiguration).getInstance();
                twitterStream.addConnectionLifeCycleListener(connectionListener());
                twitterStream.addRateLimitStatusListener(rateListener());
                twitterStream.addListener(userStreamListener());
                twitterStream.filter(new FilterQuery(TwitterCore.getInstance().getSessionManager().getActiveSession().getUserId()));
            }
        } catch (IllegalStateException e) {
            Log.w(TAG, "start FAILED! Hint: is Twitter initialized?" + e.getMessage());
            twitterStream = null;
        }
        return twitterStream != null && twitterStream.getAuthorization().isEnabled();
    }

    /**
     * Stops listening Twitter status updates.
     */
    void shutdown() {
        if (twitterStream != null) {
            twitterStream.shutdown();
            twitterStream.clearListeners();
            twitterStream = null;
        }
    }

    private ConnectionLifeCycleListener connectionListener() {
        return new ConnectionLifeCycleListener() {
            @Override
            public void onConnect() {
                Log.i(TAG, "Stream CONNECTED");
            }
            @Override
            public void onDisconnect() {
                Log.i(TAG, "Stream DISCONNECTED!");
            }
            @Override
            public void onCleanUp() {
                Log.i(TAG, "Stream CLEANUP...!");
            }
        };
    }

    private RateLimitStatusListener rateListener() {
        return new RateLimitStatusListener() {
            @Override
            public void onRateLimitStatus(RateLimitStatusEvent event) {
                RateLimitStatus rls = event.getRateLimitStatus();
                Log.w(TAG, "Rate (remaining/limit): " + rls.getRemaining() + "/" + rls.getLimit());
            }
            @Override
            public void onRateLimitReached(RateLimitStatusEvent event) {
                RateLimitStatus rls = event.getRateLimitStatus();
                Log.w(TAG, "Rate LIMIT reached! Reset in " + rls.getSecondsUntilReset() + " s");
            }
        };
    }

    private StatusListener userStreamListener() {
        return new UserStreamListener() {
            @Override
            public void onDeletionNotice(long l, long l1) {
                Log.d(TAG, "onDeletionNotice l: " + l + " l1: " + l1);
            }
            @Override
            public void onFriendList(long[] longs) {
                Log.d(TAG, "onFriendList longs: " + Arrays.toString(longs));
            }
            @Override
            public void onFavorite(User user, User user1, Status status) {
                Log.d(TAG, "onFavorite user: " + user.getScreenName() + " user1: " + user1.getScreenName() + " status: " + status.toString());
            }
            @Override
            public void onUnfavorite(User user, User user1, Status status) {
                Log.d(TAG, "onUnfavorite user: " + user.getScreenName() + " user1: " + user1.getScreenName() + " status: " + status.toString());
            }
            @Override
            public void onFollow(User user, User user1) {
                Log.d(TAG, "onFollow user: " + user.getScreenName() + " user1: " + user1.getScreenName());

            }
            @Override
            public void onUnfollow(User user, User user1) {
                Log.d(TAG, "onUnfollow user: " + user.getScreenName() + " user1: " + user1.getScreenName());
            }
            @Override
            public void onDirectMessage(DirectMessage directMessage) {
                Log.d(TAG, "onDirectMessage directMessage: " + directMessage);
            }
            @Override
            public void onUserListMemberAddition(User user, User user1, UserList userList) {
                Log.d(TAG, "onUserListMemberAddition user: " + user.getScreenName() + " user1: " + user1.getScreenName() + " userList: " + userList.toString());
            }
            @Override
            public void onUserListMemberDeletion(User user, User user1, UserList userList) {
                Log.d(TAG, "onUserListMemberDeletion user: " + user.getScreenName() + " user1: " + user1.getScreenName() + " userList: " + userList.toString());
            }
            @Override
            public void onUserListSubscription(User user, User user1, UserList userList) {
                Log.d(TAG, "onUserListSubscription user: " + user.getScreenName() + " user1: " + user1.getScreenName() + " userList: " + userList.toString());
            }
            @Override
            public void onUserListUnsubscription(User user, User user1, UserList userList) {
                Log.d(TAG, "onUserListUnsubscription user: " + user.getScreenName() + " user1: " + user1.getScreenName() + " userList: " + userList.toString());
            }
            @Override
            public void onUserListCreation(User user, UserList userList) {
                Log.d(TAG, "onUserListCreation user: " + user.getScreenName() + " userList: " + userList.toString());
            }
            @Override
            public void onUserListUpdate(User user, UserList userList) {
                Log.d(TAG, "onUserListUpdate user: " + user.getScreenName() + " userList: " + userList.toString());
            }
            @Override
            public void onUserListDeletion(User user, UserList userList) {
                Log.d(TAG, "onUserListDeletion user: " + user.getScreenName() + " userList: " + userList.toString());
            }
            @Override
            public void onUserProfileUpdate(User user) {
                Log.d(TAG, "onUserProfileUpdate user: " + user.getScreenName());
            }
            @Override
            public void onUserSuspension(long l) {
                Log.d(TAG, "onUserSuspension l: " + l);
            }
            @Override
            public void onUserDeletion(long l) {
                Log.d(TAG, "onUserDeletion l: " + l);
            }
            @Override
            public void onBlock(User user, User user1) {
                Log.d(TAG, "onBlock user: " + user.getScreenName() + " user1: " + user1.getScreenName());
            }
            @Override
            public void onUnblock(User user, User user1) {
                Log.d(TAG, "onUnblock user: " + user.getScreenName() + " user1: " + user1.getScreenName());
            }
            @Override
            public void onRetweetedRetweet(User user, User user1, Status status) {
                Log.d(TAG, "onRetweetedRetweet user: " + user.getScreenName() + " user1: " + user1.getScreenName() + " status: " + status.toString());
            }
            @Override
            public void onFavoritedRetweet(User user, User user1, Status status) {
                Log.d(TAG, "onFavoritedRetweet user: " + user.getScreenName() + " user1: " + user1.getScreenName() + " status: " + status.toString());
            }
            @Override
            public void onQuotedTweet(User user, User user1, Status status) {
                Log.d(TAG, "onQuotedTweet user: " + user.getScreenName() + " user1: " + user1.getScreenName() + " status: " + status.toString());
            }
            /*
             * Here is where it gets interesting :-)
             */
            @Override
            public void onStatus(final Status status) {
                Log.d(TAG, "onStatus: " + status.toString());
                executorService.execute(() -> {
                    String text = status.getText().trim();
                    if (!text.isEmpty()) {
                        // SENTIMENT (LANGUAGE + POLARITY)
                        Float polarity = Sentiment.NO_SENTIMENT;
                        String language = null;
                        Pair<Float, String> polarityLanguage = sentiment.getPolarityLanguage(text);
                        if (polarityLanguage != null) {
                            polarity = polarityLanguage.first;
                            language = polarityLanguage.second;
                        }
                        // STORE
                        Timestamp ts = new Timestamp(status.getCreatedAt().getTime());
                        String tz = DateTime.getTimeZoneISO8601();
                        List<Collected> lc = new ArrayList<>(2);
                        lc.add(new Collected(ts, tz, SENSOR_ID, SENSOR_ATTR_TEXT, 0.0, text, false));
                        lc.add(new Collected(ts, tz, SENSOR_ID, SENSOR_ATTR_POLARITY, polarity.doubleValue(), language, false));
                        try {
                            dataCacheDao.set(lc.toArray(new Collected[0]));
                            Log.d(TAG, "TWEET: " + lc.toString());
                        } catch (Exception e) {
                            Log.e(TAG, "TWEET [" + text + "] (@" + status.getUser().getScreenName() + ") DATA DISCARDED! ", e);
                        }
                    }
                });
            }
            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
                Log.d(TAG, "onDeletionNotice statusDeletionNotice: " + statusDeletionNotice.toString());
            }
            @Override
            public void onTrackLimitationNotice(int i) {
                Log.w(TAG, "Track LIMITATION notice! # of limited statuses: " + i);
            }
            @Override
            public void onScrubGeo(long l, long l1) {
                Log.w(TAG, "Scrub Geo EVENT! user id: " + l + " up-to-status id: " + l1);
            }
            @Override
            public void onStallWarning(StallWarning stallWarning) {
                Log.w(TAG, "Stalling: " + stallWarning);
            }
            @Override
            public void onException(Exception e) {
                Log.e(TAG, "Unknown ERROR!", e);
            }
        };
    }
}
