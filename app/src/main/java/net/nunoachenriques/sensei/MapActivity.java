/*
 * Copyright 2020 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.location.ActivityTransition;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import net.nunoachenriques.sensei.emotion.Polarity;
import net.nunoachenriques.sensei.persistence.DataCache;
import net.nunoachenriques.sensei.utility.AlertDialogFragment;
import net.nunoachenriques.sensei.utility.Calc;
import net.nunoachenriques.sensei.utility.DateTime;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.MapListener;
import org.osmdroid.events.ScrollEvent;
import org.osmdroid.events.ZoomEvent;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.BoundingBox;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.CustomZoomButtonsController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.Projection;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Polygon;
import org.osmdroid.views.overlay.TilesOverlay;
import org.osmdroid.views.overlay.infowindow.InfoWindow;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.MGRSRef;

/**
 * The geographic map showing emotional valence predictions in context.
 * Get data from Expanse regarding current timestamp and location.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see "res/layout/map.xml"
 * @see <a href="https://github.com/osmdroid/osmdroid">OpenStreetMap-Tools for Android</a>
 */
public final class MapActivity
        extends SenseiAppCompatActivity {

    private static final String TAG = "MapActivity";
    private static final String SENSOR_ID = "app.human.map";

    public static final String EXTRA_LOCATION = TAG + "location";

    private static final int MGRS_CELL_SIZE = 1000; // MGRS cell side length = 1000m
    private static final double ZOOM_MIN = 10.0;
    private static final double ZOOM_MAX = 20.0;
    private static final double ZOOM_DEFAULT = 14.0;
    private static final DecimalFormat PROBABILITY_PATTERN = new DecimalFormat("#.##");
    private static final DecimalFormat SCORE_PATTERN = new DecimalFormat("###%");

    private final Map<String, MapPrediction> mapPredictionsCache = new HashMap<>(100);
    private volatile Calendar mapPredictionsCacheTimestamp = GregorianCalendar.getInstance();

    private Resources res;
    private Resources.Theme theme;
    private Transition transition;
    private ExpanseSync expanseSync;
    private ExecutorService executorService;
    private FusedLocationProviderClient fusedLocation;
    private LocationCallback locationCallback;
    private LocationRequest locationRequest;
    private GeoPoint locationGeoPoint = null;
    private List<Polygon> predictionPolygons;
    private MapView mapView = null;
    private IMapController mapController = null;
    private Marker activityMarker;
    private Marker currentMarker;
    private Paint paintNegative;
    private Paint paintNeutral;
    private Paint paintPositive;
    private Paint paintCommonStroke;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        res = getResources();
        theme = getTheme();
        setContentView(R.layout.map);
        setTitle(R.string.map_title);
        setSupportActionBar();
        transition = new Transition(this, SENSOR_ID);
        expanseSync = ExpanseSync.getInstance(DataCache.getInstance(this).getDao());
        executorService = Executors.newSingleThreadExecutor();
        fusedLocation = LocationServices.getFusedLocationProviderClient(this);
        // LOCATION FROM INTENT
        Intent intent = getIntent();
        double[] locationSent = intent.getDoubleArrayExtra(EXTRA_LOCATION);
        if (locationSent != null) {
            locationGeoPoint = new GeoPoint(locationSent[0], locationSent[1]);
        }
        Log.d(TAG, "Location SENT: " + Arrays.toString(locationSent) + " | GEO POINT: " + locationGeoPoint);
        // MAP VIEW + CONTROLLER
        if (setupMap(isCurrentThemeDark())) {
            setupPaintStyles();
            setupDataObservers(new ViewModelProvider(this).get(MapViewModel.class));
        } else {
            AlertDialogFragment.newInstance(
                    R.drawable.ic_question_mark,
                    R.string.failure,
                    res.getString(R.string.map_failure),
                    R.string.dialog_ok,
                    (dialog, whichButton) -> {
                        dialog.dismiss();
                        finishAndRemoveTask();
                    }
            ).show(getSupportFragmentManager(), TAG);
        }
        // LOCATION UPDATES
        locationRequest = LocationRequest.create()
                .setInterval(60000)
                .setFastestInterval(10000)
                .setSmallestDisplacement(10.0f)
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult != null) {
                    (new Handler()).post(() ->
                            Toast.makeText(getApplicationContext(),
                                    R.string.map_location_data, Toast.LENGTH_SHORT).show());
                    executorService.execute(() -> {
                        try {
                            updateMapPredictions(locationResult.getLastLocation());
                        } catch (Exception e) {
                            Log.e(TAG, "Unknown FAILURE during Map predictions update!", e);
                        }
                    });
                } else {
                    (new Handler()).post(() ->
                            Toast.makeText(getApplicationContext(),
                                    R.string.map_no_location_data, Toast.LENGTH_SHORT).show());
                    Log.w(TAG, "Location FAILURE during callback, result is NULL!");
                }
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
        fusedLocation.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
        transition.setEnter();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
        fusedLocation.removeLocationUpdates(locationCallback);
        transition.setExit();
    }

    /*
     *                               M A P
     */

    private boolean setupMap(boolean darkTheme) {
        try {
            // View config
            Configuration.getInstance().setUserAgentValue(BuildConfig.APPLICATION_ID);
            mapView = findViewById(R.id.map);
            mapView.setTileSource(TileSourceFactory.MAPNIK);
            mapView.setTilesScaledToDpi(true);
            mapView.setMinZoomLevel(ZOOM_MIN);
            mapView.setMaxZoomLevel(ZOOM_MAX);
            mapView.getZoomController().setVisibility(CustomZoomButtonsController.Visibility.NEVER);
            mapView.setMultiTouchControls(true);
            if (darkTheme) {
                mapView.getOverlayManager().getTilesOverlay().setColorFilter(TilesOverlay.INVERT_COLORS);
            }
            // Controller
            mapController = mapView.getController();
            mapController.setZoom(ZOOM_DEFAULT);
            if (locationGeoPoint != null) {
                mapController.setCenter(locationGeoPoint);
            }
            // Marker: current sentiment and activity
            activityMarker = new Marker(mapView);
            currentMarker = new Marker(mapView);
            predictionPolygons = new ArrayList<>();
            mapView.addMapListener(new MapListener() {
                @Override
                public boolean onScroll(ScrollEvent event) {
                    return false;
                }
                @Override
                public boolean onZoom(ZoomEvent event) {
                    setActivityMarkerPositionRelativeToCurrent();
                    return true;
                }
            });
            mapView.invalidate();
            return true;
        } catch (Exception e) {
            Log.e(TAG, "setupMapView() FAILED!", e);
            return false;
        }
    }

    private void setActivityMarkerPositionRelativeToCurrent() {
        GeoPoint currentMarkerPosition = currentMarker.getPosition();
        if (currentMarkerPosition != null) {
            Projection projection = mapView.getProjection();
            Point point = projection.toPixels(currentMarkerPosition, null);
            GeoPoint activityMarkerPosition = (GeoPoint)projection.fromPixels(
                    point.x,
                    point.y + currentMarker.getIcon().getIntrinsicHeight());
            activityMarker.setPosition(activityMarkerPosition);
            activityMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        }
    }

    private void setupPaintStyles() {
        final Paint paintCommonFill = new Paint();
        paintCommonFill.setStyle(Paint.Style.FILL);
        paintCommonFill.setAlpha(63); // 63 = 75% transparent  [0, 255]
        final PorterDuff.Mode mode = PorterDuff.Mode.SRC_IN;
        paintNegative = new Paint(paintCommonFill);
        paintNegative.setColorFilter(
                new PorterDuffColorFilter(res.getColor(R.color.polarity_negative, theme), mode));
        paintNeutral = new Paint(paintCommonFill);
        paintNeutral.setColorFilter(
                new PorterDuffColorFilter(res.getColor(R.color.polarity_neutral, theme), mode));
        paintPositive = new Paint(paintCommonFill);
        paintPositive.setColorFilter(
                new PorterDuffColorFilter(res.getColor(R.color.polarity_positive, theme), mode));
        paintCommonStroke = new Paint();
        paintCommonStroke.setStyle(Paint.Style.STROKE);
        paintCommonStroke.setStrokeWidth(0);
        paintCommonStroke.setColor(Color.TRANSPARENT);
    }

    private void setupDataObservers(final MapViewModel mapViewModel) {
        // SENTIMENT
        mapViewModel.getPolarityAll().observe(this, lc -> {
            if (locationGeoPoint != null) {
                int[] currentValenceRes = Polarity.polarityToResources(Reasoning.currentEmotionalValence(lc));
                Drawable currentValenceIcon = res.getDrawable(currentValenceRes[2], theme);
                currentValenceIcon.setColorFilter(
                        new PorterDuffColorFilter(
                                res.getColor(currentValenceRes[0], theme),
                                currentValenceRes[0] == android.R.color.transparent
                                        ? PorterDuff.Mode.OVERLAY
                                        : PorterDuff.Mode.SRC_IN));
                currentMarker.setIcon(currentValenceIcon);
                currentMarker.setTitle(res.getString(currentValenceRes[1]).toUpperCase());
                currentMarker.setSnippet(locationGeoPoint.getLatitude() + ", " + locationGeoPoint.getLongitude());
                currentMarker.setSubDescription(res.getString(R.string.map_current_location));
                currentMarker.setPosition(locationGeoPoint);
                currentMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                currentMarker.closeInfoWindow();
                mapView.getOverlays().remove(currentMarker);
                mapView.getOverlays().add(currentMarker);
                mapView.invalidate();
                Log.d(TAG, "Sentiment report: " + res.getString(currentValenceRes[1]));
            }
        });
        // ACTIVITY
        mapViewModel.getActivityLast(1).observe(this, lc -> {
            setActivityMarkerPositionRelativeToCurrent();
            if (lc.size() > 0) { // Known
                /*
                 * The select for the last activity order by id desc (reversed):
                 *  0 = activity elapsed
                 *  1 = activity transition
                 *  2 = activity type
                 */
                if (lc.get(1).sensorValue.intValue() == ActivityTransition.ACTIVITY_TRANSITION_ENTER) {
                    final int[] r = ActivityService.activityToResources(lc.get(2).sensorValue.intValue());
                    activityMarker.setIcon(res.getDrawable(r[1], theme));
                    activityMarker.setTitle(res.getString(r[0]));
                }
            } else { // Unknown
                activityMarker.setIcon(res.getDrawable(R.drawable.ic_question_mark, theme));
                activityMarker.setTitle(res.getString(R.string.dashboard_you_activity_unknown));
            }
            activityMarker.setSubDescription(res.getString(R.string.map_current_activity));
            activityMarker.closeInfoWindow();
            Log.d(TAG, "current activity: " + activityMarker.getTitle());
            mapView.getOverlays().remove(activityMarker);
            mapView.getOverlays().add(activityMarker);
            mapView.invalidate();
        });
    }

    private boolean isCacheAvailable(Calendar now) {
        Log.d(TAG, "isCacheAvailable mapPredictionsCache.size() = " + mapPredictionsCache.size());
        if (mapPredictionsCache.size() > 0) {
            if (now.get(Calendar.HOUR_OF_DAY) == mapPredictionsCacheTimestamp.get(Calendar.HOUR_OF_DAY) // Same hour
                    && (now.getTimeInMillis() - mapPredictionsCacheTimestamp.getTimeInMillis()) < DateTime.HOUR_MS){
                return true;
            } else { // Invalidate
                mapPredictionsCache.clear();
                return false;
            }
        } else {
            return false;
        }
    }

    private void updateMapPredictions(Location location)
            throws Exception {
        // LOCATION to GeoPoint in Map
        if (locationGeoPoint == null) {
            locationGeoPoint = new GeoPoint(location.getLatitude(), location.getLongitude());
        } else {
            locationGeoPoint.setCoords(location.getLatitude(), location.getLongitude());
        }
        Log.d(TAG, "updateMapPredictions locationGeoPoint: " + locationGeoPoint);
        runOnUiThread(() -> {
            mapController.setCenter(locationGeoPoint);
            mapView.invalidate();
        });
        // PREPARE REQUEST
        final JSONArray contextData = new JSONArray();
        final List<MapPrediction> contextCache = new ArrayList<>();
        final Calendar now = GregorianCalendar.getInstance();
        boolean isCacheValid = isCacheAvailable(now);
        final String timestamp = (String)DateTime.format(DateTime.MILLISECONDS_TIMESTAMP_PATTERN, now.getTimeInMillis());
        final BoundingBox bb = mapView.getBoundingBox();
        GeoPoint bbOrigin = new GeoPoint(bb.getLatNorth(), bb.getLonWest())
                .destinationPoint(3.0, 135.0); // 3 m inside BB NW point to be safe
        GeoPoint requestGeoPoint = bbOrigin;
        Log.d(TAG, "updateMapPredictions BB Origin: " + bbOrigin + " | requestGeoPoint: " + requestGeoPoint);
        // Transverse the visible map jumping each MGRS_CELL_SIZE meters right and down (zig zag pattern)
        boolean insideMap = bb.contains(requestGeoPoint);
        while (insideMap) { // Latitude
            while (insideMap) { // Longitude
                MapPrediction mp = new MapPrediction(requestGeoPoint, MGRSRef.PRECISION_1000M); // Without polygon
                // Manage cache: all or nothing!
                Log.d(TAG, "isCacheValid: " + isCacheValid);
                if (isCacheValid) {
                    final MapPrediction mpCached = mapPredictionsCache.get(mp.getMGRSId());
                    if (mpCached != null) {
                        mp = mpCached; // With cached polygon
                    } else {
                        mapPredictionsCache.clear(); // Invalidate cache
                        isCacheValid = false;
                        Log.d(TAG, "mapPredictionsCache CLEARED!");
                    }
                }
                contextCache.add(mp);
                Log.d(TAG, "contextCache mp: " + mp);
                // Add point to the request
                contextData.put(new JSONObject()
                        .put("moment", timestamp)
                        .put("latitude", requestGeoPoint.getLatitude())
                        .put("longitude", requestGeoPoint.getLongitude())
                );
                requestGeoPoint = requestGeoPoint.destinationPoint(MGRS_CELL_SIZE, 90); // Right
                insideMap = bb.contains(requestGeoPoint) || // Include cells with points 10% offset the visible box
                        bb.contains(requestGeoPoint.destinationPoint(MGRS_CELL_SIZE / 10.0, 270));
            }
            bbOrigin = bbOrigin.destinationPoint(MGRS_CELL_SIZE, 180); // Down to next tile row
            requestGeoPoint = bbOrigin;
            insideMap = bb.contains(requestGeoPoint);
        }
        JSONArray contextDataResult = null;
        // MAKE Expanse REQUEST
        if (!isCacheValid) {
            Log.d(TAG, "contextData request (size=" + contextData.length() + "):" +
                    contextData.toString(4));
            contextDataResult = expanseSync.syncPrediction(
                    this, false, contextData, true, false);
            Log.d(TAG, "contextData result (size=" +
                    (contextDataResult == null ? "0" : contextDataResult.length()) + "):" +
                    (contextDataResult == null ? "null" : contextDataResult.toString(4)));
        }
        // PROCESS RESULT from Request OR from CACHE
        final JSONArray responseData = contextDataResult;
        final boolean isFromCache = isCacheValid;
        runOnUiThread(() -> {
            try {
                InfoWindow.closeAllInfoWindowsOn(mapView);
                // Clear polygons
                mapView.getOverlays().removeAll(predictionPolygons);
                mapView.getOverlays().remove(currentMarker);
                mapView.getOverlays().remove(activityMarker);
                predictionPolygons.clear();
                // Set polygons
                if (isFromCache) { // Cached
                    setPredictionOverlays(contextCache);
                } else if (responseData != null) { // Requested
                    setPredictionOverlays(contextCache, responseData, now);
                } else { // Expanse ERROR
                    (new Handler()).post(() ->
                            Toast.makeText(getApplicationContext(),
                                    R.string.map_no_expanse, Toast.LENGTH_SHORT).show());
                    Log.w(TAG, "Expanse returned NULL. Hint: check NETWORK and server!");
                }
                // Polygons to map
                mapView.getOverlays().addAll(predictionPolygons);
                mapView.getOverlays().add(currentMarker);
                mapView.getOverlays().add(activityMarker);
                mapView.invalidate();
                if (isFromCache || responseData != null) {
                    (new Handler()).post(() ->
                            Toast.makeText(getApplicationContext(),
                                    res.getString(R.string.map_prediction_data, predictionPolygons.size()),
                                    Toast.LENGTH_SHORT).show());
                    Log.d(TAG, "Predictions report total: " + predictionPolygons.size());
                }
            } catch (JSONException e) {
                Log.e(TAG, "JSON result GET data FAILED!", e);
            } catch (Exception e) {
                Log.e(TAG, "Unknown FAILURE during Map polygons!", e);
            }
        });
    }

    private void setPredictionOverlays(List<MapPrediction> mapPredictions) {
        for (final MapPrediction mp : mapPredictions) {
            final Polygon polygon = mp.getPolygon();
            if (polygon != null) { // Null means NO prediction but cached!
                predictionPolygons.add(polygon);
            }
        }
    }

    private void setPredictionOverlays(List<MapPrediction> mapPredictions, JSONArray responseData, Calendar now)
            throws JSONException {
        final String moment = (String)DateTime.format(DateTime.DATE_TIME_PATTERN, now.getTimeInMillis());
        int gpIndex = 0;
        for (final MapPrediction mp : mapPredictions) {
            final JSONObject result = responseData.optJSONObject(gpIndex); // point prediction
            if (result != null) {
                final String pEstimator = result.keys().next(); // Estimator name
                final JSONObject p = result.getJSONObject(pEstimator); // Estimator prediction
                final double pScore = p.getDouble("score");
                final JSONArray pClasses = p.getJSONArray("prediction_classes");
                final JSONArray pProbabilities = p.getJSONArray("prediction").getJSONArray(0);
                // Extract the values from JSON in order to process and get the maximum
                double[] predictionProbabilities = new double[pProbabilities.length()];
                for (int i = 0; i < pClasses.length(); i++) {
                    predictionProbabilities[i] = pProbabilities.getDouble(i);
                }
                int maxIndex = Calc.argMax(predictionProbabilities);
                int prediction = pClasses.getInt(maxIndex);
                double probability = predictionProbabilities[maxIndex];
                // Process point, get polygon
                final Polygon polygon = getMapValencePolygon(mp.getGeoPoint(), prediction, probability, moment, pScore);
                predictionPolygons.add(polygon);
                mp.setPolygon(polygon); // to update cache
            }
            updateCache(mp);
            gpIndex++;
        }
        mapPredictionsCacheTimestamp = now;
    }

    private void updateCache(MapPrediction mp) {
        final MapPrediction mpToCache = new MapPrediction(mp);
        mapPredictionsCache.put(mpToCache.getMGRSId(), mpToCache);
        Log.d(TAG, "updateCache mpToCache: " + mpToCache);
    }

    private Polygon getMapValencePolygon(GeoPoint geoPoint,
                                         int valenceClass,
                                         double probability,
                                         String moment,
                                         double score) {
        final Polygon polygon = new Polygon(mapView); // mapView required in order to support InfoWindow
        int[] currentValenceRes = Polarity.polarityToResources(valenceClass);
        switch (valenceClass) {
            case Polarity.NEGATIVE_CLASS:
                polygon.getFillPaint().set(paintNegative);
                break;
            case Polarity.NEUTRAL_CLASS:
                polygon.getFillPaint().set(paintNeutral);
                break;
            case Polarity.POSITIVE_CLASS:
                polygon.getFillPaint().set(paintPositive);
                break;
        }
        polygon.getOutlinePaint().set(paintCommonStroke);
        // GeoPoint to MGRS cell precision 1km square side area
        final MGRSRef mgrsRef = new LatLng(geoPoint.getLatitude(), geoPoint.getLongitude()).toMGRSRef();
        final LatLng mgrsRefNE = mgrsRef.toLatLng(); // MGRS NE coordinates from given geoPoint
        final GeoPoint mgrsRefNEPoint = new GeoPoint(mgrsRefNE.getLatitude(), mgrsRefNE.getLongitude());
        final GeoPoint mgrsRefSWPoint = mgrsRefNEPoint.destinationPoint(
                Math.sqrt(2 * Math.pow(MGRS_CELL_SIZE, 2)), 225); // NE to SW
        polygon.setPoints(new ArrayList<GeoPoint>(5){{
            add(mgrsRefNEPoint);
            add(mgrsRefNEPoint.destinationPoint(MGRS_CELL_SIZE, 180)); // SE
            add(mgrsRefSWPoint);
            add(mgrsRefSWPoint.destinationPoint(MGRS_CELL_SIZE, 0)); // NW
            add(mgrsRefNEPoint); // close
        }});
        polygon.setTitle(res.getString(currentValenceRes[1]).toUpperCase());
        polygon.setSnippet(res.getString(R.string.map_valence_polygon_snippet,
                PROBABILITY_PATTERN.format(probability), moment, mgrsRef.toString(MGRSRef.PRECISION_1000M)));
        polygon.setSubDescription(res.getString(R.string.map_valence_polygon, SCORE_PATTERN.format(score)));
        return polygon;
    }
}
