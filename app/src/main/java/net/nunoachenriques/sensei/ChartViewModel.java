/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import net.nunoachenriques.sensei.emotion.Polarity;
import net.nunoachenriques.sensei.persistence.Collected;
import net.nunoachenriques.sensei.persistence.DataCache;

import java.util.List;

/**
 * The chart (information up-to-date) view model. Gets live data from
 * the collected human emotion valence reports, analysis, and social that's
 * useful for plotting in a chart.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see AndroidViewModel
 */
public final class ChartViewModel
        extends AndroidViewModel {

    private final LiveData<List<Collected>> polarityAll;

    public ChartViewModel(@NonNull Application a) {
        super(a);
        polarityAll = DataCache.getInstance(a.getApplicationContext()).getDao()
                .getCollectedByAttributeLive(Polarity.SENSOR_ATTR_POLARITY_COMMON);
    }

    LiveData<List<Collected>> getPolarityAll() {
        return polarityAll;
    }
}
