/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Pair;
import android.util.SparseArray;
import android.util.SparseLongArray;

import androidx.annotation.Nullable;

import net.nunoachenriques.sensei.persistence.Collected;
import net.nunoachenriques.sensei.persistence.DataCache;
import net.nunoachenriques.sensei.persistence.DataCacheDao;
import net.nunoachenriques.sensei.utility.Calc;
import net.nunoachenriques.sensei.utility.DateTime;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * The device hardware sensors by android feed listener.
 * Catches data from listening sensors, processes the data using
 * {@link SensorsFilter} and stores it.
 * Deals with regular feed sensors and one-shot trigger sensors.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see SensorEvent
 * @see TriggerEvent
 * @see SensorsFilter
 */
public final class Sensors
        extends TriggerEventListener
        implements SensorEventListener {

    private static final String TAG = "Sensors";
    private static final int CACHE_CAPACITY_DEFAULT = 1000;
    private static final double CACHE_TO_STORAGE_THRESHOLD_DEFAULT = 0.8;

    private static volatile Sensors INSTANCE;
    private static volatile SparseLongArray lastTime;
    private static volatile Map<String, Float> lastValue;

    private final SharedPreferences sharedPreferences;
    private final DataCacheDao dataCacheDao;
    private final SensorManager sensorManager;
    private final SensorsFilter sensorsFilter;

    private ExecutorService executorService;
    private int cacheCapacity;
    private int cacheToStorageThreshold;
    private final SparseArray<Pair<Sensor, Integer>> sensorsListening; // key: sensor type id, pair: (sensor, rate)
    private final SparseArray<List<Collected>> sensorsCollected;

    private Sensors(Context c) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        resetCacheAndThreshold();
        executorService = Executors.newSingleThreadExecutor();
        sensorManager = (SensorManager)c.getSystemService(Context.SENSOR_SERVICE);
        lastTime = new SparseLongArray();
        lastValue = new HashMap<>();
        sensorsFilter = SensorsFilter.getInstance(c);
        sensorsListening = new SparseArray<>(sensorsFilter.getSensorsCount());
        sensorsCollected = new SparseArray<>(sensorsFilter.getSensorsCount());
        dataCacheDao = DataCache.getInstance(c).getDao();
    }

    /**
     * Gets an instance of this class. If no single instance is available, i.e.,
     * {@code null}, then creates and returns a single new one
     * (Singleton pattern).
     *
     * @param c Android application {@link Context}.
     * @return The single instance of this class.
     */
    public static Sensors getInstance(Context c) {
        if (INSTANCE == null) {
            synchronized (Sensors.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Sensors(c);
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Gets the current values from preferences and resets cache and cache
     * threshold.
     */
    private void resetCacheAndThreshold() {
        String cc = sharedPreferences.getString("in_memory_cache_capacity", null);
        if (cc == null) {
            sharedPreferences.edit().putString("in_memory_cache_capacity", String.valueOf(CACHE_CAPACITY_DEFAULT)).apply();
            cacheCapacity = CACHE_CAPACITY_DEFAULT;
        } else {
            cacheCapacity = Integer.parseInt(cc);
        }
        double thresholdPercentage;
        String tp = sharedPreferences.getString("in_memory_cache_capacity_before_storage", null);
        if (tp == null) {
            sharedPreferences.edit().putString("in_memory_cache_capacity_before_storage", String.valueOf(CACHE_TO_STORAGE_THRESHOLD_DEFAULT)).apply();
            thresholdPercentage = CACHE_TO_STORAGE_THRESHOLD_DEFAULT;
        } else {
            thresholdPercentage = Double.parseDouble(tp);
        }
        if (thresholdPercentage > 0.9) {
            thresholdPercentage = 0.9;
        } else if (thresholdPercentage < 0.1) {
            thresholdPercentage = 0.1;
        }
        cacheToStorageThreshold = Calc.round(thresholdPercentage * cacheCapacity);
    }

    /**
     * Gets the memory cache capacity for each sensor.
     *
     * @return Number of items to cache in memory for each sensor.
     */
    private int getCacheCapacity() {
        return cacheCapacity;
    }

    /**
     * Gets the cache threshold before writing to persistent storage.
     *
     * @return Number of items in memory cache per sensor before writing to storage.
     */
    private int getCacheToStorageThreshold() {
        return cacheToStorageThreshold;
    }

    /**
     * Sets the sensors to listen from a list of sensor types, rate and
     * max report latency values.
     *
     * @see SensorsFilter#getSensorsOfInterest()
     */
    void setAllSensors() {
        for (int id : sensorsFilter.getSensorsOfInterest()) {
            setSensor(id);
        }
        Log.d(TAG, "SET all!   LISTENING: " + sensorsListening.size());
    }

    private void setSensor(int id) {
        if (sensorsFilter.isEnabled(id)) {
            Sensor s = sensorManager.getDefaultSensor(id);
            if (s == null) {
                Log.d(TAG, "SENSOR of type [" + id + "] NOT AVAILABLE!");
            } else {
                int r = sensorsFilter.getRate(id);
                int l = sensorsFilter.getLatency(id);
                if (r != SensorsFilter.ERROR_INT
                        && l != SensorsFilter.ERROR_INT
                        && !listen(s, r, l)) {
                    Log.e(TAG, "listen FAILED ON SENSOR [" + s.getStringType() + "]! Hint: check rate and latency in preferences.");
                }
            }
        } else {
            Log.d(TAG, "Sensor [" + id + "] is DISABLED in preferences!");
        }
    }

    /**
     * Listens to a specific {@link Sensor}. Notice that a FIFO queue is used
     * with {@code maxReportLatencyUs} of several (e.g., 10) times {@code r}.
     * Deals with regular feed sensors and one-shot trigger sensors.
     *
     * @param s Sensor to listen to.
     * @param r Data rate in microseconds or special constant 0, 1, 2, 3.
     * @param l Max report latency in microseconds (e.g., 1000000 us = 1 s).
     * @return True if sensor supported and enabled.
     * @see SensorManager#requestTriggerSensor(TriggerEventListener, Sensor)
     * @see SensorManager#registerListener(SensorEventListener, Sensor, int, int)
     */
    private boolean listen(Sensor s, int r, int l) {
        boolean listening;
        if (s.getReportingMode() == Sensor.REPORTING_MODE_ONE_SHOT) {
            listening = sensorManager.requestTriggerSensor(this, s);
        } else {
            listening = sensorManager.registerListener(this, s, r, l);
        }
        Log.d(TAG, "listening [" + listening + "] sensor: " + s.getType());
        if (listening) {
            sensorsListening.append(s.getType(), Pair.create(s, r));
            if (sensorsCollected.get(s.getType()) == null) {
                sensorsCollected.append(s.getType(), new ArrayList<>(getCacheCapacity()));
            }
        }
        return listening;
    }

    /**
     * Stops listening for all registered sensors.
     * Deals with regular feed sensors and one-shot trigger sensors.
     * It DOES NOT care flushing sensors' FIFO queues.
     * Although it flushes in memory cache data to storage.
     */
    void unsetAllSensors() {
        Log.d(TAG, "UNSET all | START    | LISTENING: " + sensorsListening.size());
        sensorManager.cancelTriggerSensor(this, null);
        sensorManager.unregisterListener(this);
        // Possible concurrent race fixed
        onSensorChangedExecutorCleanRestart();
        // Force flush all listening before clear
        for (int soi : sensorsFilter.getSensorsOfInterest()) {
            final Pair<Sensor, Integer> sip = sensorsListening.get(soi);
            if (sip != null) {
                executorService.execute(() -> cacheToStorage(sip.first, true));
            }
        }
        sensorsListening.clear();
        Log.d(TAG, "UNSET all | FINISHED | LISTENING: " + sensorsListening.size());
    }

    private void onSensorChangedExecutorCleanRestart() {
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(100, TimeUnit.MILLISECONDS)) {
                executorService.shutdownNow();
                if (!executorService.awaitTermination(100, TimeUnit.MILLISECONDS)) {
                    Log.w(TAG, "FAILED to shutdown executor after 2x 100 ms waiting!");
                }
            }
        } catch (InterruptedException e) {
            Log.e(TAG, "onSensorChangedExecutorCleanRestart INTERRUPTED: " + e.getMessage());
            executorService.shutdownNow();
        }
        executorService = Executors.newSingleThreadExecutor();
    }

    @Override
    public final void onAccuracyChanged(Sensor s, int i) {
        switch (i) {
            case SensorManager.SENSOR_STATUS_NO_CONTACT:
                Log.d(TAG, String.format("Sensor %s NO CONTACT! Hint: disconnected from device?", s.getStringType()));
                break;
            case SensorManager.SENSOR_STATUS_UNRELIABLE:
                Log.d(TAG, String.format("Sensor %s UNRELIABLE! Hint: calibration or readings needed?", s.getStringType()));
                break;
            case SensorManager.SENSOR_STATUS_ACCURACY_LOW:
                Log.d(TAG, String.format("Sensor %s ACCURACY LOW! Hint: calibration needed?", s.getStringType()));
                break;
            case SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM:
                Log.d(TAG, String.format("Sensor %s ACCURACY MEDIUM!", s.getStringType()));
                break;
            case SensorManager.SENSOR_STATUS_ACCURACY_HIGH:
                Log.d(TAG, String.format("Sensor %s ACCURACY HIGH!", s.getStringType()));
                break;
        }
    }

    @Override
    public final void onSensorChanged(final SensorEvent se) {
        try {
            executorService.execute(() -> processSensorEvent(se.sensor, se.timestamp, se.values, se.accuracy, true));
        } catch (RejectedExecutionException | NullPointerException e) {
            Log.e(TAG, "Service execution FAILED!", e);
        }
    }

    @Override
    public void onTrigger(final TriggerEvent te) {
        try {
            executorService.execute(() -> processSensorEvent(te.sensor, te.timestamp, te.values, null, false));
        } catch (RejectedExecutionException | NullPointerException e) {
            Log.e(TAG, "Service execution FAILED!", e);
        }
    }

    private void processSensorEvent(final Sensor sensor,
                                    final long timestamp,
                                    final float[] values,
                                    @Nullable final Integer accuracy,
                                    boolean regular) {
        final Timestamp ts = DateTime.bootNanosToTimestamp(timestamp);
        final String tz = DateTime.getTimeZoneISO8601();
        final int st = sensor.getType();
        final String sst = sensor.getStringType();
        /*
         * No more than one value from same sensor each sampling period,
         * declared and defined in preferences file.
         */
        final long previousTime = lastTime.get(st);
        final long elapsedNanosSinceLast = timestamp - previousTime;
        final Pair<Sensor, Integer> sensorListening = sensorsListening.get(st);
        if (previousTime == 0 ||
                (sensorListening != null &&
                        elapsedNanosSinceLast >= TimeUnit.MICROSECONDS.toNanos(sensorListening.second))) {
            lastTime.put(st, timestamp);
            int valuesCount = sensorsFilter.getValuesCount(st);
            Collection<Collected> lc = new ArrayList<>(valuesCount + (accuracy == null ? 0 : 1));
            boolean valuesOfInterest = false;
            for (int i = 0; i < valuesCount; i++) {
                /*
                 * Filter the interest in the values. Uses the sensors
                 * preferences of each value. Stores an event only if,
                 * at least, one of the event's values is of interest.
                 * Example: linear acceleration is of interest if one
                 * axis has a value greater than or equal to the
                 * differential threshold regarding the previous value.
                 */
                String k = sensorsFilter.getSensorValueKey(st, i);
                float v = values[i];
                Float previousValue = lastValue.get(k);
                if (previousValue == null
                        || sensorsFilter.isGTEDiffThreshold(st, i, v, previousValue)) {
                    lc.add(new Collected(ts, tz, sst, Integer.toString(i), v, null, false));
                    lastValue.put(k, v);
                    valuesOfInterest = true;
                }
            }
            // Add time threshold to force interest in the data value
            valuesOfInterest =
                    (valuesOfInterest || elapsedNanosSinceLast > Sensei.SENSOR_DATA_MISSING_ELAPSED_NANOS_MAX);
            if (valuesOfInterest) {
                if (accuracy != null) {
                    lc.add(new Collected(ts, tz, sst, "accuracy", (double)accuracy, null, false));
                }
                try {
                    if (!sensorsCollected.get(st).addAll(lc)) {
                        throw new Exception("Collected ADD FAILED!");
                    }
                    if (!cacheToStorage(sensor, false)) {
                        throw new Exception("Collected TO STORAGE FAILED!");
                    }
                    Log.d(TAG, "SENSOR: " + lc.toString());
                } catch (Exception e) {
                    Log.e(TAG, "SENSOR [" + st + "](" + sst + ") DATA DISCARDED! " + e.getMessage());
                }
            } else {
                Log.d(TAG, "SENSOR [" + st + "](" + sst + ") DATA DISCARDED!");
            }
        } else {
            Log.d(TAG, "SENSOR [" + st + "](" + sst + ") DATA DISCARDED (time rate too high)!");
        }
        // (Re)listen trigger sensor. Is canceled on each event (triggered).
        if (!regular) {
            listen(sensor, SensorManager.SENSOR_DELAY_FASTEST, 0);
        }
    }

    private boolean cacheToStorage(Sensor sensor, boolean force) {
        int soi = sensor.getType();
        List<Collected> lc = sensorsCollected.get(soi);
        if (lc != null && lc.size() > 0 && (force || lc.size() >= getCacheToStorageThreshold())) {
            try {
                dataCacheDao.set(lc.toArray(new Collected[0]));
                sensorsCollected.put(soi, new ArrayList<>(getCacheCapacity()));
                Log.d(TAG, "cacheToStorage [" + sensor.getType() + "](" + sensor.getStringType() + ") FLUSHED!");
                return true;
            } catch (Exception e) {
                Log.e(TAG, "cacheToStorage [" + sensor.getType() + "](" + sensor.getStringType() + ") FAILED!", e);
                return false;
            }
        }
        return true;
    }
}
