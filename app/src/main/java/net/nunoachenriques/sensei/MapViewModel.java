/*
 * Copyright 2020 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import net.nunoachenriques.sensei.emotion.Polarity;
import net.nunoachenriques.sensei.persistence.Collected;
import net.nunoachenriques.sensei.persistence.DataCache;
import net.nunoachenriques.sensei.persistence.DataCacheDao;

import java.util.List;

/**
 * The predictions map (information up-to-date) view model. Gets live data from
 * specific sources (e.g., collected from sensors) of interest to present.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see AndroidViewModel
 */
public final class MapViewModel
        extends AndroidViewModel {

    private final DataCacheDao dataCacheDao;

    public MapViewModel(@NonNull Application a) {
        super(a);
        final Context c = a.getApplicationContext();
        dataCacheDao = DataCache.getInstance(c).getDao();
    }

    @SuppressWarnings("SameParameterValue")
    LiveData<List<Collected>> getActivityLast(int i) {
        return dataCacheDao
                .getCollectedLastLive(ActivityFeed.SENSOR_ID, i * ActivityFeed.SENSOR_FIXED_ATTRS);
    }

    LiveData<List<Collected>> getPolarityAll() {
        return dataCacheDao.getCollectedByAttributeLive(Polarity.SENSOR_ATTR_POLARITY_COMMON);
    }
}
