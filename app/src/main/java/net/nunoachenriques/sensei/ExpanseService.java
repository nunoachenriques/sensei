/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import net.nunoachenriques.sensei.persistence.DataCache;
import net.nunoachenriques.sensei.utility.Availability;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * The expanse {@link JobService} which manages all about the data sync
 * processing between local and expanse resources.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class ExpanseService
        extends JobService {

    private static final String TAG = "ExpanseService";
    private static volatile ExecutorService executorService = Executors.newSingleThreadExecutor();

    @Override
    public boolean onStartJob(final JobParameters jp) {
        if (Availability.isAtRest(getApplicationContext())) {
            try {
                if (executorService.isShutdown()) {
                    executorService = Executors.newSingleThreadExecutor();
                }
                executorService.execute(() -> {
                    final Context c = getApplicationContext();
                    final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(c);
                    final String batchSize = sp.getString("sync.batch", null);
                    ExpanseSync.getInstance(DataCache.getInstance(c).getDao()).sync(
                            c,
                            sp.getBoolean(c.getResources().getString(R.string.data_wifi_pref_key), true),
                            (batchSize != null) ? Integer.parseInt(batchSize) : 1000
                    );
                    jobFinished(jp, false);
                });
                return true; // True if async processing. Wait for jobFinished()
            } catch (Exception e) {
                Log.e(TAG, "onStartJob [" + jp.getJobId() + "] FAILED: " + e.getMessage());
            }
        }
        return false; // False if finished (no need to call jobFinished())
    }

    @Override
    public boolean onStopJob(JobParameters jp) {
        Log.w(TAG, "onStopJob JobService [" + jp.getJobId() + "] INTERRUPTING...");
        executorService.shutdown();
        // One more chance (wait 200 ms) for sync service to gracefully stop
        try {
            executorService.awaitTermination(200, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            Log.e(TAG, "onStopJob awaitTermination INTERRUPTED: " + e.getMessage());
        }
        jobFinished(jp, false);
        Log.w(TAG, "onStopJob [" + jp.getJobId() + "] INTERRUPTED!");
        return false; // False to drop the job. True to reschedule.
    }
}
