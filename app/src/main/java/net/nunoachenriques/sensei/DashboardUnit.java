/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * The Dashboard unit data place holder.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
final class DashboardUnit {

    /*
     * The Dashboard unit possible types.
     * Memory optimization: change enum to primitive type int.
     */
    @IntDef({TITLE_PROGRESS_IMAGE, TITLE_VALUE_HISTOGRAM, TITLE_VALUE_IMAGE, TITLE_VALUE_PROGRESS})
    @Retention(RetentionPolicy.SOURCE)
    private @interface DashboardUnitType {}

    static final int TITLE_PROGRESS_IMAGE = 0;
    static final int TITLE_VALUE_HISTOGRAM = 1;
    static final int TITLE_VALUE_IMAGE = 2;
    static final int TITLE_VALUE_PROGRESS = 3;

    private final int id;
    private final @DashboardUnitType int type;

    /**
     * Constructor sets all values.
     *
     * @param i Unique identifier (e.g., R.id.unique_name).
     * @param t Unit type.
     */
    DashboardUnit(int i, @DashboardUnitType int t) {
        id = i;
        type = t;
    }

    /**
     * Gets a unique identifier for this unit.
     *
     * @return Unit unique identifier.
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the type of this unit.
     *
     * @return One of the possible types for a unit.
     */
    public @DashboardUnitType int getType() {
        return type;
    }
}
