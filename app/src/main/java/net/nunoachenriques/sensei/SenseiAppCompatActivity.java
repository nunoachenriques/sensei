/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

/**
 * The application super class from which all activities should inherit.
 * Deals with the changing theme. {@link #onCreate(Bundle)} sets the theme based
 * on the dark theme preference key value: {@code true} for dark, {@code false}
 * for light (default).
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public class SenseiAppCompatActivity
        extends AppCompatActivity {

    private @StyleRes int currentThemeStyleId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false); // once
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        currentThemeStyleId = sp.getBoolean(getResources().getString(R.string.app_theme_dark_pref_key), false)
                ? R.style.SenseiDark
                : R.style.SenseiLight;
        setTheme(currentThemeStyleId);
        super.onCreate(savedInstanceState);
    }

    /**
     * Gets the {@link StyleRes} integer identifier that the current
     * {@link Resources.Theme} is using (e.g., R.style.SenseiDark).
     *
     * @return {@link StyleRes} integer identifier used by the current
     *         {@link Resources.Theme}.
     * @see "res/values/styles.xml"
     */
    @StyleRes int getCurrentThemeStyleId() {
        return currentThemeStyleId;
    }

    /**
     * Checks if the current theme set is dark opposite to light.
     *
     * @return True if dark theme, false otherwise.
     */
    boolean isCurrentThemeDark() {
        return (currentThemeStyleId == R.style.SenseiDark);
    }
    /**
     * Sets the action bar (toolbar at the top of the activity) with the back
     * to parent activity arrow. Only if support action bar is available.
     */
    void setSupportActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
