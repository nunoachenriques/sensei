/*
 * Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.settings;

import android.content.SharedPreferences;

import androidx.lifecycle.LiveData;

/**
 * A {@link SharedPreferences} preference as an observable {@link LiveData}.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public abstract class LivePreference<T>
        extends LiveData<T> {

    protected final SharedPreferences sharedPreferences;

    private final String key;
    private final T defaultValue;

    LivePreference(SharedPreferences prefs, String key, T defaultValue) {
        this.sharedPreferences = prefs;
        this.key = key;
        this.defaultValue = defaultValue;
    }

    private final SharedPreferences.OnSharedPreferenceChangeListener preferenceChangeListener =
            new SharedPreferences.OnSharedPreferenceChangeListener() {
                @Override
                public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
                    if (LivePreference.this.key.equals(key)) {
                        setValue(getValueFromPreferences(key, defaultValue));
                    }
                }
            };

    abstract T getValueFromPreferences(String key, T defaultValue);

    @Override
    protected void onActive() {
        super.onActive();
        setValue(getValueFromPreferences(key, defaultValue));
        sharedPreferences.registerOnSharedPreferenceChangeListener(preferenceChangeListener);
    }

    @Override
    protected void onInactive() {
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(preferenceChangeListener);
        super.onInactive();
    }
}
