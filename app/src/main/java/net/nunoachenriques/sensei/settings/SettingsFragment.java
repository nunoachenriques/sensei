/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.settings;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import net.nunoachenriques.sensei.R;
import net.nunoachenriques.sensei.utility.DatePreference;
import net.nunoachenriques.sensei.utility.DatePreferenceDialogFragment;

/**
 * The app preferences {@link Fragment} to feed the configuration.
 * Deals with checking user input on changing preferences and before persist.
 * Moreover, changes preference interface summary updating with the new value.
 * The general sequence for a preference: UI input value -> before persist
 * (e.g., check values) -> persist -> after persist (e.g., update summary).
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see SettingsActivity
 * @see "res/xml/preferences.xml"
 * @see Preference.OnPreferenceChangeListener
 * @see SharedPreferences.OnSharedPreferenceChangeListener
 */
public final class SettingsFragment
        extends PreferenceFragmentCompat
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = "SettingsFragment";

    private Resources res;
    private SharedPreferences sharedPreferences;

    public SettingsFragment() {}

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        res = getResources();
        addPreferencesFromResource(R.xml.preferences);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext());
        // Set summary for all except switch-type key
        for (String k : sharedPreferences.getAll().keySet()) {
            if (!k.equals(res.getString(R.string.app_theme_dark_pref_key))) { // Avoid theme switch setting
                onSharedPreferenceChanged(sharedPreferences, k);
            }
        }
    }

    @Override
    public void onDisplayPreferenceDialog(Preference preference) {
        if (preference instanceof DatePreference) {
            final DialogFragment df;
            df = DatePreferenceDialogFragment.newInstance(preference.getKey());
            df.setTargetFragment(this, 0);
            df.show(getParentFragmentManager(), null);
        } else {
            super.onDisplayPreferenceDialog(preference);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {  // AFTER persist!
        if (key.equals(res.getString(R.string.app_theme_dark_pref_key))) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.recreate();
            }
        } else if (key.equals(res.getString(R.string.human_name_pref_key))) {
            setSummaryWithValue(sp, key);
        } else if (key.equals(res.getString(R.string.human_birthday_pref_key))) {
            setSummaryWithValue(sp, key);
        } else if (key.equals(res.getString(R.string.human_gender_pref_key))) {
            setSummaryWithCurrentListEntry(key);
        } else if (key.equals(res.getString(R.string.human_email_pref_key))) {
            setSummaryWithValue(sp, key);
        } else if (key.equals(res.getString(R.string.human_entity_pref_key))) {
            setSummaryWithValue(sp, key, R.string.human_entity_pref_summary);
        } else if (key.equals(res.getString(R.string.app_agreement_date_pref_key))) {
            setSummaryWithValue(sp, key, R.string.app_agreement_date_pref_summary);
        } else if (key.equals(res.getString(R.string.source_twitter_screen_name_pref_key))) {
            setSummaryWithValue(sp, key, R.string.source_twitter_pref_summary);
        }
    }

    private void setSummaryWithValue(SharedPreferences sp, String key) {
        setSummaryWithValue(sp, key, null);
    }

    private void setSummaryWithValue(SharedPreferences sp, String key, Integer summary) {
        String s = sp.getString(key, "");
        if (s.length() == 0) {
            if (summary != null) {
                s = res.getString(summary);
            } else {
                return;
            }
        }
        Preference p = findPreference(key);
        if (p != null) {
            p.setSummary(s);
            Log.d(TAG, "Preference [" + key + "] summary set with [" + s + "]");
        }
    }

    private void setSummaryWithCurrentListEntry(CharSequence key) {
        ListPreference lp = findPreference(key);
        if (lp != null) {
            lp.setSummary(lp.getEntry());
            Log.d(TAG, "Preference [" + key + "] summary set with [" + lp.getEntry() + "]");
        }
    }
}
