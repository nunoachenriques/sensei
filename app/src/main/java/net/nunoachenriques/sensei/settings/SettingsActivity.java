/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.settings;

import android.os.Bundle;

import net.nunoachenriques.sensei.SenseiAppCompatActivity;

/**
 * An activity that presents a set of application settings.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see <a href="http://developer.android.com/design/patterns/settings.html">Android Design: Settings</a>
 * @see <a href="http://developer.android.com/guide/topics/ui/settings.html">Settings API Guide</a>
 * @see SettingsFragment
 */
public final class SettingsActivity
        extends SenseiAppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
}
