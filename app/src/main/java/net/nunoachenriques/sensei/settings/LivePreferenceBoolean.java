/*
 * Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.settings;

import android.content.SharedPreferences;

/**
 * A {@link LivePreference} of type {@link Boolean}.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class LivePreferenceBoolean
        extends LivePreference<Boolean> {

    public LivePreferenceBoolean(SharedPreferences sp, String key, Boolean defaultValue) {
        super(sp, key, defaultValue);
    }

    @Override
    Boolean getValueFromPreferences(String key, Boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }
}
