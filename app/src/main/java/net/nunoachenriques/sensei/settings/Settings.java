/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.settings;

import android.content.SharedPreferences;

/**
 * Settings converter utility for the application persisted shared preferences.
 * Refer to the {@code PreferenceScreen} with key {@code parameters}
 * in ({@code res/xml/preferences.xml}).
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see SharedPreferences
 */
public final class Settings {

    private Settings() {}

    /**
     * Puts a {@code double} value specified by the {@code key} in the shared
     * preferences.
     *
     * @param sp The shared preferences loaded.
     * @param key The shared preferences key ({@code android:key}).
     * @param v The {@code double} value to store.
     */
    public static void putDouble(SharedPreferences sp, String key, double v) {
        sp.edit().putString(key, String.valueOf(v)).apply();
    }

    /**
     * Gets the shared preferences value as a {@code double} specified by the
     * {@code key}.
     *
     * @param sp The shared preferences loaded.
     * @param key The shared preferences key ({@code android:key}).
     * @return The value as a {@code double}, {@link Double#NaN} if non-existent.
     * @see Double#isNaN(double)
     */
    public static double getDouble(SharedPreferences sp, String key) {
        String v = sp.getString(key, null);
        return (v != null ? Double.parseDouble(v) : Double.NaN);
    }
}
