/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.TextView;

import net.nunoachenriques.sensei.persistence.CollectedDataStats;
import net.nunoachenriques.sensei.persistence.CollectedDataStatsSensor;
import net.nunoachenriques.sensei.persistence.CollectedMomentValue;
import net.nunoachenriques.sensei.persistence.DataCache;
import net.nunoachenriques.sensei.persistence.DataCacheDao;
import net.nunoachenriques.sensei.utility.DateTime;

import java.text.DecimalFormat;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Collected data statistics information in a view updated each 3 seconds.
 * Useful for debugging purposes.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class DataStatsActivity
        extends SenseiAppCompatActivity {

    private static final String TAG = "DataStatsActivity";
    private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    private Resources res;
    private SharedPreferences sharedPreferences;
    private DataCacheDao dataCacheDao;
    private TextView dataTextView;
    private ScheduledFuture<?> dataHandle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        res = getResources();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        setContentView(R.layout.data_stats);
        setTitle(R.string.data_stats_title);
        setSupportActionBar();
        setupDataView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        dataHandle = scheduler.scheduleAtFixedRate(new DataStats(), 0, 3, TimeUnit.SECONDS);
    }

    @Override
    protected void onPause() {
        super.onPause();
        dataHandle.cancel(true);
    }

    /*
     *                        D A T A   S T A T S
     */

    private void setupDataView() {
        dataCacheDao = DataCache.getInstance(this).getDao();
        dataTextView = findViewById(R.id.data_stats_text_view);
    }

    private class DataStats
            implements Runnable {
        @Override
        public void run() {
            final StringBuilder sb = new StringBuilder();
            try {
                CollectedDataStats dataStats = dataCacheDao.getCollectedDataStats();
                long lastPause = 0;
                long startDown = 0;
                long sumDown = 0;
                List<CollectedMomentValue> collectedStatus =
                        dataCacheDao.getCollectedMomentValue(FeedService.SENSOR_ID, FeedService.SENSOR_ATTR_STATUS);
                for (CollectedMomentValue status : collectedStatus) {
                    switch ((int)status.value) {
                        case FeedService.STATUS_FEED:
                            if (startDown > 0) {
                                sumDown += (status.moment - startDown);
                                startDown = 0;
                            }
                            break;
                        case FeedService.STATUS_PAUSE:
                            lastPause = status.moment;
                        case FeedService.STATUS_DESTROY:
                            if (startDown == 0) {
                                startDown = status.moment;
                            }
                            break;
                    }
                }
                // On PAUSE restricts screen information
                boolean onPause = sharedPreferences.getBoolean(
                        res.getString(R.string.data_pause_mode_pref_key), false);
                if (onPause) {
                    String userName = sharedPreferences.getString(
                            res.getString(R.string.human_name_pref_key), null);
                    long[] duration = DateTime.durationMillisToDayHourMinute(
                            (System.currentTimeMillis() - lastPause));
                    sb.append(res.getString(R.string.data_stats_on_pause,
                            userName,
                            lastPause, lastPause,
                            duration[0], duration[1], duration[2]));
                } else {
                    // REGULAR information
                    double active = (dataStats.duration > 0)
                            ? ((dataStats.duration - sumDown) / (double)dataStats.duration)
                            : 0;
                    sb.append(res.getString(R.string.data_stats_duration_active,
                            (new DecimalFormat("##0.###%")).format(active)));
                    sb.append(res.getString(R.string.data_stats_total_title));
                    sb.append(res.getString(R.string.data_stats_total_database,
                            dataStats.total,
                            dataStats.total_synced,
                            dataStats.total_waiting));
                    sb.append(res.getString(R.string.data_stats_total_event,
                            dataStats.total_events,
                            dataStats.total_events_synced,
                            dataStats.total_events_waiting));
                    if (ExpanseSync.collectedLastSync.getTime() == ExpanseSync.NO_SYNC) {
                        sb.append(res.getString(R.string.data_stats_last_sync_no));
                    } else {
                        sb.append(res.getString(R.string.data_stats_last_sync,
                                ExpanseSync.collectedLastSync, ExpanseSync.collectedLastSync));
                    }
                    sb.append(res.getString(R.string.data_stats_duration_title));
                    long[] duration = DateTime.durationMillisToDayHourMinute(dataStats.duration);
                    sb.append(res.getString(R.string.data_stats_duration,
                            duration[0], duration[1], duration[2],
                            dataStats.first, dataStats.first,
                            dataStats.last, dataStats.last));
                    sb.append(res.getString(R.string.data_stats_sensor_title));
                    for (CollectedDataStatsSensor dataStatsSensor : dataCacheDao.getCollectedDataStatsSensor()) {
                        sb.append(res.getString(R.string.data_stats_sensor,
                                dataStatsSensor.total,
                                dataStatsSensor.total_events,
                                dataStatsSensor.last, dataStatsSensor.last,
                                dataStatsSensor.name));
                    }
                }
            } catch (Exception e) {
                Log.wtf(TAG, "DataStats INTERRUPTED!", e);
            }
            runOnUiThread(() -> dataTextView.setText(sb.toString()));
            Log.d(TAG, sb.toString());
        }
    }
}
