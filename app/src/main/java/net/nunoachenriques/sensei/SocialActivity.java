/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

/**
 * The social networks linking/unlinking activity.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class SocialActivity
        extends SenseiAppCompatActivity {

    private static final String TAG = "SocialActivity";

    private Resources res;
    private SharedPreferences sharedPreferences;
    private TwitterFeed twitterFeed;
    private TwitterLoginButton twitterLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        res = getResources();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        setContentView(R.layout.social);
        setTitle(R.string.social_title);
        setSupportActionBar();
        setupTwitter();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE) {
            // Pass the activity result to the Twitter login button.
            twitterLoginButton.onActivityResult(requestCode, resultCode, data);
        }
    }

    /*
     *                           T W I T T E R
     */

    private void setupTwitter() {
        TwitterSession session = null;
        try {
            if (twitterFeed == null) { // Initialize Twitter
                twitterFeed = TwitterFeed.getInstance(getApplicationContext());
            }
            session = TwitterCore.getInstance().getSessionManager().getActiveSession();
        } catch (Exception e) {
            Log.e(TAG, "setupTwitter FAILED!", e);
        }
        setupTwitterView(session == null, session);
    }

    private final Callback<TwitterSession> twitterSessionCallback = new Callback<TwitterSession>() {
        @Override
        public void success(Result<TwitterSession> result) {
            Log.i(TAG, "TwitterSession SUCCEED! Screen name: " + result.data.getUserName() + " | id: " + result.data.getUserId());
            sharedPreferences.edit().putString(res.getString(R.string.source_twitter_screen_name_pref_key), result.data.getUserName()).apply();
            setupTwitterView(false, result.data);
        }
        @Override
        public void failure(TwitterException e) {
            Log.w(TAG, "TwitterSession FAILED! " + e.getMessage());
            setupTwitterView(true, null);
        }
    };

    private final View.OnClickListener twitterOffListener = v -> {
        try {
            TwitterFeed.getInstance(getApplicationContext()).shutdown();
            TwitterCore.getInstance().getSessionManager().clearActiveSession();
            Log.i(TAG, "Twitter SESSION CLEARED!");
            setupTwitterView(true, null);
        } catch (Exception e) {
            Log.e(TAG, "TwitterFeed SHUTDOWN FAILED!", e);
        }
    };

    private void setupTwitterView(boolean enableAuth, TwitterSession session) {
        twitterLoginButton = findViewById(R.id.twitter_login_button);
        ImageView twitterOffButton = findViewById(R.id.twitter_off_button);
        if (twitterLoginButton != null && twitterOffButton != null) {
            if (enableAuth) { // ENABLED
                twitterLoginButton.setEnabled(true);
                twitterLoginButton.setText(R.string.tw__login_btn_txt);
                twitterLoginButton.setCallback(twitterSessionCallback);
                twitterOffButton.setVisibility(View.GONE);
                twitterOffButton.setEnabled(false);
                twitterOffButton.setOnClickListener(null);
                twitterOffButton.setClickable(false);
            } else {          // DISABLED
                twitterLoginButton.setEnabled(false);
                twitterLoginButton.setText(session.getUserName());
                twitterOffButton.setVisibility(View.VISIBLE);
                twitterOffButton.setEnabled(true);
                twitterOffButton.setOnClickListener(twitterOffListener);
            }
        }
    }
}
