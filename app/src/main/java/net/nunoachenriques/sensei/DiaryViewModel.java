/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import net.nunoachenriques.sensei.persistence.Chat;
import net.nunoachenriques.sensei.persistence.DataCache;
import net.nunoachenriques.sensei.persistence.DataCacheDao;

import java.util.List;

/**
 * The diary (information up-to-date) view model. Gets live data from
 * the collected human messages from diary input box.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see AndroidViewModel
 */
public final class DiaryViewModel
        extends AndroidViewModel {

    private final DataCacheDao dataCacheDao;
    private final LiveData<List<Chat>> messageAll;

    public DiaryViewModel(@NonNull Application a) {
        super(a);
        dataCacheDao = DataCache.getInstance(a.getApplicationContext()).getDao();
        messageAll = dataCacheDao.getDiaryMessageAll();
    }

    LiveData<List<Chat>> getDiaryMessage(@Nullable String searchToken) {
        return (searchToken == null)
                ? messageAll
                : dataCacheDao.getDiaryMessageMatch(DataCache.getChatMatchQuery(searchToken));
    }
}
