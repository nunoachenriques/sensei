/*
 * Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import androidx.annotation.NonNull;

import net.nunoachenriques.sensei.emotion.Polarity;
import net.nunoachenriques.sensei.persistence.Collected;
import net.nunoachenriques.sensei.utility.DateTime;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

/**
 * Common reasoning algorithms for the agent about the human.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
final class Reasoning {

    private Reasoning() {}

    /**
     * Ad hoc algorithm to assess current emotional valence value from all
     * the collected polarity values (e.g., smile reports, diary messages
     * sentiment analysis).
     *
     * @param collectedPolarity All polarity values collected.
     * @return Current assessed emotional valence value.
     */
    static double currentEmotionalValence(@NonNull List<Collected> collectedPolarity) {
        double polarity = Polarity.NO_POLARITY;
        if (collectedPolarity.size() > 0) {
            // Calculate average within one day
            Timestamp momentMin = new Timestamp(System.currentTimeMillis() - DateTime.DAY_MS);
            Collections.reverse(collectedPolarity);
            Double lastSmileValue = null;
            double polaritySum = 0;
            int polaritySumCount = 0;
            for (Collected collected : collectedPolarity) {
                if (collected.moment.after(momentMin)) {
                    if (lastSmileValue == null
                            && collected.sensorType.equals(SmileReportService.SENSOR_ID)) {
                        lastSmileValue = collected.sensorValue;
                    } else {
                        polaritySum += collected.sensorValue;
                        polaritySumCount++;
                    }
                }
            }
            // Only relevant if there is, at least, one smile report
            if (lastSmileValue != null) {
                // Last smile weights half from average total, full if only report
                if (polaritySumCount > 0) {
                    polarity = (0.5 * lastSmileValue + 0.5 * polaritySum / polaritySumCount);
                } else {
                    polarity = lastSmileValue;
                }
            }
        }
        return polarity;
    }
}
