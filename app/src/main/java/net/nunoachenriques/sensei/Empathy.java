/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

import net.nunoachenriques.sensei.persistence.DataCacheDaoService;
import net.nunoachenriques.sensei.persistence.Parameter;
import net.nunoachenriques.sensei.settings.Settings;
import net.nunoachenriques.sensei.utility.Calc;
import net.nunoachenriques.sensei.utility.DateTime;

import java.sql.Timestamp;

/**
 * Empathy all about!
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
@SuppressWarnings("WeakerAccess")
public final class Empathy {

    public static final String SENSOR_ID = "app.agent.empathy";
    public static final String SENSOR_ATTR_EMPATHY = "empathy";
    // public static final String SENSOR_ATTR_DECAY_FACTOR = "decay_factor";
    public static final String EMPATHY_PREF_KEY = "empathy";

    public static final double MIN_DEFAULT = 0;
    public static final double MAX_DEFAULT = 100;
    public static final double START_DEFAULT = (MAX_DEFAULT - MIN_DEFAULT) / 2;  // half way...
    public static final double DECAY_FACTOR_DEFAULT = 1.0;  // linear decay
    public static final double DECAY_FACTOR_ON_PAUSE = 100.0;  // 100x accelerated decay

    private static final String TAG = "Empathy";
    private static final String DECAY_FULL_INTERVAL_PREF_KEY = "empathy.decay_full_interval";
    private static final String DECAY_FACTOR_PREF_KEY = "empathy.decay_factor";

    private static volatile Empathy INSTANCE;

    private static int pauseColor;
    private static int negativeColor;
    private static int negativeNeutralColor;
    private static int neutralColor;
    private static int positiveNeutralColor;
    private static int positiveColor;
    private static SharedPreferences sharedPreferences;
    private static long lastTime;
    private static double decayRate;
    private static double decayFactor;
    private static double empathy;

    /**
     * Gets the default values for decay rate, factor and amount. Empathy
     * defaults to {@link #START_DEFAULT} if no value found.
     *
     * @param c Android app context.
     */
    private Empathy(Context c) {
        Resources res = c.getResources();
        pauseColor = res.getColor(R.color.pause, c.getTheme());
        negativeColor = res.getColor(R.color.polarity_negative, c.getTheme());
        negativeNeutralColor = res.getColor(R.color.polarity_negative_neutral, c.getTheme());
        neutralColor = res.getColor(R.color.polarity_neutral, c.getTheme());
        positiveNeutralColor = res.getColor(R.color.polarity_positive_neutral, c.getTheme());
        positiveColor = res.getColor(R.color.polarity_positive, c.getTheme());
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        lastTime = SystemClock.elapsedRealtime();
        final String decaySp = sharedPreferences.getString(DECAY_FULL_INTERVAL_PREF_KEY, null);
        setDecayRate((decaySp != null) ? Integer.parseInt(decaySp) : 86400);
        decayFactor = Settings.getDouble(sharedPreferences, DECAY_FACTOR_PREF_KEY);
        if (Double.isNaN(decayFactor)) {
            setDecayFactor(DECAY_FACTOR_DEFAULT);
        }
        empathy = Settings.getDouble(sharedPreferences, EMPATHY_PREF_KEY);
        if (Double.isNaN(empathy)) {
            set(START_DEFAULT);
            c.startService(new Intent(c, DataCacheDaoService.class)
                    .putExtra(DataCacheDaoService.EXTRA_PARAMETER_SET,
                            new Parameter[]{new Parameter(
                                    new Timestamp(System.currentTimeMillis()), DateTime.getTimeZoneISO8601(),
                                    EMPATHY_PREF_KEY, String.valueOf(START_DEFAULT),
                                    false)}));
        }
    }

    /**
     * Gets an instance of this class. If no single instance is available, i.e.,
     * {@code null}, then creates and returns a single new one
     * (Singleton pattern).
     *
     * @param c Android application {@link Context}.
     * @return The single instance of this class.
     */
    public static Empathy getInstance(Context c) {
        if (INSTANCE == null) {
            synchronized (Empathy.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Empathy(c);
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Sets empathy to the value specified by {@code e}.
     *
     * @param e The new empathy value to set.
     */
    public void set(double e) {
        empathy = e;
        Settings.putDouble(sharedPreferences, EMPATHY_PREF_KEY, e);
        Log.d(TAG, "set: " + e);
    }

    /**
     * Sets the empathy new (automatic) value after a decay amount weighted
     * by a factor.
     *
     * @see #setDecayFactor(double)
     * @see #setDecayRate(int)
     */
    public void setAfterDecay() {
        long current = SystemClock.elapsedRealtime();
        long elapsed = (current - lastTime) / 1000;  // milliseconds to seconds
        lastTime = current;
        double decay = elapsed * decayRate * decayFactor;
        set(Math.max(empathy - decay, MIN_DEFAULT));
        Log.d(TAG, "setAfterDecay: " + empathy + " | decay: " + decay
                + " | decayRate: " + decayRate + " | decayFactor: " + decayFactor);
    }

    /**
     * Sets the empathy new decay factor. A factor of {@code 1.0} is a special
     * case ({@code default_decay_rate * 1.0 = default_decay_rate}).
     *
     * @param f The new decay factor in [0.0, {@link Double#MAX_VALUE}]
     * @see #setDecayRate(int)
     */
    public void setDecayFactor(double f) {
        decayFactor = f;
        Settings.putDouble(sharedPreferences, DECAY_FACTOR_PREF_KEY, f);
    }

    /**
     * Sets the empathy new decay amount. The value is calculated to be the same
     * every decay interval step, i.e.,
     * amount = {@link #MAX_DEFAULT} / interval.
     *
     * @param interval Full time interval in seconds.
     */
    public void setDecayRate(int interval) {
        decayRate = MAX_DEFAULT / interval;
    }

    /**
     * Gets the empathy value.
     *
     * @return Empathy value in percentage, i.e., [{@link #MIN_DEFAULT},
     *         {@link #MAX_DEFAULT}].
     */
    public double get() {
        return empathy;
    }

    /**
     * Gets the empathy value rounded to the nearest integer.
     *
     * @return Empathy value in percentage, i.e., [{@link #MIN_DEFAULT},
     *         {@link #MAX_DEFAULT}] rounded to the nearest integer.
     * @see Calc#round(double)
     */
    public int getRound() {
        return Calc.round(empathy);
    }

    /**
     * Gets the {@link Color} integer value defined for the empathy when equals
     * to the given value. Returns a negative color if on pause.
     *
     * @param pause Pause mode.
     * @param value Value between 0 and 100.
     * @return A {@link Color} integer value.
     */
    public static int getProgressTint(boolean pause, int value) {
        float f = value / (float)MAX_DEFAULT;
        if (pause) {
            return pauseColor;
        } else if (f < 0.2) {
            return negativeColor;
        } else if (f < 0.4) {
            return negativeNeutralColor;
        } else if (f < 0.6) {
            return neutralColor;
        } else if (f < 0.8) {
            return positiveNeutralColor;
        } else {
            return positiveColor;
        }
    }

    /**
     * Gets the {@link Color} integer value defined for the current empathy
     * value and pause mode.
     *
     * @param pause Pause mode.
     * @return A {@link Color} integer value.
     * @see #getProgressTint(boolean, int)
     */
    public int getProgressTint(boolean pause) {
        return getProgressTint(pause, Calc.round(empathy));
    }

    /**
     * Gets the progress tint {@link Color} as a {@link ColorStateList}.
     *
     * @param pause Pause mode.
     * @return A {@link ColorStateList} for the default color.
     * @see #getProgressTint(boolean, int)
     */
    public ColorStateList getProgressTintList(boolean pause) {
        return ColorStateList.valueOf(getProgressTint(pause, Calc.round(empathy)));
    }

    /**
     * Gets the progress tint {@link Color} integer value defined for the
     * empathy when equals to the given value and whether in pause mode as a
     * {@link ColorStateList}.
     *
     * @param pause Pause mode.
     * @param value Value between 0 and 100.
     * @return A {@link ColorStateList} for the default color.
     * @see #getProgressTint(boolean, int)
     */
    public static ColorStateList getProgressTintList(boolean pause, int value) {
        return ColorStateList.valueOf(getProgressTint(pause, value));
    }
}
