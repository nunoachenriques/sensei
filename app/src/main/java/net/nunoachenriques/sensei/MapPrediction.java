/*
 * Copyright 2020 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.Polygon;

import uk.me.jstott.jcoord.LatLng;

/**
 * Map activity prediction data holder for local persistence.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
final class MapPrediction {

    private final GeoPoint geoPoint;
    private final String mgrsId;
    @Nullable
    private Polygon polygon;

    MapPrediction(GeoPoint gp, int MGRSPrecision) {
        geoPoint = gp;
        mgrsId = new LatLng(geoPoint.getLatitude(), geoPoint.getLongitude()).toMGRSRef().toString(MGRSPrecision);
    }

    MapPrediction(MapPrediction mp) {
        geoPoint = mp.getGeoPoint();
        mgrsId = mp.getMGRSId();
        polygon = mp.getPolygon();
    }

    GeoPoint getGeoPoint() {
        return geoPoint;
    }

    String getMGRSId() {
        return mgrsId;
    }

    @Nullable
    Polygon getPolygon() {
        return polygon;
    }

    void setPolygon(@Nullable Polygon p) {
        polygon = p;
    }

    @NonNull
    @Override
    public String toString() {
        return "MapPredictionCell{" +
                "geoPoint=" + geoPoint.toDoubleString() +
                ", mgrsId=" + mgrsId +
                ", polygon=" + (polygon == null ? null : polygon.getBounds()) +
                "}";
    }
}
