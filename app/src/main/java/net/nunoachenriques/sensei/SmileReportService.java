/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;

import net.nunoachenriques.sensei.emotion.Polarity;
import net.nunoachenriques.sensei.persistence.Collected;
import net.nunoachenriques.sensei.persistence.DataCacheDaoService;
import net.nunoachenriques.sensei.utility.DateTime;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The smile report (discreet sentiment polarity, i.e., -1, 0, 1) service for
 * the feed notification which presents the polarity smileys (:-(, :-|, :-)).
 * Gets data from the intent, process and store it.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see SenseiActivity
 */
public final class SmileReportService
        extends IntentService {

    public static final String SENSOR_ID = "app.human.smile";
    public static final String SENSOR_ATTR_POLARITY = Polarity.SENSOR_ATTR_POLARITY_COMMON;
    public static final double NEGATIVE = -1;
    public static final double NEUTRAL = 0;
    public static final double POSITIVE = 1;

    private static final String TAG = "SmileReportService";
    private static final String EXTRA_SMILE = TAG + SENSOR_ATTR_POLARITY;

    /** Last report timestamp in milliseconds since UTC. */
    public static volatile long lastTimestamp = 0;

    public SmileReportService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent i) {
        if (i != null) {
            double v = i.getDoubleExtra(SmileReportService.EXTRA_SMILE, Double.NaN);
            if (!Double.isNaN(v)) {
                // STORE
                lastTimestamp = System.currentTimeMillis();
                List<Collected> lc = new ArrayList<>(1);
                lc.add(new Collected(
                        new Timestamp(lastTimestamp), DateTime.getTimeZoneISO8601(),
                        SENSOR_ID, SENSOR_ATTR_POLARITY, v, null,
                        false)
                );
                startService(
                        new Intent(this, DataCacheDaoService.class)
                                .putExtra(DataCacheDaoService.EXTRA_COLLECTED_SET, lc.toArray(new Collected[0])));
                // EMPATHY reset to MAX!
                Empathy.getInstance(this).set(Empathy.MAX_DEFAULT);
                // NOTIFICATION
                // Update empathy
                startService(new Intent(this, EmpathyNotifyService.class)
                        .putExtra(EmpathyNotifyService.EXTRA_VALUE_MAX, (int)Empathy.MAX_DEFAULT)
                        .putExtra(EmpathyNotifyService.EXTRA_VALUE, (int)Empathy.MAX_DEFAULT));
                // Cancel Homeostasis reporting notification
                final NotificationManager nm = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
                if (nm != null) {
                    nm.cancel(HomeostasisService.REPORTING_NOTIFICATION_ID);
                }
                // LOCATION (GPS)
                Geopositioning.getInstance(this).get()
                        .addOnSuccessListener(v1 -> Log.d(TAG, "GEOPOSITIONING NOW successful!"))
                        .addOnFailureListener(e -> Log.e(TAG, "GEOPOSITIONING NOW FAILED!", e));
            } else {
                Log.d(TAG, "SMILE reported DATA DISCARDED!");
            }
        }
    }

    /**
     * Get a {@link PendingIntent} with a smile value to be processed.
     *
     * @param c Application context.
     * @param rc Request code.
     * @param value Value to be sent, i.e., type of smile :-(, :-|, :-).
     * @return The {@link PendingIntent} with all set.
     * @see #getIntent(Context, double)
     */
    public static PendingIntent getPendingIntent(Context c, int rc, double value) {
        return PendingIntent.getService(c, rc, getIntent(c, value), PendingIntent.FLAG_UPDATE_CURRENT);
    }

    /**
     * Get an {@link Intent} with a smile value to be processed.
     *
     * @param c Application context.
     * @param value Value to be sent, i.e., type of smile :-(, :-|, :-).
     * @return The {@link Intent} with all set.
     */
    public static Intent getIntent(Context c, double value) {
        return new Intent(c, SmileReportService.class).putExtra(EXTRA_SMILE, value);
    }
}
