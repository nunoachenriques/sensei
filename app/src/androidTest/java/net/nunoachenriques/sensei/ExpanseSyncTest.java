/*
 * Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei;

import android.content.Context;
import android.util.Log;

import net.nunoachenriques.sensei.persistence.Collected;
import net.nunoachenriques.sensei.persistence.DataCache;
import net.nunoachenriques.sensei.persistence.DataCacheDao;
import net.nunoachenriques.sensei.persistence.Entity;
import net.nunoachenriques.sensei.persistence.Parameter;
import net.nunoachenriques.sensei.utility.DateTime;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

/**
 * Testing Expanse sync (local data cache to cloud Web service).
 *
 * TODO: create a proper mock up to test database get/set offline!
 *
 * WORKAROUND:
 *  1. In order to have a valid OAuth2 ID Token, use the emulator and
 *     run the app until Google Sign in and then stop it in Android Studio.
 *  2. Next run this test.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
@RunWith(AndroidJUnit4.class)
public class ExpanseSyncTest {

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();  // Live data sync required!

    private Context context;
    private DataCache dataCache;
    private DataCacheDao dataCacheDao;
    private ExpanseSync expanseSync;

    @Before
    public void initialization() {
        context = ApplicationProvider.getApplicationContext();
        dataCache = Room.inMemoryDatabaseBuilder(context, DataCache.class)  // in memory
                .allowMainThreadQueries()
                .addCallback(new DataCache.DataCacheCallback())
                .build();
        dataCacheDao = dataCache.getDao();
        expanseSync = ExpanseSync.getInstance(dataCacheDao);
    }

    @After
    public void finalization() {
        dataCache.close();
    }

    @Test
    public void dataSetAndSyncTest() {
        // SET
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        String tz = DateTime.getTimeZoneISO8601();
        // Entity
        dataCacheDao.set(new Entity("12p3oiuo1i2u3o12iu3894983y498b948bno9283489bnur9uwo3urw8423ur984",
                Date.valueOf("1942-11-24"), "1", false));
        // Parameter
        List<Parameter> parameterList = new ArrayList<>(3);
        parameterList.add(new Parameter(ts, tz, "k42", "42", false));
        parameterList.add(new Parameter(ts, tz, "kAA", "AA", false));
        parameterList.add(new Parameter(ts, tz, "k42BB", "42BB", false));
        dataCacheDao.set(parameterList.toArray(new Parameter[0]));
        // Collected
        List<Collected> collectedList = new ArrayList<>(3);
        collectedList.add(new Collected(ts, tz,
                "app.sensor.geopositioning", "latitude", 38.7555857, null, false));
        collectedList.add(new Collected(ts, tz,
                "app.sensor.geopositioning", "provider", 0.0, "fused", false));
        collectedList.add(new Collected(ts, tz,
                "android.sensor.magnetic_field", "0", 10.41107177734375, null, false));
        dataCacheDao.set(collectedList.toArray(new Collected[0]));
        // SYNC
        expanseSync.sync(context, false, 100);
        final int notSyncedExpected = 0;
        Entity entity = dataCacheDao.getEntity();
        parameterList = dataCacheDao.getParameterNotSynced();
        collectedList = dataCacheDao.getCollectedNotSynced();
        final int notSynced = (!entity.synced ? 1 : 0)
                + parameterList.size()
                + collectedList.size();
        // CHECK
        Assert.assertEquals("SET and SYNC FAILED!", notSyncedExpected, notSynced);
    }

    /**
     * Testing the predictions from sensei-expanse database in a development
     * environment.
     *
     * NOTICE: he database MUST be populated with dummy data for entity "x".
     *         This must include model and pipeline results from learning!
     */
    @Test
    public void dataGetPredictionTest() {
        try {
            JSONArray contextData = new JSONArray()
                    .put(new JSONObject()
                            .put("moment", "2001-01-01T01:01:01.000+00:00")
                            .put("latitude", 38.0)
                            .put("longitude", 9.0))
                    .put(new JSONObject()
                            .put("moment", "2002-02-02T02:02:02.000+00:00")
                            .put("latitude", 38.72)
                            .put("longitude", -9.34));
            JSONArray result = expanseSync.syncPrediction(context, false,
                    contextData, true, false);
            // CHECK
            Assert.assertNotNull("Result is null!", result);
            Log.d("ExpanseSyncTest result:", result.toString(4));
        } catch (Exception e) {
            Log.e("ExpanseSyncTest", "FAILED! :-(", e);
        }
    }
}
