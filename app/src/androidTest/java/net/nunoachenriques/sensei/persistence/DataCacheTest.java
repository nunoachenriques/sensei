/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.persistence;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import net.nunoachenriques.sensei.utility.DateTime;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Testing database version 2+ set and search (FTS).
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
@RunWith(AndroidJUnit4.class)
public class DataCacheTest {

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();  // Live data sync required!

    private DataCache dataCache;
    private DataCacheDao dataCacheDao;

    @Before
    public void createDatabase() {
        Context ctx = ApplicationProvider.getApplicationContext();
        dataCache = Room.inMemoryDatabaseBuilder(ctx, DataCache.class)
                .allowMainThreadQueries()
                .addCallback(new DataCache.DataCacheCallback())
                .build();
        dataCacheDao = dataCache.getDao();
    }

    @After
    public void closeDatabase() {
        dataCache.close();
    }

    @Test
    public void chatSetAndSearchTest() {
        // SET
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        String tz = DateTime.getTimeZoneISO8601();
        List<Chat> chats = new ArrayList<>(3);
        final String textExpected = "News from south Africa very much disturbing :-(";
        chats.add(new Chat(ts, tz, 1,
                textExpected, "en", -0.74629998207092285156));
        chats.add(new Chat(ts, tz, 1,
                "Testing the new diary with style ;-)", "en", 0.25));
        chats.add(new Chat(ts, tz, 1,
                "Diary performing top level with search filter and all :-D", "en", 0.70340001583099365234));
        dataCacheDao.set(chats.toArray(new Chat[0]));
        // SEARCH
        LiveData<List<Chat>> chatListLiveData = dataCacheDao.getDiaryMessageMatch(
                DataCache.getChatMatchQuery("disturbing :-("));
        String textResult;
        try {
            List<Chat> chatList = getValue(chatListLiveData);
            textResult = (chatList != null) ? chatList.get(0).text : null;
        } catch (InterruptedException e) {
            textResult = null;
        }
        // CHECK
        Assert.assertEquals("SET and SEARCH FAILED!", textExpected, textResult);
    }

    /** @see <a href="https://stackoverflow.com/a/44271247/8418165">From: Stack Overflow</a> */
    private static <T> T getValue(final LiveData<T> liveData) throws InterruptedException {
        final Object[] data = new Object[1];
        final CountDownLatch latch = new CountDownLatch(1);
        Observer<T> observer = new Observer<T>() {
            @Override
            public void onChanged(@Nullable T o) {
                data[0] = o;
                latch.countDown();
                liveData.removeObserver(this);
            }
        };
        liveData.observeForever(observer);
        latch.await(2, TimeUnit.SECONDS);
        //noinspection unchecked
        return (T) data[0];
    }
}
