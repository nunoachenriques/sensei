/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensei.persistence;

import android.database.Cursor;

import net.nunoachenriques.sensei.utility.DateTime;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.Objects;

import androidx.room.testing.MigrationTestHelper;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

/**
 * Testing database and migration possible issues.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
@RunWith(AndroidJUnit4.class)
public class DataCacheMigrationTest {

    private static final String TEST_DB = "DataCacheMigrationTest.db";

    @Rule
    public final MigrationTestHelper helper;

    public DataCacheMigrationTest() {
        helper = new MigrationTestHelper(InstrumentationRegistry.getInstrumentation(),
                Objects.requireNonNull(DataCache.class.getCanonicalName()),
                new FrameworkSQLiteOpenHelperFactory());
    }

    @Test
    public void migrate1To2() throws IOException {
        long ts = System.currentTimeMillis();
        String tz = DateTime.getTimeZoneISO8601();
        // OPEN DB VERSION 1
        SupportSQLiteDatabase db = helper.createDatabase(TEST_DB, 1);
        // TEST INSERT
        final int INSERT_ROWS = 3;
        db.execSQL("INSERT INTO `Chat` VALUES (?,?,?,?,?,?,?)",
                new Object[]{1, ts, tz, 1,
                        "News from south Africa very much disturbing :-(", "en", -0.74629998207092285156});
        db.execSQL("INSERT INTO `Chat` VALUES (?,?,?,?,?,?,?)",
                new Object[]{2, ts, tz, 1,
                        "Testing the new diary with style ;-)", "en", 0.25});
        db.execSQL("INSERT INTO `Chat` VALUES (?,?,?,?,?,?,?)",
                new Object[]{3, ts, tz, 1,
                        "Diary performing top level with search filter and all :-D", "en", 0.70340001583099365234});
        Cursor cursor = db.query("select * from `Chat`");
        int count1 = cursor.getCount();
        cursor.close();
        db.close();
        // Re-OPEN DB and provide MIGRATION_1_2 for final VERSION 2.
        db = helper.runMigrationsAndValidate(TEST_DB, 2, false, DataCache.MIGRATION_1_2);
        // TEST migration
        final int AFTER_DELETE_ROWS = 2; // After migration and delete operation
        db.execSQL("DELETE FROM `Chat` WHERE polarity = 0.25");
        cursor = db.query("select * from `Chat`");
        int count2 = cursor.getCount();
        cursor.close();
        // TEST full text search
        final int AFTER_MATCH_ROWS = 1;
        cursor = db.query(DataCache.getChatMatchQuery("disturb"));
        int count3 = cursor.getCount();
        cursor.close();
        // CLOSE DB
        db.close();
        // CHECK
        Assert.assertEquals("INSERT rows FAILED!", INSERT_ROWS, count1);
        Assert.assertEquals("DELETE rows FAILED!", AFTER_DELETE_ROWS, count2);
        Assert.assertEquals("MATCH rows FAILED!", AFTER_MATCH_ROWS, count3);
    }

    @Test
    public void migrate2To3() throws IOException {
        // OPEN DB VERSION 2
        SupportSQLiteDatabase db = helper.createDatabase(TEST_DB, 2);
        // TEST INSERT duplicate data
        final int INSERT_ROWS = 14;
        db.execSQL("INSERT INTO Collected VALUES(5,1538417095129,'+01:00','app.sensor.geopositioning','latitude',34.178699799999996856,NULL,0)");
        db.execSQL("INSERT INTO Collected VALUES(6,1538417095129,'+01:00','app.sensor.geopositioning','longitude',-86.615415299999995116,NULL,0)");
        db.execSQL("INSERT INTO Collected VALUES(7,1538417095129,'+01:00','app.sensor.geopositioning','h_accuracy',3644.9999999999999999,NULL,0)");
        db.execSQL("INSERT INTO Collected VALUES(8,1538417095129,'+01:00','app.sensor.geopositioning','provider',0.0,'fused',0)");
        db.execSQL("INSERT INTO Collected VALUES(33,1538417095129,'+01:00','app.sensor.geopositioning','latitude',34.178699799999996856,NULL,0)");
        db.execSQL("INSERT INTO Collected VALUES(34,1538417095129,'+01:00','app.sensor.geopositioning','longitude',-86.615415299999995116,NULL,0)");
        db.execSQL("INSERT INTO Collected VALUES(35,1538417095129,'+01:00','app.sensor.geopositioning','h_accuracy',3644.9999999999999999,NULL,0)");
        db.execSQL("INSERT INTO Collected VALUES(36,1538417095129,'+01:00','app.sensor.geopositioning','provider',0.0,'fused',0)");
        db.execSQL("INSERT INTO Collected VALUES(100,1538417095130,'+01:00','app.sensor.activity','type',0.0,'IN_VEHICLE',0)");
        db.execSQL("INSERT INTO Collected VALUES(101,1538417095130,'+01:00','app.sensor.activity','transition',0.0,'ENTER',0)");
        db.execSQL("INSERT INTO Collected VALUES(102,1538417095130,'+01:00','app.sensor.activity','type',1.0,'ON_BICYCLE',0)");
        db.execSQL("INSERT INTO Collected VALUES(103,1538417095130,'+01:00','app.sensor.activity','transition',1.0,'EXIT',0)");
        db.execSQL("INSERT INTO Collected VALUES(104,1538417095130,'+01:00','app.sensor.activity','type',0.0,'IN_VEHICLE',0)");
        db.execSQL("INSERT INTO Collected VALUES(105,1538417095130,'+01:00','app.sensor.activity','transition',0.0,'ENTER',0)");
        Cursor cursor = db.query("select * from `Collected`");
        int count1 = cursor.getCount();
        cursor.close();
        db.close();
        // Re-OPEN DB and provide MIGRATION_2_3 for final VERSION 3.
        db = helper.runMigrationsAndValidate(TEST_DB, 3, false, DataCache.MIGRATION_2_3);
        // TEST migration
        final int AFTER_MIGRATION_ROWS = 4; // After migration operation
        cursor = db.query("select * from `Collected`");
        int count2 = cursor.getCount();
        cursor.close();
        // CLOSE DB
        db.close();
        // CHECK
        Assert.assertEquals("INSERT rows FAILED!", INSERT_ROWS, count1);
        Assert.assertEquals("DELETE rows FAILED!", AFTER_MIGRATION_ROWS, count2);
    }
}
