[![Android 6.0+](https://img.shields.io/badge/mobile-Android%206.0%2B-%2378C257.svg "Android 6.0+")][1]
[![Apache 2.0 License](https://img.shields.io/badge/license-Apache%202.0-blue.svg "Apache 2.0 License")][2]

# SensAI --- Sensorial agent for Android

An artificial intelligence (AI) companion to humans for emotional
sensing. The SensAI is the sensorial interaction in Java for Android™.
The app delivers interaction ability between a human and an artificial
agent towards empathy using mobile devices. SensAI collects sensors
data, human emotional sharing, syncs with Cloud expanded resources
([SensAI Expanse](https://gitlab.com/nunoachenriques/sensei-expanse))
and learns towards emotional valence in context (geographical and time)
prediction.

## Repository

https://gitlab.com/nunoachenriques/sensei

## Install

<a href='https://play.google.com/store/apps/details?id=net.nunoachenriques.sensei&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png'/></a>

## Development

### Android Studio (recommended)

 1. Fork or download project [source code](#repository).
 2. Use [Android Studio](https://developer.android.com/studio) to open the project.
 3. Use emulators or connected devices and the usual actions.

### Google Identity Platform integration

https://developers.google.com/api-client-library/python/auth/web-app#creatingcred

1. New project PROJECT_NAME (e.g., SensAI Expanse)
2. OAuth consent screen:
   1. Application name: APP_NAME (e.g., SensAI)
   2. (defaults)
3. Credentials Web service app:
   1. Create credentials:
      * OAuth client ID
      * Web application
   2. Name: CREDENTIALS_NAME (e.g., SensAI + Expanse)
   3. Download JSON secrets to `assets/NAME` (e.g., `assets/sensai_expanse_credentials.json.secret`)
   4. (See [SensAI Expanse](https://gitlab.com/nunoachenriques/sensei-expanse))
      Download JSON secrets to Web app.
4. Credentials Android app:
   1. Create credentials:
      * OAuth client ID
      * Android
   2. Name: create one credential each for (1) testing (debug, emulator
      or device, use Android default debug keystore); (2) production
      (release) and NOTICE that if your app is being signed by Google
      then the certificate is the one from Google at Play developer
      console "App signing certificate".
   3. Fill in app signing certificate SHA-1 fingerprint and package
      name.

### Twitter integration

Create `res/values/twitter.xml` and fill it with your app consumer key and
secret (https://apps.twitter.com/):

```xml
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <string name="com.twitter.sdk.android.CONSUMER_KEY" translatable="false">FILL_IN_HERE</string>
    <string name="com.twitter.sdk.android.CONSUMER_SECRET" translatable="false">FILL_IN_HERE</string>
</resources>
```

Moreover, the testing device or emulator **must** have **Twitter app installed**
and **user logged in**.

### Command-line

 1. Fork or download project [source code](#repository).
 2. Use a device connected with ADB.
 3. Test it.

    ```shell
    ./gradlew :app:connectedAndroidTest
    ```
    
 4. Install and debug

    ```shell
    ./gradlew installDebugRelease
    ```

### TODO

 1. Better resources consumption (memory + processor) learn and adapt.
 
 2. Better UI with more information such as live prediction map.
    
 3. Tests, tests, tests, automated and continuous (CI/CD).

## FAQ

 1. Why do I need a Google account if data is to be anonymous?
 
    Yes, data will be anonymous by the agent. In order to distinguish
    between anonymous entities (person) the agent uses the Google account to
    generate an abstract unique identifier (e.g., e49ir4wp4ri49ruw84ur4).
    Moreover, data is encrypted in transit (HTTP over TLS) and authenticated
    (OAuth2 ID Token) by the secure Web service before storage to avoid
    tampering.
 
 2. Is my directly identifiable data stored in the cloud by the agent?
 
    No. Account email or other directly identifiable data is not stored
    in the cloud. Moreover, all natural language text is processed by the agent
    and only the resulted sentiment is sent, i.e., a floating point value
    (e.g., 1.0).

## License

Copyright 2017 Nuno A. C. Henriques https://nunoachenriques.net/

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Trademarks

Android is a trademark of Google LLC.

[1]: https://developer.android.com/about/versions/marshmallow
[2]: http://www.apache.org/licenses/LICENSE-2.0.html
